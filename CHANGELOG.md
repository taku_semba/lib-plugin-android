## [6.4.0] - 
### Added
- Now is possible to delay the start event (and therefore have all the metadata ready) and have correcto joinTime

## [6.3.9] - 2019-02-20
### Added
- sessionRoot will be sent (again) for every request
### Deprecated
- Constructor Plugin(Options)

## [6.3.8] - 2019-02-13
### Added
- Fingerprint parameter
### Fixed
- Akamai CDN parse should now work as expected
- fireStop will not be sent anymore when the adapter is not started
- Infinity url parameters will not be sent anymore if isInfinity is false

## [6.3.7] - 2019-02-06
### Added
- accountCode has been added to every request
- appName and appReleaseVersion added as an option
- TELEFO CDN added
- title2 renamed to program 
- extraparams renamed to customDimensions
- adExtraparams renamed to adCustomDimensions

## [6.3.6] - 2019-01-28
### Added
 - Now adInit is sent automatically when some information cannot be retrieved

## [6.3.5] - 2019-01-21
### Fix 
 - Fixed communication reseting in case of an error event before /start request

## [6.3.4] - 2019-01-14
### Added 
 - Add pluginInfo param 
 - isActivityStopped property added

## [6.3.3] - 2018-12-27
### Added 
 - Now almost every device dimensiones it's customizable through options
 - Last year deploy, HAPPY 2019!!!

## [6.3.2] - 2018-12-20
### Fixed 
 - Fix auto background with ads and init

## [6.3.1] - 2018-12-18
### Fixed 
 - In case of joinTime firing too early in offline mode it gets queued until start is sent

## [6.3.0] - 2018-11-29
### Added
 - Now init is sent automatically when some information cannot be retrieved
### Fixed 
 - Now the autoDetectBackground option is set to true by default

## [6.2.11] - 2018-10-25
### Added
 - Add Smart Switch options
### Fixed
 - Depending on the applicaton architecture the offline access to SQLite DB may crash, that is fixed
 - Now P2P params are added to the RequestBuilder
 
## [6.2.10] - 2018-10-09
### Improved
 - Errors doesn't change viewcode if they are sent right after stop (500ms or less)
### Fixed
 - Wrong mediaResource if ParseHLS and contentResource option were active at the same time
 - Timer setNextTick() being call when shouldn't
 - Crash when not closing local DB
 - Make local DB singleton syncrhonized to avoid exceptions

## [6.2.9] - 2018-09-12
### Fixed
- AnonymousUser now is sent correctly

## [6.2.8] - 2018-09-06
### Fixed
- Crash when enabling offline mode
- Now offline stored events will be sent even if the adapter has not been started

## [6.2.7] - 2018-08-29
### Added
 - Get activity setted as default in case of no onStart

## [6.2.6] - 2018-08-29
### Fixed
 - Fix background detection by checking activities

## [6.2.5] - 2018-08-28
### Changed
 - Changed Infinity event endpoint
### Fixed
 - Fix background detection

## [6.2.4] - 2018-08-27
### Fixed
 - Fix expiration time

## [6.2.3] - 2018-08-27
### Fixed
 - isInfinity option

## [6.2.2] - 2018-08-24
### Fixed
 - Fix a crash when not providing context
 - Fix dimensions name for event

## [6.2.1] - 2018-08-23
### Fixed
 - Wrong parameter name for dimensions

## [6.2.0] - 2018-08-22
### Added
 - Infinity
 - Option to anonymous user

## [6.1.8] - 2018-07-10
### Added
 - Now the username will be included in the data of the request, but just if it isn't null
 - Three methods: fireClick(String url), fireCasted(), fireSkipped()
### Fixed
 - Now if the adapter is not changed and an stop is send the adNumber will reset properly
 - Now the resource set on the options won't be overwritten by the resource of the video being played
 - Now when fireStop() is called from the Plugin's class, it will check if the adapter is not null. If it isn't, it will call the adapter's fireStop().
 - If the adDuration is null, it won't be sent on the adStart anymore
### Removed
 - Nqs6Transform usages commented
  
## [6.1.7] - 2018-05-04
### Added
 - HouseholdId param

## [6.1.6] - 2018-04-27
### Added
 - Possibility to hide IP Address
 - New parameters: latency, packetLoss, packetSend
 - Option to disable seeks in case of live

## [6.1.5] - 2018-04-16
### Fixed
 - Wrong pingTime if sending offline events just before starting offline playback

## [6.1.4] - 2018-03-23
### Added
 - Experiment ids for smart users
### Fixed
 - No exception raised if adapter.getDuration() returns null
 
## [6.1.3] - 2018-03-08
### Added
 - Now there are ten extraparams (total twenty)

## [6.1.2] - 2018-02-21
### Fixed
 - Now if you fire an error without init or start the view number will increase

## [6.1.1] - 2018-02-19
### Improved
 - If is live content duration will be reported as 0
### Fixed
 - Error serverity is not send anymore (adapters can send it anyway)
 - No crash if an ads adapter doesn't implement getPosition() method

## [6.1.0] - 2018-01-30
### Added
 - Streaming protocol option now is "sendable"
 - User Type option
 - Ad extraparams
### Removed
 - Now Room is no longer a dependency

## [6.1.0-beta2] - 2018-01-15
### Fixed
 - Now events are properly removed when they have been send
 - New event send system

## [6.1.0-beta] - 2018-01-05
### Refactored
 - Offline mode refactored, now SQLite is used instead of SharedPrefs

## [6.0.10] - 2018-01-02
### Added
 - Streaming protocol option
 - Test cases

## [6.0.9] - 2017-12-22
### Fixed
 - No data petition in case of offline mode
 - Pause duration properly reported in case of stopping while paused
 - Fire fatal error improved

## [6.0.8] - 2017-12-15
### Added
 - removeAdapter and removeAdsAdapter stopping pings is optional
 - Add AdError specific methods
 - Add playhead
 - Add timemark for debug porpuses
### Fixed
 - If no adInit join starts
 - In case of stop while paused now pauseDuration is send too
 - Only stops if init
## Removed
 - Send ALWAYS ads stop if needed with player stop

## [6.0.8-beta8] - 2017-12-04
### Added
 - Callback to call when ads have ended
 - Timemark on all requests
### Fixed
 - Null check chronos

## [6.0.8-beta7] - 2017-11-29
### Added
 - FireEnd method
### Fixed
 - FireEnd with params
 - adPlayhead and playhead doesn't need adapter

## [6.0.8-beta6] - 2017-11-28
### Added
 - FireFatalError at plugin
 - Check for playback has really ended
### Fixed
 - Wrong join time with adInit.

## [6.0.8-beta5] - 2017-11-24
### Added
 - FireStop plugin
 - Check playhead for ad position

## [6.0.8-beta4] - 2017-11-24
### Added
 - FireFatalError with Exception to filter by it

## [6.0.8-beta3] - 2017-11-23
### Added
 - New error method
###Fixed
 - Wrong joinTime if having preroll and /init
 - Fix type in Constants.java

## [6.0.8-beta2] - 2017-11-23
### Added
 - Ads after stop

## [6.0.8-beta] - 2017-11-22
### Added
 - Autostart option

## [6.0.7] - 2017-11-15
### Fixed
 - Playhead on ad events
### Removed
 - errorLevel fatal

## [6.0.6] - 2017-11-14
### Fixed
 - Add null check for adInit

## [6.0.5] - 2017-11-08
### Added
 - pauseDuration on stop event
 - DeviceInfo class
### Fixed
 - AdDuration is no 0 on adinit and adstart
 - AdNumber not incrementing
 - If no activity passed but autobackground option enabled display error log message

## [6.0.4] - 2017-10-10
### Added
 - AdInit method

## [6.0.3] - 2017-10-02
### Added
 - Null check when removing activity callbacks
 - AutoDetectBackground option

## [6.0.2] - 2017-09-29
### Added
 - Null check when removing activity callbacks
 - New Ad events, adSkip, adClick
 - Remove unnecesary callback
 - Offline events
### Removed
 - GenericAdsAdapter

## [6.0.1] - 2017-08-25
### Fixed
 - Join time calculation

## [6.0.0] - 2017-07-18
### Fixed
 - First release
 
