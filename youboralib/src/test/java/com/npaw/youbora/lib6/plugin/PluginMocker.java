package com.npaw.youbora.lib6.plugin;

import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.Timer;
import com.npaw.youbora.lib6.adapter.PlaybackChronos;
import com.npaw.youbora.lib6.adapter.PlaybackFlags;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.comm.Communication;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.FlowTransform;
import com.npaw.youbora.lib6.comm.transform.OfflineTransform;
import com.npaw.youbora.lib6.comm.transform.ResourceTransform;
import com.npaw.youbora.lib6.comm.transform.ViewTransform;

import org.junit.Before;

import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PluginMocker {

    class TestPlugin extends Plugin {
        TestPlugin(Options options) { super(options); }
        TestPlugin(Options options, PlayerAdapter adapter) { super(options, adapter); }

        @Override
        Chrono createChrono() { return mockChrono; }

        @Override
        Options createOptions() { return mockOptions; }

        @Override
        Timer createTimer(Timer.TimerEventListener listener, long interval) {
            // Capture timer listener
            timerEventListener = listener;
            return mockTimer;
        }

        @Override
        RequestBuilder createRequestBuilder(Plugin plugin) { return mockRequestBuilder; }

        @Override
        ResourceTransform createResourceTransform(Plugin plugin) { return mockResourceTransform; }

        @Override
        ViewTransform createViewTransform(Plugin plugin) { return mockViewTransform; }

        @Override
        Communication createCommunication(){ return mockCommunication; }

        @Override
        FlowTransform createFlowTransform() { return mockFlowTransform; }

        @Override
        OfflineTransform createOfflineTransform() { return mockOfflineTransform; }

        @Override
        Request createRequest(String host, String service) { return mockRequest; }
    }

    TestPlugin p;

    Chrono mockChrono = null;
    private Timer mockTimer = null;
    Options mockOptions = null;
    RequestBuilder mockRequestBuilder = null;
    ResourceTransform mockResourceTransform = null;
    private FlowTransform mockFlowTransform = null;
    ViewTransform mockViewTransform = null;
    private OfflineTransform mockOfflineTransform = null;
    private Communication mockCommunication = null;
    private Request mockRequest = null;
    PlayerAdapter mockAdapter = null;
    PlayerAdapter mockAdAdapter = null;
    PlayerAdapter.AdapterEventListener adapterEventListener = null;
    PlayerAdapter.AdapterEventListener adAdapterEventListener = null;
    Timer.TimerEventListener timerEventListener = null;

    static final Double [] DOUBLE_INVALID = new Double[] {null, Double.MAX_VALUE,
            Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NaN};

    static final Integer [] INTEGER_INVALID = new Integer[] {null, Integer.MAX_VALUE,
            Integer.MIN_VALUE};

    static final Long [] LONG_INVALID = new Long[] {null, Long.MAX_VALUE, Long.MIN_VALUE};

    @Before
    public void before() {
        // Mock classes used by the Plugin
        mockChrono = mock(Chrono.class);
        mockTimer = mock(Timer.class);
        mockOptions = mock(Options.class);
        mockRequestBuilder = mock(RequestBuilder.class);
        mockResourceTransform = mock(ResourceTransform.class);
        mockFlowTransform = mock(FlowTransform.class);
        mockViewTransform = mock(ViewTransform.class);
        mockOfflineTransform = mock(OfflineTransform.class);
        mockCommunication = mock(Communication.class);
        mockRequest = mock(Request.class);
        mockAdapter = mock(PlayerAdapter.class);
        mockAdAdapter = mock(PlayerAdapter.class);

        p = new TestPlugin(mockOptions, mockAdapter);

        // Capture adapter listener
        ArgumentCaptor<PlayerAdapter.AdapterEventListener> listenerCaptor = ArgumentCaptor.forClass(PlayerAdapter.AdapterEventListener.class);
        verify(mockAdapter).addEventListener(listenerCaptor.capture());
        adapterEventListener = listenerCaptor.getValue();

        p.setAdsAdapter(mockAdAdapter);

        // Capture adAdapter listener
        verify(mockAdAdapter).addEventListener(listenerCaptor.capture());
        adAdapterEventListener = listenerCaptor.getValue();

        // Provide the adapters with real flags and chronos
        PlaybackFlags adapterFlags = new PlaybackFlags();
        PlaybackChronos adapterChronos = new PlaybackChronos();
        when(mockAdapter.getFlags()).thenReturn(adapterFlags);
        when(mockAdapter.getChronos()).thenReturn(adapterChronos);

        PlaybackFlags adAdapterFlags = new PlaybackFlags();
        PlaybackChronos adAdapterChronos = new PlaybackChronos();
        when(mockAdAdapter.getFlags()).thenReturn(adAdapterFlags);
        when(mockAdAdapter.getChronos()).thenReturn(adAdapterChronos);
    }
}
