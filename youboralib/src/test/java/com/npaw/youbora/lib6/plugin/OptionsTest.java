package com.npaw.youbora.lib6.plugin;

import android.os.Bundle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)

public class OptionsTest {
    private Options o;
    private Bundle optBundle;
    private Map<String, Object> fakeBundle;

    @Before
    public void setUp() {
        o = new Options();
        optBundle = mock(Bundle.class);
        fakeBundle = new HashMap<>();

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) {
                Object[] arguments = invocationOnMock.getArguments();
                String key = ((String) arguments[0]);
                Boolean value = ((Boolean) arguments[1]);
                fakeBundle.put(key, value);
                return null;
            }
        }).when(optBundle).putBoolean(anyString(),anyBoolean());

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) {
                Object[] arguments = invocationOnMock.getArguments();
                String key = ((String) arguments[0]);
                String value = ((String) arguments[1]);
                fakeBundle.put(key, value);
                return null;
            }
        }).when(optBundle).putString(anyString(),anyString());


        when(optBundle.getString(anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) {
                Object[] arguments = invocation.getArguments();
                String key = ((String) arguments[0]);
                return (String)fakeBundle.get(key);
            }
        });


        when(optBundle.getBoolean(anyString())).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocation) {
                Object[] arguments = invocation.getArguments();
                String key = ((String) arguments[0]);
                return (Boolean)fakeBundle.get(key);
            }
        });

    }

    @After
    public void tearDown() {
        o = null;
    }

    @Test
    public void testOptions() {
        //Setting options using set method
        setValues(o);
        assertValues(o);

        //Setting options using Bundle
        setBundleAsOptions();
    }

    private void setValues(Options o) {
        // Set values
        o.setEnabled(false);
        o.setHttpSecure(true);
        o.setHost("a");
        o.setAccountCode("b");
        o.setUsername("c");
        o.setAnonymousUser("anon");
        o.setParseHls(true);
        o.setParseCdnNameHeader("d");
        o.setParseCdnNode(true);
        ArrayList<String> cdnNodes = new ArrayList<>();
        cdnNodes.add("listitem1");
        cdnNodes.add("listitem2");
        o.setParseCdnNodeList(cdnNodes);
        o.setNetworkIP("f");
        o.setNetworkIsp("g");
        o.setNetworkConnectionType("h");
        o.setNetworkObfuscateIp(true);
        o.setDeviceCode("i");
        o.setContentResource("j");
        o.setContentIsLive(true);
        o.setContentTitle("k");
        o.setProgram("l");
        o.setContentDuration(1.0);
        o.setContentTransactionCode("m");
        o.setContentBitrate(2L);
        o.setContentThroughput(3L);
        o.setContentRendition("n");
        o.setContentCdn("o");
        o.setContentFps(4.0);
        Bundle b = mock(Bundle.class);
        when(b.getString("p")).thenReturn("q");
        b.putString("p", "q");
        o.setContentMetadata(b);
        Bundle b2 = mock(Bundle.class);
        when(b2.getString("r")).thenReturn("s");
        b.putString("r", "s");
        o.setAdMetadata(b2);
        o.setAdIgnore(false);
        o.setAdsAfterStop(0);
        o.setAutoDetectBackground(false);
        o.setAutoStart(true);
        o.setOffline(false);
        o.setIsInfinity(false);
        o.setCustomDimension1("t");
        o.setCustomDimension2("u");
        o.setCustomDimension3("v");
        o.setCustomDimension4("w");
        o.setCustomDimension5("x");
        o.setCustomDimension6("y");
        o.setCustomDimension7("z");
        o.setCustomDimension8("aa");
        o.setCustomDimension9("ab");
        o.setCustomDimension10("ac");
        o.setCustomDimension11("ad");
        o.setCustomDimension12("ae");
        o.setCustomDimension13("af");
        o.setCustomDimension14("ag");
        o.setCustomDimension15("ah");
        o.setCustomDimension16("ai");
        o.setCustomDimension17("aj");
        o.setCustomDimension18("ak");
        o.setCustomDimension19("al");
        o.setCustomDimension20("am");
        o.setUserType("an");
        o.setContentStreamingProtocol("DASH");
        o.setAppName("appName");
        o.setAppReleaseVersion("appReleaseVersion");
    }

    private void assertValues(Options o) {
        // Verify
        assertEquals(false, o.isEnabled());
        assertEquals(true, o.isHttpSecure());
        assertEquals("a", o.getHost());
        assertEquals("b", o.getAccountCode());
        assertEquals("c", o.getUsername());
        assertEquals("anon", o.getAnonymousUser());
        assertEquals(true, o.isParseHls());
        assertEquals("d", o.getParseCdnNameHeader());
        assertEquals(true, o.isParseCdnNode());
        assertEquals("listitem1", o.getParseCdnNodeList().get(0));
        assertEquals("listitem2", o.getParseCdnNodeList().get(1));
        assertEquals("f", o.getNetworkIP());
        assertEquals("g", o.getNetworkIsp());
        assertEquals("h", o.getNetworkConnectionType());
        assertEquals(true,o.getNetworkObfuscateIp());
        assertEquals("i", o.getDeviceCode());
        assertEquals("j", o.getContentResource());
        assertEquals(true, o.getContentIsLive());
        assertEquals("k", o.getContentTitle());
        assertEquals("l", o.getProgram());
        assertEquals((Double) 1.0, o.getContentDuration());
        assertEquals("m", o.getContentTransactionCode());
        assertEquals((Long) 2L, o.getContentBitrate());
        assertEquals((Long) 3L, o.getContentThroughput());
        assertEquals("n", o.getContentRendition());
        assertEquals("o", o.getContentCdn());
        assertEquals((Double) 4.0, o.getContentFps());
        assertEquals("q", o.getContentMetadata().getString("p"));
        assertEquals("s", o.getAdMetadata().getString("r"));
        assertEquals(false,o.getAdIgnore());
        assertEquals(0,o.getAdsAfterStop());
        assertEquals(false,o.isAutoDetectBackground());
        assertEquals(true,o.isAutoStart());
        assertEquals(false,o.isOffline());
        assertEquals(false, o.getIsInfinity());
        assertEquals("t", o.getCustomDimension1());
        assertEquals("u", o.getCustomDimension2());
        assertEquals("v", o.getCustomDimension3());
        assertEquals("w", o.getCustomDimension4());
        assertEquals("x", o.getCustomDimension5());
        assertEquals("y", o.getCustomDimension6());
        assertEquals("z", o.getCustomDimension7());
        assertEquals("aa", o.getCustomDimension8());
        assertEquals("ab", o.getCustomDimension9());
        assertEquals("ac", o.getCustomDimension10());
        assertEquals("ad", o.getCustomDimension11());
        assertEquals("ae", o.getCustomDimension12());
        assertEquals("af", o.getCustomDimension13());
        assertEquals("ag", o.getCustomDimension14());
        assertEquals("ah", o.getCustomDimension15());
        assertEquals("ai", o.getCustomDimension16());
        assertEquals("aj", o.getCustomDimension17());
        assertEquals("ak", o.getCustomDimension18());
        assertEquals("al", o.getCustomDimension19());
        assertEquals("am", o.getCustomDimension20());
        assertEquals("an", o.getUserType());
        assertEquals("DASH",o.getContentStreamingProtocol());
        assertEquals("appName", o.getAppName());
        assertEquals("appReleaseVersion", o.getAppReleaseVersion());
    }

    private void setBundleAsOptions() {

        /*doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] arguments = invocationOnMock.getArguments();
                String key = ((String) arguments[0]);
                String value = ((String) arguments[1]);
                fakeBundle.put(key, value);
                return null;
            }
        }).when(optBundle).putStringArrayList(anyString(), Matchers.anyListOf(String.class));*/

        ArrayList<String> cdnNodeList = new ArrayList<>();
        cdnNodeList.add("listitem1");
        cdnNodeList.add("listitem2");

        Bundle metadataBundle = mock(Bundle.class);
        metadataBundle.putString("p","q");

        Bundle adMetadataBundle = mock(Bundle.class);
        adMetadataBundle.putString("r","s");

        optBundle.putBoolean("enabled",false);
        optBundle.putBoolean("httpSecure",false);
        optBundle.putString("host","a");
        optBundle.putString("config.accountCode","b");
        optBundle.putString("username","c");
        optBundle.putString("anonymousUser", "anon");
        optBundle.putBoolean("parse.Hls",true);
        optBundle.putString("parse.CdnNameHeader","d");
        optBundle.putBoolean("parse.CdnNode",true);
        optBundle.putStringArrayList("parse.CdnNodeList",cdnNodeList);
        fakeBundle.put("parse.CdnNodeList",cdnNodeList);
        optBundle.putString("network.IP","f");
        optBundle.putString("network.Isp","g");
        optBundle.putString("network.connectionType","h");
        optBundle.putBoolean("network.obfuscateIp",true);
        optBundle.putString("device.code","i");
        optBundle.putString("content.resource","j");
        optBundle.putBoolean("content.isLive",true);
        optBundle.putString("content.title","k");
        optBundle.putString("content.program","l");
        optBundle.putDouble("content.duration",1.0);
        optBundle.putString("content.transactionCode","m");
        optBundle.putLong("content.bitrate",2L);
        optBundle.putLong("content.throughput",3L);
        optBundle.putString("content.rendition","n");
        optBundle.putString("content.cdn","o");
        optBundle.putDouble("content.fps",4.0);
        optBundle.putBundle("content.metadata",metadataBundle);
        fakeBundle.put("content.metadata",metadataBundle);
        optBundle.putBundle("ad.metadata",adMetadataBundle);
        fakeBundle.put("ad.metadata",adMetadataBundle);
        optBundle.putBoolean("ad.ignore",false);
        optBundle.putInt("ad.afterStop",0);
        optBundle.putBoolean("autoDetectBackground",false);
        optBundle.putBoolean("autoStart",true);
        optBundle.putBoolean("offline",false);
        optBundle.putBoolean("isInfinity", false);
        optBundle.putString("custom.dimensions.1","t");
        optBundle.putString("custom.dimensions.2","u");
        optBundle.putString("custom.dimensions.3","v");
        optBundle.putString("custom.dimensions.4","w");
        optBundle.putString("custom.dimensions.5","x");
        optBundle.putString("custom.dimensions.6","y");
        optBundle.putString("custom.dimensions.7","z");
        optBundle.putString("custom.dimensions.8","aa");
        optBundle.putString("custom.dimensions.9","ab");
        optBundle.putString("custom.dimensions.10","ac");
        optBundle.putString("custom.dimensions.11","ad");
        optBundle.putString("custom.dimensions.12","ae");
        optBundle.putString("custom.dimensions.13","af");
        optBundle.putString("custom.dimensions.14","ag");
        optBundle.putString("custom.dimensions.15","ah");
        optBundle.putString("custom.dimensions.16","ai");
        optBundle.putString("custom.dimensions.17","aj");
        optBundle.putString("custom.dimensions.18","ak");
        optBundle.putString("custom.dimensions.19","al");
        optBundle.putString("custom.dimensions.20","am");

        o = new Options(optBundle);
    }
}