package com.npaw.youbora.lib6.persistence;

import com.npaw.youbora.lib6.persistence.entity.Event;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

/**
 * Created by Enrique on 28/12/2017.
 */

public class EventTest {

    private Event event;

    @Before
    public void setUp(){
        event = new Event();
        event = new Event("Doe Jhon",2L,-1);
    }

    @Test
    public void setEventProperties(){

        assertEquals((long)event.getDateUpdate(),2L);
        assertEquals(event.getJsonEvents(),"Doe Jhon");
        assertEquals(event.getOfflineId(),-1);

        event.setDateUpdate(1L);
        event.setJsonEvents("John Doe");
        event.setOfflineId(0);
        event.setUid(1);

        assertEquals((long)event.getDateUpdate(),1L);
        assertEquals(event.getJsonEvents(),"John Doe");
        assertEquals(event.getOfflineId(),0);
        assertEquals(event.getUid(),1);
    }

}
