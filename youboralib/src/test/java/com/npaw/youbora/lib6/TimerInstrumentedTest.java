package com.npaw.youbora.lib6;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)

public class TimerInstrumentedTest {

    private long time;

    @Test
    public void testIsRunning() throws InterruptedException {

        final CountDownLatch latch = new CountDownLatch(1);

        Timer t = new Timer(null);

        assertEquals(false, t.isRunning());

        t.start();

        assertEquals(true, t.isRunning());

        t.stop();

        assertEquals(false, t.isRunning());

        latch.countDown();

        latch.await();
    }


    @Test
    public void testCallbackTicks() throws InterruptedException {

        final int ticks = 2;
        final CountDownLatch latchTicks = new CountDownLatch(ticks);

        Timer t = new Timer(new Timer.TimerEventListener() {
            @Override
            public void onTimerEvent(long delta) {
                latchTicks.countDown();
            }
        }, 1000);

        time = System.currentTimeMillis();
        t.start();

        latchTicks.await(1, TimeUnit.SECONDS);

        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();

        latchTicks.await(1, TimeUnit.SECONDS);

        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();

        assertEquals(0, latchTicks.getCount());

        long diff = System.currentTimeMillis() - time;

        assertEquals(ticks * 1000, diff, ticks * 1000 * 0.05);
    }

    @Test
    public void testSetInterval() throws InterruptedException {

        final CountDownLatch latchTicks = new CountDownLatch(1);

        Timer t = new Timer(new Timer.TimerEventListener() {
            @Override
            public void onTimerEvent(long delta) {
                latchTicks.countDown();
            }
        }, 1000);

        t.setInterval(500);

        time = System.currentTimeMillis();
        t.start();

        latchTicks.await(500, TimeUnit.MILLISECONDS);

        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();

        assertEquals(0, latchTicks.getCount());

        long diff = System.currentTimeMillis() - time;

        assertEquals(500, diff, 50);
    }

    @Test
    public void getChrono() throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);

        Timer t = new Timer(null);

        Chrono c = t.getChrono();
        assertNotNull(c);
        t.start();
        assertEquals(c, t.getChrono());
        t.stop();
        assertEquals(c, t.getChrono());

        latch.countDown();

        latch.await();
    }

    @Test
    public void startTwice() {
        Timer t = new Timer(null);

        t.start();
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();

        t.start();
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();
    }

    @Test
    public void stopBeforeStart() {
        Timer t = new Timer(null);
        t.stop();
    }

    @Test
    public void addTimerCallbackWhenCallbackNotNull() {
        Timer t = new Timer(new Timer.TimerEventListener() {
            @Override
            public void onTimerEvent(long delta) {}
        });

        t.addTimerCallback(new Timer.TimerEventListener() {
            @Override
            public void onTimerEvent(long delta) {}
        });
    }
}
