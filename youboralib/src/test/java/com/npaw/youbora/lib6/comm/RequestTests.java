package com.npaw.youbora.lib6.comm;

import org.junit.Test;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;

public class RequestTests {

    @Test
    public void testRequestFields() {
        Request r = new Request("example.com", "/service");

        // Check default values
        assertEquals(r.getHost(), "example.com");
        assertEquals(r.getService(), "/service");
        assertEquals(r.getMethod(), Request.METHOD_GET);
        assertEquals(r.getParams(), null);
        assertEquals(r.getParam("a"), null);
        assertEquals(r.getRequestHeaders(), null);
        assertEquals(r.getUrl(),"example.com/service");

        // Change properties
        r.setHost("abc.com");
        r.setService("/anotherService");
        r.setMethod(Request.METHOD_DELETE);
        r.setMaxRetries(10);
        r.setRetryInterval(10000);
        Map<String, Object> params = new HashMap<>();
        params.put("param1", "value1");
        params.put("param2", "value2");
        params.put("param3", "value3");
        r.setParams(params);
        r.setParam("param4", "value4");

        Map<String, String> reqHeaders = new HashMap<>();
        reqHeaders.put("header1", "valueheader1");
        reqHeaders.put("header2", "valueheader2");
        r.setRequestHeaders(reqHeaders);

        // Check new values
        assertEquals(r.getHost(), "abc.com");
        assertEquals(r.getService(), "/anotherService");
        assertEquals(r.getMethod(), Request.METHOD_DELETE);
        assertEquals(r.getMaxRetries(), 10);
        assertEquals(r.getRetryInterval(), 10000);

        params = r.getParams();

        assertEquals(params.size(), 4);
        assertEquals(params.get("param1"), "value1");
        assertEquals(params.get("param2"), "value2");
        assertEquals(params.get("param3"), "value3");
        assertEquals(params.get("param4"), "value4");

        reqHeaders = r.getRequestHeaders();
        assertEquals(reqHeaders.get("header1"), "valueheader1");
        assertEquals(reqHeaders.get("header2"), "valueheader2");

        assertNull(r.getParam("unexisting_key"));
    }

    @Test
    public void testRemoveGlobalListeners() {

        Request.RequestSuccessListener successListener = new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response,
                                         Map<String, Object> listenerParams) {}
        };

        Request.RequestErrorListener errorListener = new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {}
        };

        Request.addOnEverySuccessListener(successListener);
        Request.addOnEveryErrorListener(errorListener);

        assertFalse(Request.removeOnEveryErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {}
        }));

        assertFalse(Request.removeOnEverySuccessListener(new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response,
                                         Map<String, Object> listenerParams) {}
        }));

        assertTrue(Request.removeOnEveryErrorListener(errorListener));
        assertTrue(Request.removeOnEverySuccessListener(successListener));

        assertFalse(Request.removeOnEveryErrorListener(null));
        assertFalse(Request.removeOnEverySuccessListener(null));
    }

    @Test
    public void settersAndGetters() {
        Request r = new Request("example.com", "/service");

        // Setters
        r.setBody("");
        r.setSuccessListenerParams(new HashMap<String, Object>());

        // Getters
        assertEquals("", r.getBody());
        assertEquals(new HashMap<String, Object>(), r.getSucessListenerParams());
    }
}
