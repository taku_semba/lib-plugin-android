package com.npaw.youbora.lib6.infinity;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentCaptor;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)

public class InfinityTest {
    private Plugin plugin;
    private Infinity infinityFactory;

    @Before
    public void setUp() {
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        plugin = new Plugin(new Options());

        infinityFactory = plugin.getInfinity(RuntimeEnvironment.application);
    }

    @After
    public void tearDown() { infinityFactory.end(); }

    @Test
    public void testBeginMethod(){
        Infinity.InfinityEventListener mockListener = mock(Infinity.InfinityEventListener.class);
        infinityFactory.addEventListener(mockListener);

        assertFalse(infinityFactory.getFlags().isStarted());

        infinityFactory.begin("screenName");
        verify(mockListener).onSessionStart(nullable(String.class), nullable(Map.class),
                nullable(String.class));
    }

    @Test
    public void testBeginMethodWithParams() {
        Infinity.InfinityEventListener mockListener = mock(Infinity.InfinityEventListener.class);
        infinityFactory.addEventListener(mockListener);

        assertFalse(infinityFactory.getFlags().isStarted());

        ArgumentCaptor<String> screenNameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Map> dimensionsCaptor = ArgumentCaptor.forClass(Map.class);
        ArgumentCaptor<String> parentIdCaptor = ArgumentCaptor.forClass(String.class);

        String screenName = "screenName";
        HashMap dimensMap = new HashMap<String, String>(1){{
            put("key","value");
        }};
        String parentId = "parentId";

        infinityFactory.begin(screenName, dimensMap, parentId);

        assertTrue(infinityFactory.getFlags().isStarted());

        verify(mockListener, times(1))
                .onSessionStart(screenNameCaptor.capture(),
                        dimensionsCaptor.capture(),
                        parentIdCaptor.capture());

        assertEquals(screenName, screenNameCaptor.getValue());
        assertTrue(dimensionsCaptor.getValue().containsKey("key"));
        assertEquals(dimensMap.get("key"), dimensionsCaptor.getValue().get("key"));
        assertEquals(parentId, parentIdCaptor.getValue());
    }

    @Test
    public void testBeginMethodWithNullParam() {
        Infinity.InfinityEventListener mockListener = mock(Infinity.InfinityEventListener.class);
        infinityFactory.addEventListener(mockListener);

        assertFalse(infinityFactory.getFlags().isStarted());

        ArgumentCaptor<String> screenNameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Map> dimensionsCaptor = ArgumentCaptor.forClass(Map.class);
        ArgumentCaptor<String> parentIdCaptor = ArgumentCaptor.forClass(String.class);

        infinityFactory.begin(null);
        assertTrue(infinityFactory.getFlags().isStarted());
        verify(mockListener, times(1))
                .onSessionStart(screenNameCaptor.capture(),
                        dimensionsCaptor.capture(),
                        parentIdCaptor.capture());

        assertNull(screenNameCaptor.getValue());
        assertNull(dimensionsCaptor.getValue());
        assertNull(parentIdCaptor.getValue());
    }
}
