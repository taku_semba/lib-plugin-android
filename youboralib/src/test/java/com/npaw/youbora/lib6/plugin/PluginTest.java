package com.npaw.youbora.lib6.plugin;

import android.content.Context;

import com.npaw.youbora.lib6.BuildConfig;
import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.adapter.PlaybackChronos;
import com.npaw.youbora.lib6.adapter.PlaybackFlags;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.Transform;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)

public class PluginTest extends PluginMocker {
    private YouboraLog.YouboraLogger mockLogger;

    @Before
    public void setUp() {
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        mockLogger = mock(YouboraLog.YouboraLogger.class);
        YouboraLog.addLogger(mockLogger);
    }

    @Test
    public void testConstructorPerformedOperations() {
        verify(mockViewTransform, times(1)).addTransformDoneListener(any(Transform.TransformDoneListener.class));
        verify(mockViewTransform, times(1)).init();
        verify(mockAdapter, times(1)).addEventListener(any(PlayerAdapter.AdapterEventListener.class));
    }

    @Test
    public void testSetOptions() {
        // Mock options
        Options o = mock(Options.class);
        when(o.getAccountCode()).thenReturn("a");

        Plugin p = new TestPlugin(o);
        assertEquals("a", p.getOptions().getAccountCode());

        p.setOptions(o);
        assertEquals("a", p.getOptions().getAccountCode());
    }

    @Test
    public void testAddAndRemoveAdapters() {
        PlayerAdapter mockAdapter = mock(PlayerAdapter.class);
        p.setAdapter(mockAdapter);

        assertEquals(mockAdapter, p.getAdapter());

        verify(mockAdapter, times(1))
                .addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        PlayerAdapter mockAdapter2 = mock(PlayerAdapter.class);
        p.setAdapter(mockAdapter2);

        assertEquals(mockAdapter2, p.getAdapter());

        verify(mockAdapter, times(1)).dispose();
        verify(mockAdapter, times(1))
                .removeEventListener(any(PlayerAdapter.AdapterEventListener.class));

        verify(mockAdapter2, times(1))
                .addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        p.removeAdapter();
        verify(mockAdapter2, times(1)).dispose();
        verify(mockAdapter2, times(1))
                .removeEventListener(any(PlayerAdapter.AdapterEventListener.class));
    }

    @Test
    public void testAddAndRemoveAdsAdapters() {
        PlayerAdapter mockAdapter = mock(PlayerAdapter.class);
        p.setAdsAdapter(mockAdapter);

        assertEquals(mockAdapter, p.getAdsAdapter());

        verify(mockAdapter, times(1))
                .addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        PlayerAdapter mockAdapter2 = mock(PlayerAdapter.class);
        p.setAdsAdapter(mockAdapter2);

        assertEquals(mockAdapter2, p.getAdsAdapter());

        verify(mockAdapter, times(1)).dispose();
        verify(mockAdapter, times(1))
                .removeEventListener(any(PlayerAdapter.AdapterEventListener.class));

        verify(mockAdapter2, times(1))
                .addEventListener(any(PlayerAdapter.AdapterEventListener.class));

        p.removeAdsAdapter();
        verify(mockAdapter2, times(1)).dispose();
        verify(mockAdapter2, times(1))
                .removeEventListener(any(PlayerAdapter.AdapterEventListener.class));
    }


    @Test
    public void testEnableDisable() {
        p.enable();
        verify(mockOptions).setEnabled(eq(true));

        p.disable();
        verify(mockOptions).setEnabled(eq(false));
    }

    @Test
    public void testPreloads() {
        p.firePreloadBegin();
        p.firePreloadBegin();
        verify(mockChrono, times(1)).start();

        p.firePreloadEnd();
        p.firePreloadEnd();
        verify(mockChrono, times(1)).stop();
    }

    // Test get info
    @Test
    public void testGetHost() {
        when(mockOptions.getHost()).thenReturn("http://host.com");
        when(mockOptions.isHttpSecure()).thenReturn(true);

        assertEquals("https://host.com", p.getHost());
    }

    @Test
    public void testParseHls() {
        when(mockOptions.isParseHls()).thenReturn(true);
        assertTrue(p.isParseHls());

        when(mockOptions.isParseHls()).thenReturn(false);
        assertFalse(p.isParseHls());
    }

    @Test
    public void testCdnNode() {
        when(mockOptions.isParseCdnNode()).thenReturn(true);
        assertTrue(p.isParseCdnNode());

        when(mockOptions.isParseCdnNode()).thenReturn(false);
        assertFalse(p.isParseHls());
    }

    @Test
    public void testParseCdnNodeList() {
        ArrayList<String> list = new ArrayList<>();
        list.add("item1");
        list.add("item2");
        list.add("item3");
        when(mockOptions.getParseCdnNodeList()).thenReturn(null);
        assertNull(p.getParseCdnNodeList());

        when(mockOptions.getParseCdnNodeList()).thenReturn(list);
        assertEquals(list, p.getParseCdnNodeList());
    }

    @Test
    public void testParseCdnNodeHeader() {
        when(mockOptions.getParseCdnNameHeader()).thenReturn("x-header");
        assertEquals("x-header", p.getParseCdnNodeNameHeader());
    }

    @Test
    public void testPlayhead() {
        // Valid values
        when(mockAdapter.getPlayhead()).thenReturn(-10.0);
        assertEquals(Double.valueOf(-10.0), p.getPlayhead());

        when(mockAdapter.getPlayhead()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getPlayhead());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdapter.getPlayhead()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getPlayhead());
        }
    }

    @Test
    public void testRate() {
        // Valid values
        when(mockAdapter.getPlayrate()).thenReturn(-10.0);
        assertEquals(Double.valueOf(-10.0), p.getPlayrate());

        when(mockAdapter.getPlayrate()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getPlayrate());

        when(mockAdapter.getPlayrate()).thenReturn(-0.5);
        assertEquals(Double.valueOf(-0.5), p.getPlayrate());

        when(mockAdapter.getPlayrate()).thenReturn(0.5);
        assertEquals(Double.valueOf(0.5), p.getPlayrate());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdapter.getPlayrate()).thenReturn(d);
            assertEquals(Double.valueOf(1.0), p.getPlayrate());
        }
    }

    @Test
    public void testFps() {
        when(mockOptions.getContentFps()).thenReturn(25.0);
        when(mockAdapter.getFramesPerSecond()).thenReturn(15.0);
        assertEquals(Double.valueOf(25.0), p.getFramesPerSecond());

        when(mockOptions.getContentFps()).thenReturn(null);
        when(mockAdapter.getFramesPerSecond()).thenReturn(15.5);
        assertEquals(Double.valueOf(15.5), p.getFramesPerSecond());

        when(mockOptions.getContentFps()).thenReturn(null);
        when(mockAdapter.getFramesPerSecond()).thenReturn(null);
        assertNull(p.getFramesPerSecond());
    }

    @Test
    public void testDroppedFrames() {
        // Valid value
        when(mockAdapter.getDroppedFrames()).thenReturn(10);
        assertEquals(Integer.valueOf(10), p.getDroppedFrames());

        // Invalid values
        for (Integer i : INTEGER_INVALID) {
            when(mockAdapter.getDroppedFrames()).thenReturn(i);
            assertEquals(Integer.valueOf(0), p.getDroppedFrames());
        }
    }

    @Test
    public void testDuration() {
        when(mockOptions.getContentDuration()).thenReturn(null);

        // Valid values
        when(mockAdapter.getDuration()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getDuration());

        when(mockAdapter.getDuration()).thenReturn(0.5);
        assertEquals(Double.valueOf(0.5), p.getDuration());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdapter.getDuration()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getDuration());
        }

        // Test options prevalence over adapter
        when(mockOptions.getContentDuration()).thenReturn(1.0);

        when(mockAdapter.getDuration()).thenReturn(2.0);
        assertEquals(Double.valueOf(1.0), p.getDuration());
    }

    @Test
    public void testBitrate() {
        when(mockOptions.getContentBitrate()).thenReturn(null);

        // Valid values
        when(mockAdapter.getBitrate()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getBitrate());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdapter.getBitrate()).thenReturn(l);
            assertEquals(Long.valueOf(-1), p.getBitrate());
        }

        // Test options prevalence over adapter
        when(mockOptions.getContentBitrate()).thenReturn(1000000L);

        when(mockAdapter.getBitrate()).thenReturn(2000000L);
        assertEquals(Long.valueOf(1000000), p.getBitrate());
    }

    @Test
    public void testThroughput() {
        when(mockOptions.getContentThroughput()).thenReturn(null);

        // Valid values
        when(mockAdapter.getThroughput()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getThroughput());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdapter.getThroughput()).thenReturn(l);
            assertEquals(Long.valueOf(-1), p.getThroughput());
        }

        // Test options prevalence over adapter
        when(mockOptions.getContentThroughput()).thenReturn(1000000L);

        when(mockAdapter.getThroughput()).thenReturn(2000000L);
        assertEquals(Long.valueOf(1000000), p.getThroughput());
    }

    @Test
    public void testRendition() {
        when(mockOptions.getContentRendition()).thenReturn(null);
        when(mockAdapter.getRendition()).thenReturn("1Mbps");
        assertEquals("1Mbps", p.getRendition());

        when(mockAdapter.getRendition()).thenReturn("");
        assertEquals("", p.getRendition());

        when(mockAdapter.getRendition()).thenReturn(null);
        assertNull(p.getRendition());

        // Test options prevalence over adapter
        when(mockOptions.getContentRendition()).thenReturn("2Mbps");
        when(mockAdapter.getRendition()).thenReturn("1Mbps");
        assertEquals("2Mbps", p.getRendition());

        // unless it's empty
        when(mockOptions.getContentRendition()).thenReturn("");
        assertEquals("1Mbps", p.getRendition());
    }

    @Test
    public void testTitle() {
        when(mockOptions.getContentTitle()).thenReturn(null);
        when(mockAdapter.getTitle()).thenReturn("batman");
        assertEquals("batman", p.getTitle());

        when(mockAdapter.getTitle()).thenReturn("");
        assertEquals("", p.getTitle());

        when(mockAdapter.getTitle()).thenReturn(null);
        assertNull(p.getTitle());

        // Test options prevalence over adapter
        when(mockOptions.getContentTitle()).thenReturn("iron man");
        when(mockAdapter.getTitle()).thenReturn("batman");
        assertEquals("iron man", p.getTitle());

        // unless it's empty
        when(mockOptions.getContentTitle()).thenReturn("");
        assertEquals("batman", p.getTitle());
    }

    @Test
    public void testProgram() {
        when(mockOptions.getProgram()).thenReturn(null);
        when(mockAdapter.getProgram()).thenReturn("episode 1");
        assertEquals("episode 1", p.getProgram());

        when(mockAdapter.getProgram()).thenReturn("");
        assertEquals("", p.getProgram());

        when(mockAdapter.getProgram()).thenReturn(null);
        assertNull(p.getProgram());

        // Test options prevalence over adapter
        when(mockOptions.getProgram()).thenReturn("episode 1");
        when(mockAdapter.getProgram()).thenReturn("episode 2");
        assertEquals("episode 1", p.getProgram());

        // unless it's empty
        when(mockOptions.getProgram()).thenReturn("");
        assertEquals("episode 2", p.getProgram());
    }

    @Test
    public void testLive() {
        when(mockOptions.getContentIsLive()).thenReturn(null);

        // Default
        when(mockAdapter.getIsLive()).thenReturn(null);
        assertFalse(p.getIsLive());

        when(mockAdapter.getIsLive()).thenReturn(false);
        assertFalse(p.getIsLive());

        when(mockAdapter.getIsLive()).thenReturn(true);
        assertTrue(p.getIsLive());

        // Test options prevalence over adapter
        when(mockOptions.getContentIsLive()).thenReturn(false);
        assertFalse(p.getIsLive());

        when(mockOptions.getContentIsLive()).thenReturn(true);
        when(mockAdapter.getIsLive()).thenReturn(false);
        assertTrue(p.getIsLive());
    }

    @Test
    public void testGetResource() {
        when(mockResourceTransform.isBlocking(nullable(Request.class))).thenReturn(true);
        when(mockOptions.getContentResource()).thenReturn("ResourceFromOptions");
        when(mockResourceTransform.getResource()).thenReturn("ResourceFromTransform");
        when(mockAdapter.getResource()).thenReturn("ResourceFromAdapter");
        assertEquals("ResourceFromOptions", p.getResource());

        when(mockResourceTransform.isBlocking(nullable(Request.class))).thenReturn(false);
        assertEquals("ResourceFromTransform", p.getResource());

        when(mockOptions.getContentResource()).thenReturn(null);
        when(mockResourceTransform.getResource()).thenReturn(null);
        assertEquals("ResourceFromAdapter", p.getResource());

        when(mockAdapter.getResource()).thenReturn(null);
        assertNull(p.getResource());
    }

    @Test
    public void testTransactionCode() {
        when(mockOptions.getContentTransactionCode()).thenReturn("transactionCode");

        assertEquals("transactionCode", p.getTransactionCode());

        when(mockOptions.getContentTransactionCode()).thenReturn(null);

        assertNull(p.getTransactionCode());
    }

    @Test
    public void testPlayerVersion() {
        when(mockAdapter.getPlayerVersion()).thenReturn(null);

        assertEquals("", p.getPlayerVersion());

        when(mockAdapter.getPlayerVersion()).thenReturn("1.2.3");

        assertEquals("1.2.3", p.getPlayerVersion());
    }

    @Test
    public void testPlayerName() {
        when(mockAdapter.getPlayerName()).thenReturn(null);

        assertEquals("", p.getPlayerName());

        when(mockAdapter.getPlayerName()).thenReturn("player-name");

        assertEquals("player-name", p.getPlayerName());
    }

    @Test
    public void testCdn() {
        when(mockResourceTransform.isBlocking(nullable(Request.class))).thenReturn(true);
        when(mockOptions.getContentCdn()).thenReturn("CdnFromOptions");
        when(mockResourceTransform.getCdnName()).thenReturn("CdnFromTransform");
        assertEquals("CdnFromOptions", p.getCdn());

        when(mockResourceTransform.isBlocking(nullable(Request.class))).thenReturn(false);
        assertEquals("CdnFromTransform", p.getCdn());

        when(mockResourceTransform.getCdnName()).thenReturn(null);
        assertEquals("CdnFromOptions", p.getCdn());

        when(mockOptions.getContentCdn()).thenReturn(null);
        assertNull(p.getCdn());
    }

    @Test
    public void testPluginVersion() {
        when(mockAdapter.getVersion()).thenReturn(null);
        assertEquals(BuildConfig.VERSION_NAME+"-adapterless", p.getPluginVersion());

        when(mockAdapter.getVersion()).thenReturn("6.0.0-CustomPlugin");
        assertEquals("6.0.0-CustomPlugin", p.getPluginVersion());

        p.removeAdapter();
        assertEquals(BuildConfig.VERSION_NAME+"-adapterless", p.getPluginVersion());
    }

    @Test
    public void testCustomDimensions() {
        when(mockOptions.getCustomDimension1()).thenReturn("value-custom-value1");
        when(mockOptions.getCustomDimension2()).thenReturn("value-custom-value2");
        when(mockOptions.getCustomDimension3()).thenReturn("value-custom-value3");
        when(mockOptions.getCustomDimension4()).thenReturn("value-custom-value4");
        when(mockOptions.getCustomDimension5()).thenReturn("value-custom-value5");
        when(mockOptions.getCustomDimension6()).thenReturn("value-custom-value6");
        when(mockOptions.getCustomDimension7()).thenReturn("value-custom-value7");
        when(mockOptions.getCustomDimension8()).thenReturn("value-custom-value8");
        when(mockOptions.getCustomDimension9()).thenReturn("value-custom-value9");
        when(mockOptions.getCustomDimension10()).thenReturn("value-custom-value10");

        assertEquals("value-custom-value1", p.getCustomDimension1());
        assertEquals("value-custom-value2", p.getCustomDimension2());
        assertEquals("value-custom-value3", p.getCustomDimension3());
        assertEquals("value-custom-value4", p.getCustomDimension4());
        assertEquals("value-custom-value5", p.getCustomDimension5());
        assertEquals("value-custom-value6", p.getCustomDimension6());
        assertEquals("value-custom-value7", p.getCustomDimension7());
        assertEquals("value-custom-value8", p.getCustomDimension8());
        assertEquals("value-custom-value9", p.getCustomDimension9());
        assertEquals("value-custom-value10", p.getCustomDimension10());

        when(mockOptions.getCustomDimension1()).thenReturn(null);
        when(mockOptions.getCustomDimension2()).thenReturn(null);
        when(mockOptions.getCustomDimension3()).thenReturn(null);
        when(mockOptions.getCustomDimension4()).thenReturn(null);
        when(mockOptions.getCustomDimension5()).thenReturn(null);
        when(mockOptions.getCustomDimension6()).thenReturn(null);
        when(mockOptions.getCustomDimension7()).thenReturn(null);
        when(mockOptions.getCustomDimension8()).thenReturn(null);
        when(mockOptions.getCustomDimension9()).thenReturn(null);
        when(mockOptions.getCustomDimension10()).thenReturn(null);

        assertNull(p.getCustomDimension1());
        assertNull(p.getCustomDimension2());
        assertNull(p.getCustomDimension3());
        assertNull(p.getCustomDimension4());
        assertNull(p.getCustomDimension5());
        assertNull(p.getCustomDimension6());
        assertNull(p.getCustomDimension7());
        assertNull(p.getCustomDimension8());
        assertNull(p.getCustomDimension9());
        assertNull(p.getCustomDimension10());
    }

    @Test
    public void testAdPlayerVersion() {
        when(mockAdAdapter.getPlayerVersion()).thenReturn(null);
        assertEquals("", p.getAdPlayerVersion());

        when(mockAdAdapter.getPlayerVersion()).thenReturn("player-version");
        assertEquals("player-version", p.getAdPlayerVersion());

        p.removeAdsAdapter();
        assertEquals("", p.getAdPlayerVersion());
    }

    @Test
    public void testAdPosition() {
        when(mockAdAdapter.getPosition()).thenReturn(PlayerAdapter.AdPosition.PRE);
        assertEquals("pre", p.getAdPosition());

        when(mockAdAdapter.getPosition()).thenReturn(PlayerAdapter.AdPosition.POST);
        assertEquals("post", p.getAdPosition());

        when(mockAdAdapter.getPosition()).thenReturn(PlayerAdapter.AdPosition.MID);
        assertEquals("mid", p.getAdPosition());

        // If ad position is unknown, the plugin will try to infer the position depending on
        // the Buffered status of the adapter. This is a workaround and postrolls will be detected
        // as midrolls
        when(mockAdAdapter.getPosition()).thenReturn(PlayerAdapter.AdPosition.UNKNOWN);

        PlaybackFlags flags = new PlaybackFlags();
        flags.setJoined(false);
        when(mockAdapter.getFlags()).thenReturn(flags);

        assertEquals("pre", p.getAdPosition());

        flags.setJoined(true);

        assertEquals("mid", p.getAdPosition());

        // No ads adapter, this should be the same as it returning UNKNOWN, "mid" expected again
        p.removeAdsAdapter();
        assertEquals("mid", p.getAdPosition());

        // No adapter at all
        p.removeAdapter();
        assertEquals("unknown", p.getAdPosition());
    }

    @Test
    public void testAdPlayhead() {
        // Valid values
        when(mockAdAdapter.getPlayhead()).thenReturn(-10.0);
        assertEquals(Double.valueOf(-10.0), p.getAdPlayhead());

        when(mockAdAdapter.getPlayhead()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getAdPlayhead());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdAdapter.getPlayhead()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getAdPlayhead());
        }
    }

    @Test
    public void testAdDuration() {
        // Valid values
        when(mockAdAdapter.getDuration()).thenReturn(10.0);
        assertEquals(Double.valueOf(10.0), p.getAdDuration());

        when(mockAdAdapter.getDuration()).thenReturn(0.5);
        assertEquals(Double.valueOf(0.5), p.getAdDuration());

        // Invalid values
        for (Double d : DOUBLE_INVALID) {
            when(mockAdAdapter.getDuration()).thenReturn(d);
            assertEquals(Double.valueOf(0.0), p.getAdDuration());
        }
    }

    @Test
    public void testAdBitrate() {
        // Valid values
        when(mockAdAdapter.getBitrate()).thenReturn(1000000L);
        assertEquals(Long.valueOf(1000000), p.getAdBitrate());

        // Invalid values
        for (Long l : LONG_INVALID) {
            when(mockAdAdapter.getBitrate()).thenReturn(l);
            assertEquals(Long.valueOf(-1), p.getAdBitrate());
        }
    }

    @Test
    public void testAdTitle() {
        when(mockAdAdapter.getTitle()).thenReturn("batman");
        assertEquals("batman", p.getAdTitle());

        when(mockAdAdapter.getTitle()).thenReturn("");
        assertEquals("", p.getAdTitle());

        when(mockAdAdapter.getTitle()).thenReturn(null);
        assertNull(p.getAdTitle());
    }

    @Test
    public void testAdGetResource() {
        when(mockAdAdapter.getResource()).thenReturn("AdResourceFromAdapter");
        assertEquals("AdResourceFromAdapter", p.getAdResource());

        when(mockAdAdapter.getResource()).thenReturn(null);
        assertEquals(null, p.getAdResource());
    }

    @Test
    public void testAdPluginVersion() {
        when(mockAdAdapter.getVersion()).thenReturn(null);
        assertNull(p.getAdAdapterVersion());

        when(mockAdAdapter.getVersion()).thenReturn("6.0.0-CustomAdapter");
        assertEquals("6.0.0-CustomAdapter", p.getAdAdapterVersion());

        p.removeAdsAdapter();
        assertEquals(null, p.getAdAdapterVersion());
    }

    @Test
    public void testCdnTraffic() {
        when(mockAdapter.getCdnTraffic()).thenReturn(1);
        assertEquals(Long.valueOf(1), Long.valueOf(p.getCdnTraffic()));

        for (Integer i : INTEGER_INVALID) {
            when(mockAdapter.getCdnTraffic()).thenReturn(i);
            assertEquals(Integer.valueOf(0), p.getCdnTraffic());
        }
    }

    @Test
    public void testP2PTraffic() {
        when(mockAdapter.getP2PTraffic()).thenReturn(1);
        assertEquals(Long.valueOf(1), Long.valueOf(p.getP2PTraffic()));

        for (Integer i : INTEGER_INVALID) {
            when(mockAdapter.getP2PTraffic()).thenReturn(i);
            assertEquals(Integer.valueOf(0), p.getP2PTraffic());
        }
    }

    @Test
    public void testUploadTraffic() {
        when(mockAdapter.getUploadTraffic()).thenReturn(1);
        assertEquals(Long.valueOf(1), Long.valueOf(p.getUploadTraffic()));

        for (Integer i : INTEGER_INVALID) {
            when(mockAdapter.getUploadTraffic()).thenReturn(i);
            assertEquals(Integer.valueOf(0), p.getUploadTraffic());
        }
    }

    @Test
    public void testIsP2PEnabled() {
        when(mockAdapter.getIsP2PEnabled()).thenReturn(null);
        assertNull(p.getIsP2PEnabled());

        when(mockAdapter.getIsP2PEnabled()).thenReturn(false);
        assertFalse(p.getIsP2PEnabled());

        when(mockAdapter.getIsP2PEnabled()).thenReturn(true);
        assertTrue(p.getIsP2PEnabled());
    }

    @Test
    public void testIp() {
        assertNull(p.getIp());
        when(mockOptions.getNetworkIP()).thenReturn("1.2.3.4");
        assertEquals("1.2.3.4", p.getIp());
    }

    @Test
    public void testIsp() {
        assertNull(p.getIsp());
        when(mockOptions.getNetworkIsp()).thenReturn("ISP");
        assertEquals("ISP", p.getIsp());
    }

    @Test
    public void testConnectionType() {
        assertNull(p.getConnectionType());
        when(mockOptions.getNetworkConnectionType()).thenReturn("DSL");
        assertEquals("DSL", p.getConnectionType());
    }

    @Test
    public void testDeviceCode() {
        assertNull(p.getDeviceCode());
        when(mockOptions.getDeviceCode()).thenReturn("42");
        assertEquals("42", p.getDeviceCode());
    }

    @Test
    public void testAccountCode() {
        assertNull(p.getAccountCode());
        when(mockOptions.getAccountCode()).thenReturn("accountcode");
        assertEquals("accountcode", p.getAccountCode());
    }

    @Test
    public void testUsername() {
        assertNull(p.getUsername());
        when(mockOptions.getUsername()).thenReturn("username");
        assertEquals("username", p.getUsername());
    }

    @Test
    public void testNodeHost() {
        assertNull(p.getNodeHost());
        when(mockResourceTransform.getNodeHost()).thenReturn("nodeHost");
        assertEquals("nodeHost", p.getNodeHost());
    }

    @Test
    public void testNodeType() {
        assertNull(p.getNodeType());
        when(mockResourceTransform.getNodeType()).thenReturn("type");
        assertEquals("type", p.getNodeType());
    }

    @Test
    public void testNodeTypeString() {
        assertNull(p.getNodeTypeString());
        when(mockResourceTransform.getNodeTypeString()).thenReturn("typeString");
        assertEquals("typeString", p.getNodeTypeString());
    }

    @Test
    public void testAppName() {
        assertNull(p.getAppName());
        when(mockOptions.getAppName()).thenReturn("appName");
        assertEquals("appName", p.getAppName());
    }

    @Test
    public void testAppReleaseVersion() {
        assertNull(p.getAppReleaseVersion());
        when(mockOptions.getAppReleaseVersion()).thenReturn("appReleaseVersion");
        assertEquals("appReleaseVersion", p.getAppReleaseVersion());
    }

    @Test
    public void testChronoTimes() {
        // Init and preload chronos, don't depend on the adapter
        when(mockChrono.getDeltaTime(anyBoolean())).thenReturn(100L);
        assertEquals(100, p.getInitDuration());

        when(mockChrono.getDeltaTime(anyBoolean())).thenReturn(200L);
        assertEquals(200,p.getPreloadDuration());

        // Adapter chronos
        PlaybackChronos c = new PlaybackChronos();
        c.buffer = mock(Chrono.class);
        c.join = mock(Chrono.class);
        c.seek = mock(Chrono.class);
        c.pause = mock(Chrono.class);
        c.total = mock(Chrono.class);

        when(c.buffer.getDeltaTime(anyBoolean())).thenReturn(100L);
        when(c.join.getDeltaTime(anyBoolean())).thenReturn(200L);
        when(c.seek.getDeltaTime(anyBoolean())).thenReturn(300L);
        when(c.pause.getDeltaTime(anyBoolean())).thenReturn(400L);
        when(c.total.getDeltaTime(anyBoolean())).thenReturn(500L);

        when(mockAdapter.getChronos()).thenReturn(c);

        assertEquals(100, p.getBufferDuration());
        assertEquals(200, p.getJoinDuration());
        assertEquals(300, p.getSeekDuration());
        assertEquals(400, p.getPauseDuration());

        // Change values to test the ads
        when(c.buffer.getDeltaTime(anyBoolean())).thenReturn(1000L);
        when(c.join.getDeltaTime(anyBoolean())).thenReturn(2000L);
        when(c.seek.getDeltaTime(anyBoolean())).thenReturn(3000L);
        when(c.pause.getDeltaTime(anyBoolean())).thenReturn(4000L);
        when(c.total.getDeltaTime(anyBoolean())).thenReturn(5000L);

        when(mockAdAdapter.getChronos()).thenReturn(c);

        assertEquals(1000, p.getAdBufferDuration());
        assertEquals(2000, p.getAdJoinDuration());
        assertEquals(4000, p.getAdPauseDuration());
        assertEquals(5000, p.getAdTotalDuration());

        // No adapters
        p.removeAdsAdapter();

        assertEquals(-1, p.getAdBufferDuration());
        assertEquals(-1, p.getAdJoinDuration());
        assertEquals(-1, p.getAdPauseDuration());
        assertEquals(-1, p.getAdTotalDuration());

        p.removeAdapter();

        assertEquals(-1, p.getBufferDuration());
        assertEquals(-1, p.getJoinDuration());
        assertEquals(-1, p.getSeekDuration());
        assertEquals(-1, p.getPauseDuration());
    }

    @Test
    public void testRequestBuilderInstance() {
        assertEquals(mockRequestBuilder, p.getRequestBuilder());
    }

    private Map<String, String> mockListeners() {
        // Make build params return the first argument
        when(mockRequestBuilder.buildParams(nullable(Map.class), anyString())).thenAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                return invocation.getArguments()[0];
            }
        });

        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("key", "value");
        return paramsMap;
    }

    // Will send listeners

    @Test
    public void willSendInit() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendInitListener(listener);
        p.fireInit(paramsMap);
        p.removeOnWillSendInitListener(listener);
        p.fireInit(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendInitWhenStarted() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendInitListener(listener);
        adapterEventListener.onStart(paramsMap);
        p.removeOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendStart() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        when(p.getTitle()).thenReturn("title");
        when(p.getResource()).thenReturn("resource");
        when(p.getIsLive()).thenReturn(false);
        when(p.getDuration()).thenReturn(30d);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        p.removeOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_START), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willForceInit() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        when(p.getTitle()).thenReturn("title");
        when(p.getResource()).thenReturn("resource");
        when(p.getIsLive()).thenReturn(false);
        when(p.getDuration()).thenReturn(30d);
        when(p.isForceInit()).thenReturn(true);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendInitListener(listener);
        adapterEventListener.onStart(paramsMap);
        p.removeOnWillSendInitListener(listener);
        adapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendStartWhenInitiated() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendStartListener(listener);
        p.fireInit(paramsMap);
        adapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendStartListener(listener);
        p.fireInit(paramsMap);
        adapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_START), eq(p), mapCaptor.capture());
        assertEquals(0, mapCaptor.getValue().size());
    }

    @Test
    public void willSendJoin() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendJoinListener(listener);
        adapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendJoinListener(listener);
        adapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_JOIN), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendPause() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendPauseListener(listener);
        adapterEventListener.onPause(paramsMap);
        p.removeOnWillSendPauseListener(listener);
        adapterEventListener.onPause(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_PAUSE), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendResume() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendResumeListener(listener);
        adapterEventListener.onResume(paramsMap);
        p.removeOnWillSendResumeListener(listener);
        adapterEventListener.onResume(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_RESUME), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendBuffer() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendBufferListener(listener);
        adapterEventListener.onBufferEnd(paramsMap);
        p.removeOnWillSendBufferListener(listener);
        adapterEventListener.onBufferEnd(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_BUFFER), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendSeek() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendSeekListener(listener);
        adapterEventListener.onSeekEnd(paramsMap);
        p.removeOnWillSendSeekListener(listener);
        adapterEventListener.onSeekEnd(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_SEEK), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendError() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendErrorListener(listener);
        adapterEventListener.onError(paramsMap);
        p.removeOnWillSendErrorListener(listener);
        adapterEventListener.onError(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_ERROR), eq(p), mapCaptor.capture());
    }

    @Test
    public void willSendStop() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendStopListener(listener);
        adapterEventListener.onStop(paramsMap);
        p.removeOnWillSendStopListener(listener);
        adapterEventListener.onStop(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_STOP), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdInit() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdInitListener(listener);
        adAdapterEventListener.onAdInit(paramsMap);
        p.removeOnWillSendAdInitListener(listener);
        adAdapterEventListener.onAdInit(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdInitWhenAdStarted() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendAdInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_INIT), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdStart() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        when(p.getAdDuration()).thenReturn(10d);
        when(p.getAdResource()).thenReturn("a");
        when(p.getAdTitle()).thenReturn("b");

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdStartListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendAdStartListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_START), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdStartWhenAdInitiated() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdStartListener(listener);
        adAdapterEventListener.onAdInit(paramsMap);
        adAdapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendAdStartListener(listener);
        adAdapterEventListener.onAdInit(paramsMap);
        adAdapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_START), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdJoin() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdJoinListener(listener);
        adAdapterEventListener.onJoin(paramsMap);
        p.removeOnWillSendAdJoinListener(listener);
        adAdapterEventListener.onJoin(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_JOIN), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdPause() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdPauseListener(listener);
        adAdapterEventListener.onPause(paramsMap);
        p.removeOnWillSendAdPauseListener(listener);
        adAdapterEventListener.onPause(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_PAUSE), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdResume() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdResumeListener(listener);
        adAdapterEventListener.onResume(paramsMap);
        p.removeOnWillSendAdResumeListener(listener);
        adAdapterEventListener.onResume(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_RESUME), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdBuffer() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdBufferListener(listener);
        adAdapterEventListener.onBufferEnd(paramsMap);
        p.removeOnWillSendAdBufferListener(listener);
        adAdapterEventListener.onBufferEnd(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_BUFFER), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdClick() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdClickListener(listener);
        adAdapterEventListener.onClick(paramsMap);
        p.removeOnWillSendAdClick(listener);
        adAdapterEventListener.onClick(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_CLICK), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendAdStop() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendAdStopListener(listener);
        adAdapterEventListener.onStop(paramsMap);
        p.removeOnWillSendAdStopListener(listener);
        adAdapterEventListener.onStop(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_AD_STOP), eq(p), mapCaptor.capture());
        assertEquals("value", mapCaptor.getValue().get("key"));
    }

    @Test
    public void willSendInitWhenAdStarted() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.addOnWillSendInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener).willSendRequest(eq(Constants.SERVICE_INIT), eq(p), mapCaptor.capture());
    }

    @Test
    public void willNotSendInitWhenAdStarted() {
        Map<String, String> paramsMap = mockListeners();
        ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);

        Plugin.WillSendRequestListener listener = mock(Plugin.WillSendRequestListener.class);
        p.fireInit();
        p.addOnWillSendInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendInitListener(listener);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener, times(0)).willSendRequest(
                eq(Constants.SERVICE_INIT), eq(p), mapCaptor.capture());

        listener = mock(Plugin.WillSendRequestListener.class);

        when(p.getTitle()).thenReturn("title");
        when(p.getResource()).thenReturn("resource");
        when(p.getIsLive()).thenReturn(false);
        when(p.getDuration()).thenReturn(30d);

        p.addOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        adAdapterEventListener.onStart(paramsMap);
        p.removeOnWillSendStartListener(listener);
        adapterEventListener.onStart(paramsMap);
        adAdapterEventListener.onStart(paramsMap);
        verify(listener, times(0)).willSendRequest(
                eq(Constants.SERVICE_INIT), eq(p), mapCaptor.capture());
    }

    // Test Youbora methods
    @Test
    public void testInit() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);
        p.fireInit(null);
        verify(p).createRequest(nullable(String.class), eq(Constants.SERVICE_INIT));
    }

    @Test
    public void testStop(){
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireInit(null);
        verify(p).createRequest(nullable(String.class), eq(Constants.SERVICE_INIT));

        p.fireStop(null);
        verify(p).createRequest(nullable(String.class), eq(Constants.SERVICE_STOP));
    }

    @Test
    public void testStopNotSentTwice() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireInit(null);
        verify(p).createRequest(nullable(String.class), eq(Constants.SERVICE_INIT));

        p.fireStop();
        verify(p, times(1)).
                createRequest(nullable(String.class), eq(Constants.SERVICE_STOP));
        p.fireStop();
        verify(p, times(1)).
                createRequest(nullable(String.class), eq(Constants.SERVICE_STOP));
    }

    @Test
    public void testStopCalledWithoutInit() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireStop();
        verify(p, times(0)).
                createRequest(anyString(), eq(Constants.SERVICE_STOP));
    }

    @Test
    public void testStopWhenAdapterNotNull() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.setAdapter(new PlayerAdapter("a"));

        PlayerAdapter.AdapterEventListenerImpl mockListener =
                mock(PlayerAdapter.AdapterEventListenerImpl.class);
        p.getAdapter().addEventListener(mockListener);
        p.getAdapter().fireStart();
        p.fireStop();

        verify(mockListener, times(1)).onStop(nullable(Map.class));
    }

    @Test
    public void testError() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireInit();
        verify(p).createRequest(nullable(String.class), eq(Constants.SERVICE_INIT));
        p.fireError(null);
        verify(p).createRequest(nullable(String.class), eq(Constants.SERVICE_ERROR));
    }

    @Test
    public void testFatalError() {
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);

        p.fireInit();
        verify(p).createRequest(nullable(String.class), eq(Constants.SERVICE_INIT));
        p.fireFatalError(null, null, null, null);
        verify(p).createRequest(nullable(String.class), eq(Constants.SERVICE_ERROR));
        verify(p).createRequest(nullable(String.class), eq(Constants.SERVICE_STOP));
    }

    // TODO: Redo this test properly
    /*@Test
    public void testSendOfflineEvents(){
        // If offline enabled don't send offlineEvents
        Plugin p = spy(new TestPlugin(null));
        when(mockOptions.isEnabled()).thenReturn(true);
        when(mockOptions.isOffline()).thenReturn(true);
        p.fireOfflineEvents();
        verify(p,times(0)).createRequest(anyString(),eq(Constants.SERVICE_OFFLINE_EVENTS));

        // If adapters are set don't send offline events
        PlayerAdapter mockAdapter = mock(PlayerAdapter.class);
        when(mockOptions.isOffline()).thenReturn(false);
        p.setAdapter(mockAdapter);
        p.fireOfflineEvents();
        verify(p,times(0)).createRequest(anyString(),eq(Constants.SERVICE_OFFLINE_EVENTS));

        // Offline not enabled and no adapter and no events
        p = spy(new TestPlugin(null));
        when(mockOptions.isOffline()).thenReturn(false);
        p.fireOfflineEvents();
        verify(p,times(0)).createRequest(anyString(),eq(Constants.SERVICE_OFFLINE_EVENTS));
    }*/

    @Test
    public void testTransformsOfflineMode() {
        Plugin p = spy(new TestPlugin(null));
        Context mockContext = RuntimeEnvironment.systemContext;
        when(mockOptions.isEnabled()).thenReturn(true);
        when(mockOptions.isOffline()).thenReturn(true);
        when(p.getApplicationContext()).thenReturn(mockContext);

        p.setApplicationContext(mockContext);
        p.fireInit();

        assertEquals(p.getApplicationContext(), mockContext);
    }
}