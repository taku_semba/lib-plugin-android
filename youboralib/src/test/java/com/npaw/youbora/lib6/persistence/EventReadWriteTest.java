package com.npaw.youbora.lib6.persistence;

import android.content.Context;

import com.npaw.youbora.lib6.persistence.dao.EventDAO;
import com.npaw.youbora.lib6.persistence.dao.EventDAOImpl;
import com.npaw.youbora.lib6.persistence.entity.Event;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Enrique on 03/01/2018.
 */

@RunWith(RobolectricTestRunner.class)

public class EventReadWriteTest {

    private EventDAO eventDao;
    private Event event;
    private Event event2;

    @Before
    public void createDb() {
        Context context = RuntimeEnvironment.systemContext;
        AppDatabaseSingleton.init(context);

        eventDao = EventDAOImpl.getInstance();

        clearDb();

        event = new Event("dummy",1L,2);
        event2 = new Event("dummy",1L,2);
    }

    @After
    public void closeDb() {
        clearDb();
        resetSingleton(AppDatabaseSingleton.class, "databaseSingleton");
    }

    @Test
    public void insertNewEventAndCheckItExists() {
        eventDao.insertNewEvent(event);

        List<Event> events = eventDao.getAll();
        assertTrue(events.size() > 0);
    }

    @Test
    public void eventsWithSameOfflineId() {
        eventDao.insertNewEvent(event);
        eventDao.insertNewEvent(event2);

        assertTrue(eventDao.getByOfflineId(2).size() == 2);
    }

    @Test
    public void lastIdReturnGreaterOfflineId() {
        eventDao.insertNewEvent(event);
        eventDao.insertNewEvent(event2);

        int lastOfflineId = eventDao.getLastId();
        assertEquals(lastOfflineId,event2.getOfflineId());
    }

    @Test
    public void firstIdReturnSmallerOfflineId() {
        eventDao.insertNewEvent(event);
        eventDao.insertNewEvent(event2);

        int firstOfflineId = eventDao.getFirstId();
        assertEquals(firstOfflineId,event.getOfflineId());
    }

    @Test
    public void deleteEventsRemovesEvents() {
        eventDao.insertNewEvent(event);
        eventDao.insertNewEvent(event2);

        List<Event> savedEvents = eventDao.getAll();

        eventDao.deleteEvents(2);

        assertTrue(eventDao.getAll().size() == 0);
    }

    private void clearDb(){
        eventDao.deleteAll();
    }

    private void resetSingleton(Class clazz, String fieldName) {
        Field instance;

        try {
            instance = clazz.getDeclaredField(fieldName);
            instance.setAccessible(true);
            instance.set(null, null);

            instance = clazz.getDeclaredField("initCalled");
            instance.setAccessible(true);
            instance.set(null, false);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
