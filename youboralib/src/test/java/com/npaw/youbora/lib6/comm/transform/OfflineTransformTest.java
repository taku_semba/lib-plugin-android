package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by rufo on 26/12/2017.
 */

public class OfflineTransformTest {

    private OfflineTransform offlineTransform;
    private Request mockRequest;
    private YouboraLog.YouboraLogger mockLogger;

    @Before
    public void setUp() {
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        mockLogger = mock(YouboraLog.YouboraLogger.class);
        YouboraLog.addLogger(mockLogger);

        offlineTransform = new OfflineTransform();
        mockRequest = mock(Request.class);
    }

    @After
    public void tearDown() {
        offlineTransform = null;
    }

    @Test
    public void testHasToSend(){
        Request mockRequest = mock(Request.class);

        assertFalse(offlineTransform.hasToSend(mockRequest));
    }

    @Test
    public void testState(){
        assertEquals(offlineTransform.getState(),Transform.STATE_OFFLINE);
    }
}
