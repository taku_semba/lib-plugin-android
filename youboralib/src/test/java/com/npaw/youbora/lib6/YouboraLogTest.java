package com.npaw.youbora.lib6;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.RobolectricTestRunner;

import java.util.concurrent.CountDownLatch;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)

public class YouboraLogTest {

    private YouboraLog.YouboraLogger defaultLogger = new YouboraLog.YouboraLogger() {
        @Override
        public void logYouboraMessage(String message, YouboraLog.Level logLevel) {}
    };

    @Test
    public void testLogCallbacks() {

        final CountDownLatch lock = new CountDownLatch(6);

        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        YouboraLog.error(new Exception());
        YouboraLog.addLogger(new YouboraLog.YouboraLogger() {
            @Override
            public void logYouboraMessage(String message, YouboraLog.Level logLevel) {
                System.out.println(message);
                lock.countDown();
            }
        });
        YouboraLog.requestLog("requestLog");
        YouboraLog.debug("debug");
        YouboraLog.notice("notice");
        YouboraLog.warn("warn");
        YouboraLog.error("error");
        YouboraLog.error(new Exception());

        assertEquals(0, lock.getCount());
    }

    @Test
    public void testAddRemoveLoggers() {
        assertFalse(YouboraLog.removeLogger(null));
        assertFalse(YouboraLog.removeLogger(defaultLogger));
        YouboraLog.addLogger(defaultLogger);
        YouboraLog.addLogger(null);
        assertFalse(YouboraLog.removeLogger(null));
        assertTrue(YouboraLog.removeLogger(defaultLogger));
        assertFalse(YouboraLog.removeLogger(defaultLogger));
    }

    @Test
    public void reportLogMessage() {
        YouboraLog.setDebugLevel(YouboraLog.Level.VERBOSE);
        YouboraLog.reportLogMessage(YouboraLog.Level.ERROR, "");
        YouboraLog.reportLogMessage(YouboraLog.Level.WARNING, "");
        YouboraLog.reportLogMessage(YouboraLog.Level.NOTICE, "");
        YouboraLog.reportLogMessage(YouboraLog.Level.DEBUG, "");
        YouboraLog.reportLogMessage(YouboraLog.Level.VERBOSE, "");
        YouboraLog.reportLogMessage(YouboraLog.Level.SILENT, "");
    }

    @Test
    public void isAtLeast() {
        YouboraLog.Level.VERBOSE.isAtLeast(YouboraLog.Level.ERROR);
    }

    @Test
    public void error() {
        YouboraLog.setDebugLevel(YouboraLog.Level.ERROR);
        YouboraLog.addLogger(defaultLogger);
        YouboraLog.error(new Exception());
        YouboraLog.removeLogger(defaultLogger);
    }
}
