package com.npaw.youbora.lib6.adapter;

import com.npaw.youbora.lib6.BuildConfig;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.ArgumentCaptor;

import org.robolectric.RobolectricTestRunner;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)

public class PlayerAdapterTest {

    private class CustomAdapter extends PlayerAdapter<String> {
        boolean unregisterListenersCalled = false;
        boolean registerListenersCalled = false;

        CustomAdapter(String player) {
            super(player);
            registerListeners();
        }

        @Override
        public void registerListeners() { registerListenersCalled = true; }

        @Override
        public void unregisterListeners() { unregisterListenersCalled = true; }

        /*@Override
        public void setPlayer(String player) {
            registerListenersCalled = false;
            unregisterListenersCalled = false;
        }*/
    }

    private class CustomAdsAdapter extends PlayerAdapter<String> {
        AdPosition adPosition;

        CustomAdsAdapter(String player) {
            super(player);
            registerListeners();
            adPosition = AdPosition.UNKNOWN;
        }

        @Override
        public AdPosition getPosition() { return adPosition; }

        private void setAdPosition(AdPosition adPosition) { this.adPosition = adPosition; }
    }

    @Test
    public void testRegisterUnregister() {
        CustomAdapter adapter = new CustomAdapter("test");

        assertEquals(true, adapter.registerListenersCalled);
        assertEquals(false, adapter.unregisterListenersCalled);

        adapter.dispose();

        assertEquals(true, adapter.unregisterListenersCalled);
    }

    @Test
    public void testDispose() {
        CustomAdapter adapter = spy(new CustomAdapter("test"));

        PlayheadMonitor mockMonitor = mock(PlayheadMonitor.class);
        when(adapter.createPlayheadMonitor(eq(adapter), anyInt(), anyInt())).thenReturn(mockMonitor);

        adapter.monitorPlayhead(true, true, 800);

        adapter.dispose();

        verify(adapter).fireStop();
        verify(mockMonitor).stop();
        verify(adapter).unregisterListeners();
        assertNull(adapter.getPlayer());
    }

    @Test
    public void testSettersGetters() {
        CustomAdapter adapter = spy(new CustomAdapter("test"));

        // Plugin
        Plugin mockPlugin = mock(Plugin.class);
        adapter.setPlugin(mockPlugin);
        assertEquals(mockPlugin, adapter.getPlugin());

        // Chronos
        assertNotNull(adapter.getChronos());

        // Flags
        assertNotNull(adapter.getFlags());

        // Monitor null
        assertNull(adapter.getMonitor());

        // Monitor not null
        PlayheadMonitor mockMonitor = mock(PlayheadMonitor.class);
        when(adapter.createPlayheadMonitor(eq(adapter), anyInt(), anyInt())).thenReturn(mockMonitor);
        adapter.monitorPlayhead(true, true, 800);
        assertEquals(mockMonitor, adapter.getMonitor());
    }

    @Test
    public void testInfoMethodsDefaults() {
        CustomAdapter adapter = new CustomAdapter("test");

        assertNull(adapter.getPlayhead());
        assertEquals(Double.valueOf(1), adapter.getPlayrate());
        assertNull(adapter.getFramesPerSecond());
        assertNull(adapter.getDroppedFrames());
        assertNull(adapter.getDuration());
        assertNull(adapter.getBitrate());
        assertNull(adapter.getThroughput());
        assertNull(adapter.getRendition());
        assertNull(adapter.getTitle());
        assertNull(adapter.getProgram());
        assertNull(adapter.getIsLive());
        assertNull(adapter.getResource());
        assertNull(adapter.getPlayerVersion());
        assertNull(adapter.getPlayerName());
        assertNull(adapter.getCdnTraffic());
        assertNull(adapter.getP2PTraffic());
        assertNull(adapter.getIsP2PEnabled());
        assertNull(adapter.getUploadTraffic());
        assertNull(adapter.getLatency());
        assertNull(adapter.getPacketLoss());
        assertNull(adapter.getPacketSent());
        assertNull(adapter.getHouseholdId());
        assertEquals(adapter.getPosition(), PlayerAdapter.AdPosition.UNKNOWN);
        assertEquals(BuildConfig.VERSION_NAME + "-generic-android", adapter.getVersion());
    }

    @Test
    public void testFireMethodsFlags() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlaybackFlags flags = adapter.getFlags();
        adapter.addEventListener(new PlayerAdapter.AdapterEventListenerImpl());

        // Initial state
        assertFalse(flags.isPreloading());
        assertFalse(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());
        assertFalse(flags.isAdInitiated());

        //Ad Init
        adapter.fireAdInit();

        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());
        assertTrue(flags.isAdInitiated());

        // Start
        adapter.fireStart();

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Join
        adapter.fireJoin();

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Pause
        adapter.firePause();

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertTrue(flags.isPaused());

        // Resume
        adapter.fireResume();

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Buffer start
        adapter.fireBufferBegin();

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertTrue(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Buffer end
        adapter.fireBufferEnd();

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Seek start
        adapter.fireSeekBegin();

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertTrue(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Seek end
        adapter.fireSeekEnd();

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertTrue(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Stop
        adapter.fireStop();

        assertFalse(flags.isPreloading());
        assertFalse(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Start then error (non fatal)
        adapter.fireStart();
        adapter.fireError(null, null, null);

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Start then error (non fatal) passing exception
        adapter.fireStart();
        adapter.fireError(null, null, null, null);

        assertFalse(flags.isPreloading());
        assertTrue(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Start then fatal error
        adapter.fireStart();
        adapter.fireFatalError(null, null, null);

        assertFalse(flags.isPreloading());
        assertFalse(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());

        // Start then fatal error passing exception
        adapter.fireStart();
        adapter.fireFatalError(null, null, null, null);

        assertFalse(flags.isPreloading());
        assertFalse(flags.isStarted());
        assertFalse(flags.isJoined());
        assertFalse(flags.isBuffering());
        assertFalse(flags.isSeeking());
        assertFalse(flags.isPaused());
    }

    @Test
    public void testFireMethodsCallbacks() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        verify(mockListener).onStart(nullable(Map.class));

        adapter.fireJoin();
        verify(mockListener).onJoin(nullable(Map.class));

        adapter.firePause();
        verify(mockListener).onPause(nullable(Map.class));

        adapter.fireResume();
        verify(mockListener).onResume(nullable(Map.class));

        adapter.fireBufferBegin();
        verify(mockListener).onBufferBegin(nullable(Map.class), anyBoolean());

        adapter.fireBufferEnd();
        verify(mockListener).onBufferEnd(nullable(Map.class));

        adapter.fireSeekBegin();
        verify(mockListener).onSeekBegin(nullable(Map.class), anyBoolean());

        adapter.fireSeekEnd();
        verify(mockListener).onSeekEnd(nullable(Map.class));

        adapter.fireStop();
        verify(mockListener).onStop(nullable(Map.class));

        adapter.fireAdInit();
        verify(mockListener).onAdInit(nullable(Map.class));

        adapter.fireClick();
        verify(mockListener).onClick(nullable(Map.class));

        adapter.fireAllAdsCompleted();
        verify(mockListener).onAllAdsCompleted(nullable(Map.class));

        // Error
        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        adapter.fireStart();
        verify(mockListener, times(2)).onStart(nullable(Map.class));
        adapter.fireError(null);
        verify(mockListener).onError(captor.capture());

        // stop should be still only have been called once
        verify(mockListener).onStop(nullable(Map.class));
        //assertEquals("error", captor.getValue().get("errorLevel"));

        // Fatal error
        adapter.fireFatalError(null);
        verify(mockListener, times(2)).onError(captor.capture());

        // now stop should have been called twice in total
        verify(mockListener, times(2)).onStop(nullable(Map.class));
        assertEquals("fatal", captor.getValue().get("errorLevel"));
    }

    @Test
    public void testBufferToSeek() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireJoin();

        adapter.fireBufferBegin();
        adapter.fireSeekBegin();
        adapter.fireBufferEnd();
        adapter.fireSeekEnd();

        verify(mockListener, times(1)).onStart(nullable(Map.class));
        verify(mockListener, times(1)).onJoin(nullable(Map.class));
        verify(mockListener, times(1)).onBufferBegin(nullable(Map.class), anyBoolean());
        verify(mockListener, times(1)).onSeekBegin(nullable(Map.class), anyBoolean());
        verify(mockListener, times(1)).onSeekEnd(nullable(Map.class));
        verify(mockListener, times(0)).onBufferEnd(nullable(Map.class));
    }

    @Test
    public void testFireBufferConvertFromSeek() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireJoin();

        //Convert from seek
        adapter.fireSeekBegin();
        adapter.fireBufferBegin(true);
        adapter.fireSeekEnd();
        adapter.fireBufferEnd();

        //Not convert from seek
        adapter.fireSeekBegin();
        adapter.fireBufferBegin(false);
        adapter.fireSeekEnd();
        adapter.fireBufferEnd();

        verify(mockListener, times(1)).onStart(nullable(Map.class));
        verify(mockListener, times(1)).onJoin(nullable(Map.class));
        verify(mockListener, times(1)).onBufferBegin(nullable(Map.class), anyBoolean());
        verify(mockListener, times(2)).onSeekBegin(nullable(Map.class), anyBoolean());
        verify(mockListener, times(1)).onSeekEnd(nullable(Map.class));
        verify(mockListener, times(1)).onBufferEnd(nullable(Map.class));
    }

    @Test
    public void testFireSeelConvertFromBuffer() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireJoin();

        //Convert from buffer
        adapter.fireBufferBegin();
        adapter.fireSeekBegin(true);
        adapter.fireBufferEnd();
        adapter.fireSeekEnd();

        //Not convert from buffer
        adapter.fireBufferBegin();
        adapter.fireSeekBegin(false);
        adapter.fireBufferEnd();
        adapter.fireSeekEnd();

        verify(mockListener, times(1)).onStart(nullable(Map.class));
        verify(mockListener, times(1)).onJoin(nullable(Map.class));
        verify(mockListener, times(2)).onBufferBegin(nullable(Map.class), anyBoolean());
        verify(mockListener, times(1)).onSeekBegin(nullable(Map.class), anyBoolean());
        verify(mockListener, times(1)).onSeekEnd(nullable(Map.class));
        verify(mockListener, times(1)).onBufferEnd(nullable(Map.class));
    }

    @Test
    public void testAddRemoveListener() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);

        assertFalse(adapter.removeEventListener(mockListener));
        assertFalse(adapter.removeEventListener(null));

        adapter.addEventListener(mockListener);

        assertFalse(adapter.removeEventListener(null));
        assertTrue(adapter.removeEventListener(mockListener));
        assertFalse(adapter.removeEventListener(mockListener));
    }

    @Test
    public void testGetPlayer() {
        CustomAdapter adapter = new CustomAdapter("test");
        assertEquals(adapter.getPlayer(),"test");
    }

    @Test
    public void testSetNullPlayer() {
        CustomAdapter adapter = new CustomAdapter("test");

        adapter.setPlayer(null);

        assertEquals(true, adapter.registerListenersCalled);
        assertEquals(true, adapter.unregisterListenersCalled);
    }

    @Test
    public void testSetNonNullPlayer() {
        CustomAdapter adapter = new CustomAdapter("test");

        adapter.setPlayer("test2");

        assertEquals(true, adapter.registerListenersCalled);
        assertEquals(true, adapter.unregisterListenersCalled);
    }

    @Test
    public void testFireStopWhilePaused(){
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireJoin();
        adapter.firePause();

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        adapter.fireStop();
        verify(mockListener,times(1)).onStop(captor.capture());
        assertTrue(captor.getValue().containsKey("pauseDuration"));
        assertTrue(captor.getValue().get("pauseDuration") != "-1");
    }

    @Test
    public void testStartPreroll(){
        CustomAdsAdapter adsAdapter = new CustomAdsAdapter("player");
        adsAdapter.setAdPosition(PlayerAdapter.AdPosition.PRE);

        //Preroll with AdInit
        adsAdapter.fireAdInit();
        assertTrue(adsAdapter.getFlags().isAdInitiated());
        assertNotNull(adsAdapter.getChronos().join.getStartTime());
        adsAdapter.fireStart();
        assertTrue(adsAdapter.getFlags().isStarted());
        adsAdapter.fireStop();


        //Not preroll with AdInit
        adsAdapter.setAdPosition(PlayerAdapter.AdPosition.MID);
        adsAdapter.fireAdInit();
        assertTrue(adsAdapter.getFlags().isAdInitiated());
        assertNotNull(adsAdapter.getChronos().join.getStartTime());
        adsAdapter.fireStart();
        assertTrue(adsAdapter.getFlags().isStarted());
        adsAdapter.fireStop();

        //Unregister listeners
        adsAdapter.unregisterListeners();
    }

    @Test
    public void checkAdClickWithValidUrl() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireClick("Url");

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener,times(1)).onClick(captor.capture());
        assertTrue(captor.getValue().containsKey("adUrl"));
        assertTrue(captor.getValue().get("adUrl") == "Url");
    }

    @Test
    public void checkAdClickWithInvalidUrl() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        String s = null;
        adapter.fireClick(s);

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);
        verify(mockListener,times(1)).onClick(captor.capture());
        assertTrue(captor.getValue().containsKey("adUrl"));
        assertNull(captor.getValue().get("adUrl"));
    }

    @Test
    public void testFireSkip() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireSkip();

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener,times(1)).onStop(captor.capture());
        assertTrue(captor.getValue().containsKey("skipped"));
        assertTrue(captor.getValue().get("skipped") == "true");
    }

    @Test
    public void testFireCast() {
        CustomAdapter adapter = new CustomAdapter("test");

        PlayerAdapter.AdapterEventListenerImpl mockListener = mock(PlayerAdapter.AdapterEventListenerImpl.class);
        adapter.addEventListener(mockListener);

        adapter.fireStart();
        adapter.fireCast();

        ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);

        verify(mockListener,times(1)).onStop(captor.capture());
        assertTrue(captor.getValue().containsKey("casted"));
        assertTrue(captor.getValue().get("casted") == "true");
    }

    @Test
    public void seekWhenContentIsLiveNoSeek() {
        Options o = new Options();
        o.setContentIsLiveNoSeek(true);
        Plugin p = new Plugin(o);

        CustomAdapter adapter = new CustomAdapter("test");
        p.setAdapter(adapter);

        adapter.fireSeekBegin();
        adapter.fireSeekEnd();
    }
}