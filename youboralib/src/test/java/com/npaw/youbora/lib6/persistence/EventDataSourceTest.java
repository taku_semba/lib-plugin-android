package com.npaw.youbora.lib6.persistence;

import android.content.Context;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.persistence.entity.Event;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertTrue;

/**
 * Created by Enrique on 03/01/2018.
 */

@RunWith(RobolectricTestRunner.class)

public class EventDataSourceTest {

    private EventDataSource dataSource;
    private Context mockContext;

    private Event mockEvent;

    private YouboraLog.YouboraLogger mockLogger;

    @Before
    public void setUp() {
        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        mockLogger = mock(YouboraLog.YouboraLogger.class);
        YouboraLog.addLogger(mockLogger);

        reset();

        mockContext = RuntimeEnvironment.systemContext;
        mockEvent = mock(Event.class);

        when(mockEvent.getOfflineId()).thenReturn(1);
        when(mockEvent.getDateUpdate()).thenReturn(2L);
        when(mockEvent.getJsonEvents()).thenReturn("dummy_events");
    }


    //TODO Review how to test singleton properly
    /*
    @Test
    public void initDatabaseWithoutContextAndInstance(){
        dataSource = new EventDataSource();
        dataSource.initDatabase();
        assertFalse(AppDatabaseSingleton.initCalled);
    }*/

    @Test
    public void initDatabaseWithContextAndWithoutInstance(){
        dataSource = new EventDataSource(mockContext);
        dataSource.initDatabase();

        assertTrue(AppDatabaseSingleton.initCalled);
    }

    @Test
    public void initDatabasePassingContext(){
        dataSource = new EventDataSource();
        dataSource.initDatabase(mockContext);

        assertTrue(AppDatabaseSingleton.initCalled);
    }
}
