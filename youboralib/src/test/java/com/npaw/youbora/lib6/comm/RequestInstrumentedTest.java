package com.npaw.youbora.lib6.comm;

import android.net.Uri;
import android.os.Bundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLooper;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(RobolectricTestRunner.class)

public class RequestInstrumentedTest {
    private MockWebServer mockWebServer;

    @Before
    public void setUp() { mockWebServer = new MockWebServer(); }

    @Test
    public void testSendSuccessRequest() throws Exception {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200));

        final CountDownLatch latch = new CountDownLatch(2);
        final Request r = new Request(mockWebServer.url("/").toString(), "/service");

        String headerName = "headerName";
        String headerValue = "headerValue";

        Map<String, String> reqHeaders = new HashMap<>(1);
        reqHeaders.put(headerName, headerValue);
        r.setRequestHeaders(reqHeaders);

        r.addOnErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                fail("RequestErrorListener called when it shouldn't have.");
            }
        });

        r.addOnSuccessListener(new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response, Map<String, Object> listenerParams) {
                latch.countDown();
            }
        });

        // "On every" listeners
        Request.RequestErrorListener errorListener = new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                fail("RequestErrorListener called when it shouldn't have.");
            }
        };
        Request.addOnEveryErrorListener(errorListener);

        Request.RequestSuccessListener successListener = new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response, Map<String, Object> listenerParams) {
                latch.countDown();
            }
        };
        Request.addOnEverySuccessListener(successListener);

        r.send();
        latch.await(1, TimeUnit.SECONDS);
        ShadowLooper.runMainLooperOneTask();
        latch.await(1, TimeUnit.SECONDS);

        assertEquals(0, latch.getCount());

        Request.removeOnEveryErrorListener(errorListener);
        Request.removeOnEverySuccessListener(successListener);
    }

    @Test
    public void testSendErrorRequest() throws Exception {
        mockWebServer.enqueue(new MockResponse().setResponseCode(500));

        final CountDownLatch latch = new CountDownLatch(2);
        final Request r = new Request(mockWebServer.url("/").toString(), "service");

        String headerName = "headerName";
        String headerValue = "headerValue";

        Map<String, String> reqHeaders = new HashMap<>(1);
        reqHeaders.put(headerName, headerValue);
        r.setRequestHeaders(reqHeaders);

        r.setMethod(Request.METHOD_HEAD);

        r.addOnErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                latch.countDown();
            }
        });

        r.addOnSuccessListener(new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response, Map<String, Object> listenerParams) {
                fail("RequestSuccessListener called when it shouldn't have. Response: " + response);
            }
        });

        // "On every" listeners
        Request.RequestErrorListener errorListener = new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                latch.countDown();
            }
        };
        Request.addOnEveryErrorListener(errorListener);

        Request.RequestSuccessListener successListener = new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response, Map<String, Object> listenerParams) {
                fail("RequestSuccessListener called when it shouldn't have. Response: " + response);
            }
        };
        Request.addOnEverySuccessListener(successListener);

        r.send();

        latch.await(1, TimeUnit.SECONDS);
        ShadowLooper.runMainLooperOneTask();
        latch.await(1, TimeUnit.SECONDS);

        assertEquals(0, latch.getCount());

        Request.removeOnEveryErrorListener(errorListener);
        Request.removeOnEverySuccessListener(successListener);
    }

    @Test
    public void testGetQuery() {
        // Test
        Request r = new Request("host.com", "/service");

        assertEquals("", r.getQuery());

        r.setParam("stringKey", "stringValue");
        r.setParam("intKey", 2);
        r.setParam("doubleKey", 23.0);

        Map<String, Object> map = new HashMap<>();
        map.put("mapStringKey", "mapStringValue");
        map.put("mapIntArrayKey", new int[]{1,2,3,4});

        r.setParam("mapKey", map);

        Bundle b = new Bundle();
        b.putFloat("bfloatKey", 12);
        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("string1");
        stringArrayList.add("string2");
        stringArrayList.add("string3");
        b.putStringArrayList("arrayListKey", stringArrayList);

        r.setParam("bundleKey", b);

        String query = r.getQuery();
        String decodedString = Uri.decode(query);

        assertTrue(decodedString.contains("stringKey=stringValue"));
        assertTrue(decodedString.contains("intKey=2"));
        assertTrue(decodedString.contains("doubleKey=23"));
        assertTrue(decodedString.contains("mapKey={"));
        assertTrue(decodedString.contains("\"mapStringKey\":\"mapStringValue\""));
        assertTrue(decodedString.contains("\"mapIntArrayKey\":[1,2,3,4]"));
        assertTrue(decodedString.contains("bundleKey={"));
        assertTrue(decodedString.contains("\"bfloatKey\":12"));
        assertTrue(decodedString.contains("\"arrayListKey\":[\"string1\",\"string2\",\"string3\"]"));

        assertEquals(258, query.length());
    }

    @Test
    public void testBuildUrl() {
        Request r = new Request("http://host.com", "/service");

        r.setParam("key", "value");

        String url = r.getUrl();
        String expected = "http://host.com/service?key=value";

        assertEquals(expected, url);
    }
}
