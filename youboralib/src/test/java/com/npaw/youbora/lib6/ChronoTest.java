package com.npaw.youbora.lib6;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ChronoTest {

    private Chrono chrono;

    @Before
    public void before() {
        chrono = new Chrono();
    }

    @Test
    public void testStart() {
        chrono.start();
        assertTrue(chrono.getStartTime() > -1);
    }

    @Test
    public void testStop() {
        chrono.start();
        assertTrue(chrono.stop() > -1);
        assertTrue(chrono.getDeltaTime() > -1);
    }

    @Test
    public void testGetDeltaTime() {
        chrono.start();
        assertTrue(chrono.getDeltaTime(true) > -1);
        assertTrue(chrono.getDeltaTime() > -1);
    }

    @Test
    public void testStopBeforeStart() {
        assertTrue(chrono.stop() == -1);
        assertTrue(chrono.getDeltaTime() == -1);
    }

    @Test
    public void testClone() {
        chrono.setStartTime(1L);
        chrono.setStopTime(2L);
        chrono.setOffset(3L);

        Chrono chrono2 = chrono.copy();

        assertEquals(chrono.getStartTime(), chrono2.getStartTime());
        assertEquals(chrono.getStopTime(), chrono2.getStopTime());
        assertEquals(chrono.getOffset(), chrono2.getOffset());
    }

    @Test
    public void testReset() {
        chrono.setStartTime(1L);
        chrono.setStopTime(2L);
        chrono.setOffset(3L);

        chrono.reset();

        Chrono chrono2 = new Chrono();

        assertEquals(chrono.getStartTime(), chrono2.getStartTime());
        assertEquals(chrono.getStopTime(), chrono2.getStopTime());
        assertEquals(chrono.getOffset(), chrono2.getOffset());
    }

    @Test
    public void testTime() throws InterruptedException {

        chrono.start();

        Thread.sleep(1000);

        assertEquals(1000.0, chrono.stop(), 50.0);
        assertEquals(1000.0, chrono.getDeltaTime(), 50.0);

    }
}
