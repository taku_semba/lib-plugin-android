package com.npaw.youbora.lib6;

import org.json.JSONException;
import org.json.JSONObject;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)

public class DeviceInfoTest {

    @Test
    public void deviceInfoBuilderDefaultValues() {
        DeviceInfo deviceInfo = getDefaultDeviceInfo();

        assertNotNull(deviceInfo.getDeviceBrand());
        assertNotNull(deviceInfo.getDeviceModel());
        assertNull(deviceInfo.getDeviceType());
        assertNull(deviceInfo.getDeviceCode());
        assertNull(deviceInfo.getDeviceOsName());
        assertNotNull(deviceInfo.getDeviceOsVersion());
        assertNull(deviceInfo.getDeviceName());
        assertEquals("", deviceInfo.getDeviceBrowserName());
        assertEquals("", deviceInfo.getDeviceBrowserVersion());
        assertEquals("", deviceInfo.getDeviceBrowserType());
        assertEquals("", deviceInfo.getDeviceBrowserEngine());
    }

    @Test
    public void deviceInfoBuilder() {
        DeviceInfo deviceInfo = getCompleteDeviceInfo();

        assertEquals("brand", deviceInfo.getDeviceBrand());
        assertEquals("model", deviceInfo.getDeviceModel());
        assertEquals("type", deviceInfo.getDeviceType());
        assertEquals("code", deviceInfo.getDeviceCode());
        assertEquals("osName", deviceInfo.getDeviceOsName());
        assertEquals("osVersion", deviceInfo.getDeviceOsVersion());
        assertEquals("name", deviceInfo.getDeviceName());
        assertEquals("browser name", deviceInfo.getDeviceBrowserName());
        assertEquals("browser version", deviceInfo.getDeviceBrowserVersion());
        assertEquals("browser type", deviceInfo.getDeviceBrowserType());
        assertEquals("browser engine", deviceInfo.getDeviceBrowserEngine());
    }

    @Test
    public void testMapJsonStringWithDefaultValues() throws JSONException {
        String deviceInfoString = getDefaultDeviceInfo().mapToJSONString();
        JSONObject jsonDeviceInfo = new JSONObject(deviceInfoString);

        assertNotNull(jsonDeviceInfo.getString("brand"));
        assertNotNull(jsonDeviceInfo.getString("model"));
        assertNotNull(jsonDeviceInfo.getString("osVersion"));
        assertEquals("", jsonDeviceInfo.getString("browserName"));
        assertEquals("", jsonDeviceInfo.getString("browserVersion"));
        assertEquals("", jsonDeviceInfo.getString("browserType"));
        assertEquals("", jsonDeviceInfo.getString("browserEngine"));
        int exceptionCounter = 0;
        String dummyValue = "";

        try {
            dummyValue = jsonDeviceInfo.getString("deviceType");
        } catch (JSONException ex) {
            exceptionCounter++;
        }

        try {
            dummyValue = jsonDeviceInfo.getString("deviceCode");
        } catch (JSONException ex) {
            exceptionCounter++;
        }

        try {
            dummyValue = jsonDeviceInfo.getString("osName");
        } catch (JSONException ex) {
            exceptionCounter++;
        }

        try {
            dummyValue = jsonDeviceInfo.getString("deviceName");
        } catch (JSONException ex) {
            exceptionCounter++;
        }

        assertEquals(4, exceptionCounter);
        assertEquals("", dummyValue);
    }

    @Test
    public void testMapJsonStringWithCompleteValues() throws JSONException {
        String deviceInfoString = getCompleteDeviceInfo().mapToJSONString();
        JSONObject jsonDeviceInfo = new JSONObject(deviceInfoString);

        assertEquals("brand", jsonDeviceInfo.getString("brand"));
        assertEquals("model", jsonDeviceInfo.getString("model"));
        assertEquals("type", jsonDeviceInfo.get("deviceType"));
        assertEquals("code", jsonDeviceInfo.get("deviceCode"));
        assertEquals("osName", jsonDeviceInfo.get("osName"));
        assertEquals("osVersion", jsonDeviceInfo.getString("osVersion"));
        assertEquals("browser name", jsonDeviceInfo.getString("browserName"));
        assertEquals("browser version", jsonDeviceInfo.getString("browserVersion"));
        assertEquals("browser type", jsonDeviceInfo.getString("browserType"));
        assertEquals("browser engine", jsonDeviceInfo.getString("browserEngine"));

        int exceptionCounter = 0;
        String dummyValue = "";

        try {
            dummyValue = jsonDeviceInfo.getString("deviceName");
        } catch (JSONException ex) {
            exceptionCounter++;
        }

        assertEquals(1, exceptionCounter);
        assertEquals("", dummyValue);
    }

    private DeviceInfo getDefaultDeviceInfo() {
        return new DeviceInfo.Builder().build();
    }

    private DeviceInfo getCompleteDeviceInfo() {
        return new DeviceInfo.Builder()
                .setDeviceBrand("brand")
                .setDeviceModel("model")
                .setDeviceType("type")
                .setDeviceCode("code")
                .setDeviceOsName("osName")
                .setDeviceOsVersion("osVersion")
                .setDeviceName("name")
                .setDeviceBrowserName("browser name")
                .setDeviceBrowserVersion("browser version")
                .setDeviceBrowserType("browser type")
                .setDeviceBrowserEngine("browser engine")
                .build();
    }
}
