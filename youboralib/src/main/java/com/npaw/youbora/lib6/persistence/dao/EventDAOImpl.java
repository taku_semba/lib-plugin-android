package com.npaw.youbora.lib6.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.persistence.AppDatabaseSingleton;
import com.npaw.youbora.lib6.persistence.OfflineContract;
import com.npaw.youbora.lib6.persistence.entity.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Enrique on 25/01/2018.
 */

public class EventDAOImpl implements EventDAO{

    private static EventDAOImpl instance = null;

    public static EventDAOImpl getInstance() {
        if(instance == null) {
            instance = new EventDAOImpl();
        }
        return instance;
    }

    @Override
    public long insertNewEvent(Event event){
        SQLiteDatabase db = getWritableDatabase();

        if(db == null)
            return 0;

        ContentValues values = new ContentValues();
        values.put(OfflineContract.OfflineEntry.COLUMN_NAME_JSON_EVENTS, event.getJsonEvents());
        values.put(OfflineContract.OfflineEntry.COLUMN_NAME_DATE_UPDATE, System.currentTimeMillis());
        values.put(OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID, event.getOfflineId());

        long result = db.insert(OfflineContract.OfflineEntry.TABLE_NAME, null, values);
        return result;
    }

    @Override
    public List<Event> getAll(){

        List<Event> events = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        if (db == null)
            return events;

        String[] projection = getAllColumns();

        Cursor cursor = db.query(
                OfflineContract.OfflineEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        while(cursor.moveToNext()) {
            events.add(generateEvent(cursor));
        }
        cursor.close();
        return events;
    }

    @Override
    public int getLastId(){
        SQLiteDatabase db = getReadableDatabase();

        if(db == null)
            return 0;

        String[] projection = {
                OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID
        };

        Cursor cursor = db.query(
                OfflineContract.OfflineEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // no WHERE clause
                null,                            // no WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                "offline_id DESC",                                 // The sort order
                "1"                                         //Limit to 1
        );

        while(cursor.moveToNext()) {
            int offlineId = cursor.getInt(
                    cursor.getColumnIndex(OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID));
            cursor.close();
            return offlineId;
        }
        return 0;
    }

    @Override
    public List<Event> getByOfflineId(int offlineId){

        List<Event> events = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        if (db == null)
            return events;

        String[] projection = getAllColumns();

        Cursor cursor = db.query(
                OfflineContract.OfflineEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                "offline_id = ?",                                // no WHERE clause
                new String[]{String.valueOf(offlineId)},                            // no WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null
        );

        while(cursor.moveToNext()) {
            events.add(generateEvent(cursor));
        }
        cursor.close();

        return events;

    }

    @Override
    public int getFirstId(){
        SQLiteDatabase db = getReadableDatabase();

        if (db == null)
            return 0;

        String[] projection = {
                OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID
        };

        Cursor cursor = db.query(
                OfflineContract.OfflineEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // no WHERE clause
                null,                            // no WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                "offline_id ASC",                                 // The sort order
                "1"                                         //Limit to 1
        );

        while(cursor.moveToNext()) {
            int offlineId = cursor.getInt(
                    cursor.getColumnIndex(OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID));
            cursor.close();
            return offlineId;
        }

        return 0;
    }

    @Override
    public int deleteEvents(int offlineId){

        SQLiteDatabase db = getWritableDatabase();

        if (db == null)
            return 0;
        int result = db.delete(OfflineContract.OfflineEntry.TABLE_NAME
                , OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID + " LIKE ?"
                , new String[]{String.valueOf(offlineId)});
        return result;

    }

    @Override
    public int deleteAll() {
        SQLiteDatabase db = getWritableDatabase();

        if (db == null)
            return 0;

        int result = db.delete(OfflineContract.OfflineEntry.TABLE_NAME
                , null
                , null);
        return result;
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        return AppDatabaseSingleton.getInstance() == null ? null : AppDatabaseSingleton.getInstance().getReadableDatabase();
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        return AppDatabaseSingleton.getInstance() == null ? null : AppDatabaseSingleton.getInstance().getWritableDatabase();
    }

    /*@Query("delete from event where offline_id = :offlineId")
    void deleteEventsByOfflineId(int offlineId);*/

    /*@Delete
    int deleteEvents(List<Event> events);*/

    private String[] getAllColumns(){
        return new String[] {
                OfflineContract.OfflineEntry.COLUMN_NAME_UID,
                OfflineContract.OfflineEntry.COLUMN_NAME_JSON_EVENTS,
                OfflineContract.OfflineEntry.COLUMN_NAME_DATE_UPDATE,
                OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID
        };
    }

    private Event generateEvent(Cursor cursor){

        //Init variables
        int eventId = 0;
        String eventEvents = "";
        long updated = 0;
        int offlineId = 0;

        try{
            eventId = cursor.getInt(
                    cursor.getColumnIndexOrThrow(OfflineContract.OfflineEntry.COLUMN_NAME_UID));

            eventEvents = cursor.getString(
                    cursor.getColumnIndex(OfflineContract.OfflineEntry.COLUMN_NAME_JSON_EVENTS));

            updated = cursor.getLong(
                    cursor.getColumnIndex(OfflineContract.OfflineEntry.COLUMN_NAME_DATE_UPDATE));

            offlineId = cursor.getInt(
                    cursor.getColumnIndex(OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID));
        }catch(Exception ex){
            YouboraLog.error(ex);
        }

        return new Event(eventId, eventEvents, updated, offlineId);
    }
}
