package com.npaw.youbora.lib6.persistence.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

public class InfinitySharedPreferencesManager implements InfinityStorageContract {

    private SharedPreferences sharedPreferences;

    private final String PREFERENCES_FILE_NAME = "youbora_infinity";

    //Key names
    private final String PREFERENCES_SESSION_ID_KEY = "session_id";
    private final String PREFERENCES_CONTEXT_KEY = "context_id";
    private final String PREFERENCES_LAST_ACTIVE_KEY = "last_active_id";
    private final String PREFERENCES_FINGERPRINT = "fingerprint";

    //Default values
    private final String STRING_DEFAULT_VALUE = null;
    private final Long LONG_DEFAULT_VALUE = -1L;

    public InfinitySharedPreferencesManager(Context context) {
        this.sharedPreferences = context
                .getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void saveSessionId(String sessionId) {
        saveString(PREFERENCES_SESSION_ID_KEY, sessionId);
    }

    @Override
    public String getSessionId() { return getString(PREFERENCES_SESSION_ID_KEY); }

    @Override
    public void saveContext(String context) { saveString(PREFERENCES_CONTEXT_KEY, context); }

    @Override
    public String getContenxt() { return getString(PREFERENCES_CONTEXT_KEY); }

    @Override
    public void saveLastActive() {
        Long unixTime = System.currentTimeMillis();
        saveLong(PREFERENCES_LAST_ACTIVE_KEY, unixTime);
    }

    @Override
    public Long getLastActive() { return getLong(PREFERENCES_LAST_ACTIVE_KEY); }

    @Override
    public void saveFingerprint(String fingerprint) {
        saveString(PREFERENCES_FINGERPRINT, fingerprint);
    }

    @Override
    public String getFingerPrint() {
        return getString(PREFERENCES_FINGERPRINT);
    }

    private void saveString(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    private String getString(String key) {
        return sharedPreferences.getString(key, STRING_DEFAULT_VALUE);
    }

    private void saveLong(String key, Long value) {
        sharedPreferences.edit().putLong(key, value).apply();
    }

    private Long getLong(String key) { return sharedPreferences.getLong(key, LONG_DEFAULT_VALUE); }
}
