package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.persistence.EventDataSource;
import com.npaw.youbora.lib6.persistence.entity.Event;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Enrique on 20/07/2017.
 */

public class OfflineTransform extends Transform {

    private EventDataSource dataSource;

    private boolean startSaved;
    private ArrayList<Map<String,Object>>  queuedEvents;

    public OfflineTransform() {
        sendRequest = false;
        isBusy = false;

        startSaved = false;
        queuedEvents = new ArrayList<>();
    }

    @Override
    public void parse(Request request) {
        if (request != null && request.getParams() != null) {
            Map<String,Object> messageData = request.getParams();
            saveEvent(messageData, request.getService().substring(1));
        }
    }

    @Override
    public boolean hasToSend(Request request) {
        return false;
    }

    @Override
    public int getState() {
        return Transform.STATE_OFFLINE;
    }

    /**
     * Appends the new offline event to the existing ones
     *
     * @param service Event service
     */
    private void saveEvent(final Map<String,Object> messageData, final String service){
        dataSource = new EventDataSource();
        dataSource.initDatabase();

        if(service.equals(Constants.SERVICE_INIT.substring(1))){
            return;
        }

        messageData.put("request",service);
        messageData.put("unixtime",System.currentTimeMillis());

        if (startSaved || service.equals(Constants.SERVICE_START.substring(1))) {
            dataSource.getLastId(new EventDataSource.QuerySuccessListener() {
                @Override
                public void onQueryResolved(Object queryResult) {
                    final int finalOffline_id = (Integer) queryResult;
                    dataSource.getAllEvents(new EventDataSource.QuerySuccessListener() {
                        @Override
                        public void onQueryResolved(Object queryResult) {
                            final int eventsCount = ((List<Event>) queryResult).size();
                            int offline_id = finalOffline_id;
                            if(eventsCount != 0 && service.equals(Constants.SERVICE_START.substring(1))){
                                offline_id++;
                            }
                            messageData.put("code",String.format("[VIEW_CODE]_%s",String.valueOf(offline_id)));
                            JSONObject json = new JSONObject(messageData);

                            YouboraLog.debug(String.format("Saving offline event %s: %s", service, json.toString()));
                            Event event = new Event(json.toString(), System.currentTimeMillis(), offline_id);
                            if (service.equals(Constants.SERVICE_START.substring(1))) {
                                dataSource.insertNewElement(event, new EventDataSource.QuerySuccessListener() {
                                    @Override
                                    public void onQueryResolved(Object queryResult) {
                                        startSaved = true;
                                        processQueue();
                                    }
                                });
                            } else if (service.equals(Constants.SERVICE_STOP.substring(1))) {
                                dataSource.insertNewElement(event, new EventDataSource.QuerySuccessListener() {
                                    @Override
                                    public void onQueryResolved(Object queryResult) {
                                        startSaved = false;
                                    }
                                });
                            } else {
                                dataSource.insertNewElement(event, null);
                            }
                        }
                    });
                }
            });
        } else {
            queuedEvents.add(messageData);
        }
    }

    void processQueue() {
        for (int k = 0 ; k < queuedEvents.size() ; k++) {
            saveEvent(queuedEvents.get(k), (String)queuedEvents.get(k).get("request"));
        }
    }
}
