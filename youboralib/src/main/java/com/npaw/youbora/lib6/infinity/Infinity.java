package com.npaw.youbora.lib6.infinity;

import android.content.Context;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.comm.Communication;
import com.npaw.youbora.lib6.comm.transform.ViewTransform;
import com.npaw.youbora.lib6.comm.transform.infinity.TimestampLastSentTransform;
import com.npaw.youbora.lib6.persistence.sharedpreferences.InfinitySharedPreferencesManager;
import com.npaw.youbora.lib6.persistence.sharedpreferences.InfinityStorageContract;
import com.npaw.youbora.lib6.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Infinity {
    private static final Infinity ourInstance = new Infinity();
    public static boolean isInitiated = false;

    private Plugin plugin;
    private Context context;
    private InfinityStorageContract infinityStorage;
    private Communication communication;

    private ViewTransform viewTransform;

    private InfinityFlags flags;

    //List of active sessions
    private List<String> activeSessions;

    // Listener list
    protected List<InfinityEventListener> eventListeners = new ArrayList<>();

    public static Infinity getInstance() {
        isInitiated = true;
        return ourInstance;
    }

    private Infinity() {
        flags = new InfinityFlags();
    }

    public void setPlugin(Plugin plugin){
        this.plugin = plugin;
    }

    /**
     * Application context, it's important to pass to it the application context, not the view / activity one,
     * doing that will lead to memory leaks
     * @param context Application context
     */
    public void setContext(Context context){
        this.context = context;
    }

    public void begin(String screenName){
        begin(screenName, null);
    }

    public void begin(String screenName, Map<String, String> dimensions) {
        begin(screenName, dimensions, null);
    }

    public void begin(String screenName, Map<String, String> dimensions, String parentId) {

        if (context == null) {
            YouboraLog.error("Context is null, have the context been set?");
            return;
        }

        if (plugin == null) {
            YouboraLog.error("Plugin is null, have the plugin been set?");
            return;
        }

        if (!getFlags().isStarted()) {
            getFlags().setStarted(true);
            communication = new Communication();
            if (viewTransform != null) {
                communication.addTransform(viewTransform);
            }
            communication.addTransform(new TimestampLastSentTransform(context));

            activeSessions = null;
            fireSessionStart(screenName, dimensions, parentId);
        } else {
            fireNav(screenName);
        }
    }

    public void fireSessionStart(String screenName, Map<String, String> dimensions,
                                 String parentId) {
        infinityStorage = new InfinitySharedPreferencesManager(context);
        //We use the timestamp as a unique id "locally"
        generateNewContext();
        for (InfinityEventListener l : eventListeners) {
            l.onSessionStart(screenName, dimensions, parentId);
        }
    }

    public void fireNav(String screenName) {
        for (InfinityEventListener l : eventListeners) {
            l.onNav(screenName);
        }
    }

    public void fireEvent(Map<String, String> dimensions, Map<String, Double> values,
                          String eventName) {
        for (InfinityEventListener l : eventListeners) {
            l.onEvent(dimensions, values, eventName);
        }
    }

    public void fireSessionStop(Map<String, String> params) {
        if (getFlags().isStarted()) {
            for (InfinityEventListener l : eventListeners) {
                l.onSessionStop(params);
            }
        }
    }

    public void end(){
        end(null);
    }

    public void end(Map<String, String> params) {
        if (getFlags().isStarted()) {
            getFlags().reset();
            fireSessionStop(params);
        }

    }

    /**
     * Adds an event listener that will be invoked whenever a "fire*" method is called.
     * @param eventListener the listener to add
     */
    public void addEventListener(InfinityEventListener eventListener){
        eventListeners.add(eventListener);
    }

    /**
     * Remove an event listener
     * @param eventListener listener ot remove
     * @return whether the listener has been removed or not
     */
    public boolean removeEventListener(InfinityEventListener eventListener) {
        return eventListeners.remove(eventListener);
    }

    public Communication getCommunication() {
        return communication;
    }

    private void generateNewContext(){
        //For now we only will have one context that will be the app name
        infinityStorage.saveContext(YouboraUtil.getApplicationName(context));
    }

    public void setViewTransform(ViewTransform viewTransform) {
        this.viewTransform = viewTransform;
    }

    public InfinityFlags getFlags(){
        return flags;
    }

    public String getNavContext(){
        return YouboraUtil.getApplicationName(context);
    }

    public Long getLastSent(){
        return infinityStorage.getLastActive();
    }

    public void addActiveSession(String sessionId) {
        if (activeSessions == null)
            activeSessions = new ArrayList<>(1);
        activeSessions.add(sessionId);
    }

    public void removeActiveSession(String sessionId) {
        if (activeSessions == null)
            return;
        activeSessions.remove(sessionId);
    }

    public List<String> getActiveSessions() {
        return activeSessions;
    }

    /**
     * Listener interface. The methods will be called whenever each corresponding event is fired.
     * These events are listened by the {@link Plugin} in order to send the corresponding request.
     * They are only used by Infinity class
     */
    public interface InfinityEventListener{
        /**
         * Infinity detected session start event.
         * @param screenName name to be displayed in Youbora
         * @param dimensions map with all desired dimensions
         * @param parentId name of the sent event
         */
        void onSessionStart(String screenName, Map<String, String> dimensions, String parentId);

        /**
         * Infinity detected session stop event.
         * @param params params to add to the request
         */
        void onSessionStop(Map<String, String> params);

        /**
         * Infinity detected session nav event.
         * @param screenName name to be displayed in Youbora
         */
        void onNav(String screenName);

        /**
         * Infinity detected session nav event.
         * @param dimensions map with all desired dimensions
         * @param values map with all desired values
         * @param eventName name of the sent event
         */
        void onEvent(Map<String, String> dimensions, Map<String, Double> values, String eventName);
    }

    /**
     * Convenience class that provides an empty implementation for all the methods in the
     * {@link Infinity.InfinityEventListener}.
     * Use in case that you only want to register to a subset of all the listeners.
     */
    public static class InfinityEventListenerImpl implements InfinityEventListener {

        @Override
        public void onSessionStart(String screenName, Map<String, String> dimensions, String parentId) { }

        @Override
        public void onSessionStop(Map<String, String> params) { }

        @Override
        public void onNav(String screenName) { }

        @Override
        public void onEvent(Map<String, String> dimensions, Map<String, Double> values, String eventName) { }
    }
}
