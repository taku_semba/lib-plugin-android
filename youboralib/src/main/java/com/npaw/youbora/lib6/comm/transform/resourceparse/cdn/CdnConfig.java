package com.npaw.youbora.lib6.comm.transform.resourceparse.cdn;

import com.npaw.youbora.lib6.comm.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An instance of this class has all the info and logic needed in order to "match" against a
 * particular CDN and get its node host and type (hit or miss).
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public class CdnConfig {

    private String code;
    private List<CdnParsableResponseHeader> parsers;
    private Map<String, String> requestHeaders;
    private CdnTypeParser typeParser;

    /**
     * Request method to use when requesting for info on the CDN
     */
    private String requestMethod;

    /**
     * Constructor
     */
    public CdnConfig() {
        code = null;
        parsers = new ArrayList<>();
        requestHeaders = new HashMap<>();
        requestMethod = Request.METHOD_HEAD;
        typeParser = null;
    }

    /**
     * Constructor
     * @param code the CDN code, one of the following <a href="http://mapi.youbora.com:8081/cdns">list</a>.
     */
    public CdnConfig(String code) {
        this();
        this.code = code;
    }

    /**
     * CDN code
     * @return the code that represents this CDN
     */
    public String getCode() { return code; }

    /**
     * Sets the cdn code
     * @param code the code to set
     * @return itself to chain method calls
     */
    public CdnConfig setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * Returns the List of {@link CdnParsableResponseHeader}
     * @return the List of {@link CdnParsableResponseHeader}
     */
    public List<CdnParsableResponseHeader> getParsers() { return parsers; }

    /**
     * Adds a {@link CdnParsableResponseHeader}
     * @param parser the {@link CdnParsableResponseHeader} to add
     * @return itself to chain method calls
     */
    public CdnConfig addParser(CdnParsableResponseHeader parser) {
        parsers.add(parser);
        return this;
    }

    /**
     * Returns a map of the request headers.
     * @see #setRequestHeader(String, String)
     * @return the request headers
     */
    public Map<String, String> getRequestHeaders() { return requestHeaders; }

    /**
     * Adds a request header key:value pair.
     * This headers will be added to the HEAD request used to get the CDN info. Some CDNs need
     * special headers to be set in a request in order to respond with the info we need.
     * @param key header name
     * @param value header value
     * @return itself to chain method calls
     */
    public CdnConfig setRequestHeader(String key, String value) {
        requestHeaders.put(key, value);
        return this;
    }

    /**
     * Get the current {@link CdnTypeParser}
     * @return the {@link CdnTypeParser}
     */
    public CdnTypeParser getTypeParser() { return typeParser; }

    /**
     * Set the {@link CdnTypeParser} that will be used to parse the cdn info. Once the http HEAD
     * request is performed, this parser will be invoked with the http response to parse it and
     * extract the CDN host and/or type.
     * @see CdnTypeParser
     * @param typeParser instance of {@link CdnTypeParser}
     * @return itself to chain method calls
     */
    public CdnConfig setTypeParser(CdnTypeParser typeParser) {
        this.typeParser = typeParser;
        return this;
    }

    /**
     * Get the current request method
     * @return requestMethod
     */
    public String getRequestMethod() { return requestMethod; }

    /**
     * Sets the new request method
     * @param requestMethod
     */
    public CdnConfig setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
        return this;
    }
}
