package com.npaw.youbora.lib6.comm.transform.infinity;

import android.content.Context;

import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.Transform;
import com.npaw.youbora.lib6.persistence.sharedpreferences.InfinitySharedPreferencesManager;
import com.npaw.youbora.lib6.persistence.sharedpreferences.InfinityStorageContract;

public class TimestampLastSentTransform extends Transform {

    private InfinityStorageContract infinityStorage;

    public TimestampLastSentTransform(Context context){
        infinityStorage = new InfinitySharedPreferencesManager(context);
        done();
    }

    @Override
    public void parse(Request request) {
        infinityStorage.saveLastActive();
    }
}
