package com.npaw.youbora.lib6.infinity;

public class InfinityFlags {
    private boolean started;

    public InfinityFlags() {
        reset();
    }

    /**
     * Reset all flag values to false.
     */
    public void reset() {
        started = false;
    }

    /**
     * Returns the started flag.
     * @return the started flag.
     */
    public boolean isStarted() {
        return started;
    }

    /**
     * Sets the started flag.
     * @param started new value
     */
    public void setStarted(boolean started) {
        this.started = started;
    }
}
