package com.npaw.youbora.lib6.persistence;

import android.content.Context;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.persistence.helper.EventDbHelper;

/**
 * Created by Enrique on 27/12/2017.
 */

public class AppDatabaseSingleton {

    private static EventDbHelper databaseSingleton = null;
    public static boolean initCalled = false;

    public static synchronized EventDbHelper getInstance() {
        if(!initCalled){
            YouboraLog.warn("AppDatabaseSingleton.Init(context) has to be called first with a valid context in order to get Instance");
        }
        return databaseSingleton;
    }

    private AppDatabaseSingleton() {

    }

    public static void init(Context context) {
        if (databaseSingleton == null) {
            if(context == null){
                YouboraLog.error("Context can't be null");
                return;
            }

            databaseSingleton = new EventDbHelper(context);

            initCalled = true;
        }
    }
}
