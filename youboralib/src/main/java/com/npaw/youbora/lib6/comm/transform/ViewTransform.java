package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import static com.npaw.youbora.lib6.Constants.SERVICE_DATA;
import static com.npaw.youbora.lib6.Constants.SERVICE_INIT;
import static com.npaw.youbora.lib6.Constants.SERVICE_OFFLINE_EVENTS;
import static com.npaw.youbora.lib6.Constants.SERVICE_PING;
import static com.npaw.youbora.lib6.Constants.SERVICE_SESSION_BEAT;
import static com.npaw.youbora.lib6.Constants.SERVICE_SESSION_EVENT;
import static com.npaw.youbora.lib6.Constants.SERVICE_SESSION_NAV;
import static com.npaw.youbora.lib6.Constants.SERVICE_SESSION_START;
import static com.npaw.youbora.lib6.Constants.SERVICE_SESSION_STOP;
import static com.npaw.youbora.lib6.Constants.SERVICE_START;

/**
 * Manages Fastdata service interaction and view codes generation.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public class ViewTransform extends Transform{

    private Request request;
    private Map<String, String> params;
    private int viewIndex;
    private Plugin plugin;
    private String viewCode = null;

    /**
     * Instance of {@link FastDataConfig}
     */
    public FastDataConfig fastDataConfig;

    public ViewTransform(Plugin plugin) {
        super();

        this.plugin = plugin;

        fastDataConfig = new FastDataConfig();
        viewIndex = -1;

        String service = SERVICE_DATA;
        params = new HashMap<>();
        params.put("apiVersion", "v6,v7");
        params.put("outputformat", "jsonp");
        params = plugin.getRequestBuilder().buildParams(params, service);
        if (params != null) {
            if ("nicetest".equals(params.get("system"))) {
                // "nicetest" is the default accountCode.
                // If found here, it's very likely that the customer has forgotten to set it.
                YouboraLog.error("No accountCode has been set. Please set your accountCode in plugin's options.");
            }

            // Prepare request but don't send it yet
            request = createRequest(plugin.getHost(), service);
            Map<String, Object> paramsObjectMap = new HashMap<String, Object>(params);
            request.setParams(paramsObjectMap);
        }
    }

    /**
     * Starts the 'FastData' fetching. This will send the initial request to YOUBORA in order to get
     * the needed info for the rest of the requests.
     *
     * This is an asynchronous process.
     *
     * When the fetch is complete, {@link #fastDataConfig} will contain the parsed info.
     * @see FastDataConfig
     */
    public void init() {
       if(plugin != null && plugin.getOptions() != null && plugin.getOptions().isOffline()){
            fastDataConfig.code = "OFFLINE_MODE";
            fastDataConfig.host = "OFFLINE_MODE";
            fastDataConfig.pingTime = 60;
            buildCode(true);
            done();
            YouboraLog.debug("Offline mode, skipping fastdata request...");
            return;
        }
        requestData();
    }

    private void requestData() {
        request.addOnSuccessListener(new Request.RequestSuccessListener() {
            @Override
            public void onRequestSuccess(HttpURLConnection connection, String responseString, Map<String, Object> listenerParams) {

                if (responseString == null || responseString.length() == 0) {
                    YouboraLog.error("FastData empty response");
                    return;
                }

                try {
                    responseString = responseString.substring(7, responseString.length() - 1);

                    JSONObject outerJson = createJSONFromString(responseString);

                    if (outerJson.has("q")) {
                        JSONObject innerJson = outerJson.getJSONObject("q");

                        String host = "";
                        String code = "";
                        String pt = "";
                        String bt = "";
                        String exp = "";
                        String yid = null;

                        if (innerJson.has("h")) host = innerJson.getString("h");

                        if (innerJson.has("c")) code = innerJson.getString("c");

                        if (innerJson.has("pt")) pt = innerJson.getString("pt");

                        if (innerJson.has("i")) {
                            if (innerJson.getJSONObject("i").has("bt"))
                                bt = innerJson.getJSONObject("i").getString("bt");

                            if (innerJson.getJSONObject("i").has("exp"))
                                exp = innerJson.getJSONObject("i").getString("exp");
                        }

                        if (innerJson.has("f")) {
                            if (innerJson.getJSONObject("f").has("yid"))
                                yid = innerJson.getJSONObject("f").getString("yid");
                        }

                        if (host.length() > 0 && code.length() > 0 && pt.length() > 0) {

                            if (fastDataConfig == null) fastDataConfig = new FastDataConfig();

                            fastDataConfig.code = code;
                            Options options = plugin.getOptions();
                            fastDataConfig.host = YouboraUtil.addProtocol(
                                    host, options != null && options.isHttpSecure());
                            fastDataConfig.pingTime = Integer.parseInt(pt);

                            if (bt.length() > 0)
                                fastDataConfig.beatTime = Integer.parseInt(bt);
                            else
                                fastDataConfig.beatTime = 30;

                            if (exp.length() > 0)
                                fastDataConfig.expirationTime = Integer.parseInt(exp);
                            else
                                fastDataConfig.expirationTime = 300;

                            fastDataConfig.youboraId = yid;

                            buildCode();

                            YouboraLog.notice(String.format("FastData '%s' is ready.", code));

                            done();
                        } else {
                            YouboraLog.error("FastData response is wrong.");
                        }
                    } else {
                        YouboraLog.error("FastData response is wrong.");
                    }
                } catch (Exception e) {
                    YouboraLog.error("FastData response is wrong.");
                    YouboraLog.error(e);
                }
            }
        });

        request.addOnErrorListener(new Request.RequestErrorListener() {
            @Override
            public void onRequestError(HttpURLConnection connection) {
                YouboraLog.error("Fastdata request failed.");
            }
        });

        request.send();
    }

    /**
     * {@inheritDoc}
     *
     * ViewTransform will set the correct view code to each request and set the pingTime param
     * for the services that need to carry it.
     */
    @Override
    public void parse(Request request) {
        Map<String, Object> params = request.getParams();

        if (request.getHost() == null || request.getHost().length() == 0)
            request.setHost(fastDataConfig.host);

        if (params.get("code") == null) {
            if (request.getService().equals(SERVICE_OFFLINE_EVENTS)) nextView();

            params.put("code", getViewCode());
        }

        if (params.get("sessionRoot") == null) params.put("sessionRoot", fastDataConfig.code);

        if (plugin.getIsInfinity() != null && plugin.getIsInfinity()) {
            if (params.get("sessionId") == null) params.put("sessionId", fastDataConfig.code);
        }

        if (plugin.getOptions().getAccountCode() != null)
            params.put("accountCode", plugin.getOptions().getAccountCode());

        // Request-specific transforms
        switch (request.getService()) {
            case SERVICE_PING:
            case SERVICE_START:
                if (params.get("pingTime") == null) params.put("pingTime", fastDataConfig.pingTime);
                if (params.get("sessionParent") == null &&
                        plugin.getIsInfinity() != null && plugin.getIsInfinity())
                    params.put("sessionParent", fastDataConfig.code);
                break;
            case SERVICE_OFFLINE_EVENTS:
                request.setBody(addCodeToEvents(request.getBody()));
                break;
            case SERVICE_SESSION_START:
                if (params.get("beatTime") == null) params.put("beatTime", fastDataConfig.beatTime);
                if (params.get("code") == getViewCode()) params.put("code", fastDataConfig.code);
                break;
            case SERVICE_SESSION_BEAT:
            case SERVICE_SESSION_NAV:
            case SERVICE_SESSION_STOP:
            case SERVICE_SESSION_EVENT:
                if (params.get("code") == getViewCode()) params.put("code", fastDataConfig.code);
                break;
            default:
        }

        if (request.getService().equals(SERVICE_START) ||
                request.getService().equals(SERVICE_INIT) ||
                request.getService().equals(SERVICE_SESSION_START))
            params.put("youboraId", fastDataConfig.youboraId);
    }

    private String addCodeToEvents(String body) {
        if (body != null) return body.replace("[VIEW_CODE]",this.fastDataConfig.code);

        return null;
    }

    JSONObject createJSONFromString(String string) throws JSONException {
        return new JSONObject(string);
    }

    Request createRequest(String host, String service) {
        return new Request(host, service);
    }

    /**
     * Increments the view counter and generates a new view code.
     * @return the new view code
     */
    public String nextView() {
        viewIndex++;
        buildCode();
        return getViewCode();
    }

    /**
     * Builds the view code. It has the following scheme:
     * [fast data response code]_[view count]
     *
     * The only thing that matters is that view codes are unique. For this reason we only ask the
     * backend only once for a code, and then append a view counter that is incremented with each
     * view.
     * @see {@link #nextView()}
     */
    private void buildCode() {
        buildCode(false);
    }

    /**
     * Builds the view code. It has the following scheme:
     * [fast data response code]_[view count]
     *
     * The only thing that matters is that view codes are unique. For this reason we only ask the
     * backend only once for a code, and then append a view counter that is incremented with each
     * view.
     * @param isOffline
     * @see {@link #nextView()}
     */
    private void buildCode(boolean isOffline) {
        String suffix = isOffline ? Integer.toString(viewIndex) : getViewCodeTimeStamp();

        if (fastDataConfig.code != null && fastDataConfig.code.length() > 0)
            viewCode = fastDataConfig.code + "_" + suffix;
        else
            viewCode = null;
    }

    /**
     * Current view code
     * @return the current view code
     */
    private String getViewCode() { return viewCode; }

    public String getViewCodeTimeStamp() { return Long.toString(System.currentTimeMillis()); }

    /**
     * Container class that has all the info we need from YOUBORA's 'FastData' service.
     */
    public static class FastDataConfig {

        /**
         * The YOUBORA host where to send traces
         */
        public String host;

        /**
         * Unique view identifier.
         */
        public String code;

        /**
         * Ping time; how often should pings be reported. This is per-account configurable
         * although 99% of the time this is 5 seconds.
         */
        public Integer pingTime;

        /**
         * Beat time: how often should beats be reported. This is a per-account configurable
         * although 99% of the time this is 30 seconds.
         */
        public Integer beatTime;

        /**
         * The maximum time (in seconds) is going to last without expiring and sending session start again
         */
        public Integer expirationTime;

        /**
         * Youbora id used internally to identify every device.
         */
        public String youboraId;
    }
}
