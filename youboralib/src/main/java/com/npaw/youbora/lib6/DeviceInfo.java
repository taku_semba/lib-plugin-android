package com.npaw.youbora.lib6;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Utility class to obtain as much device information as possible, some values are set by default
 */
public class DeviceInfo {

    /**
     * Device model (e.g. MI 5), set by default using android.os.Build.MODEL
     */
    private String deviceModel;

    /**
     * Device brand (e.g. Xiaomi), set by default using android.os.Build.BRAND
     */
    private String deviceBrand;

    /**
     * Device type (e.g. pc, smartphone, stb, tv)
     */
    private String deviceType;

    /**
     * Device name, internal identifier for the device
     */
    private String deviceName;

    /**
     * Youbora's device code. If specified it will rewrite info gotten from user agent.
     * See a list of codes in <a href="http://mapi.youbora.com:8081/devices">http://mapi.youbora.com:8081/devices</a>.
     */
    private String deviceCode;

    /**
     * Device OS name (e.g. Android)
     */
    private String deviceOsName;

    /**
     * Device OS version (e.g. 8.1.0), set by default using android.os.Build.VERSION.RELEASE
     */
    private String deviceOsVersion;

    /**
     * Device browser name, unset by default
     */
    private String deviceBrowserName;

    /**
     * Device browser version, unset by default
     */
    private String deviceBrowserVersion;

    /**
     * Device browser type, unset by default
     */
    private String deviceBrowserType;

    /**
     * Device browser engine, unset by default
     */
    private String deviceBrowserEngine;

    public DeviceInfo() {
    }

    /**
     * Getter for device model
     * @return device model
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * Getter for device brand
     * @return device brand
     */
    public String getDeviceBrand() {
        return deviceBrand;
    }

    /**
     * Getter for device type
     * @return device type
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Getter for device name
     * @return device name
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Getter for device code
     * @return device code
     */
    public String getDeviceCode() {
        return deviceCode;
    }

    /**
     * Getter for device OS name
     * @return device OS name
     */
    public String getDeviceOsName() {
        return deviceOsName;
    }

    /**
     * Getter for device OS version
     * @return device OS version
     */
    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    /**
     * Getter for device browser name
     * @return device browser name
     */
    public String getDeviceBrowserName() {
        return deviceBrowserName;
    }

    /**
     * Getter for device browser version
     * @return device browser version
     */
    public String getDeviceBrowserVersion() {
        return deviceBrowserVersion;
    }

    /**
     * Getter for device browser type
     * @return device browser type
     */
    public String getDeviceBrowserType() {
        return deviceBrowserType;
    }

    /**
     * Getter for device browser engine
     * @return device browser engine
     */
    public String getDeviceBrowserEngine() {
        return deviceBrowserEngine;
    }

    /**
     * This method maps all the device values to a JSON String
     * @return all device info as a JSON String
     */
    public String mapToJSONString(){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("model",getDeviceModel());
            jsonObject.put("osVersion",getDeviceOsVersion());
            jsonObject.put("brand",getDeviceBrand());
            if (getDeviceType() != null) jsonObject.put("deviceType",getDeviceType());
            if (getDeviceCode() != null) jsonObject.put("deviceCode",getDeviceCode());
            if (getDeviceOsName() != null) jsonObject.put("osName",getDeviceOsName());
            jsonObject.put("browserName",getDeviceBrowserName());
            jsonObject.put("browserVersion",getDeviceBrowserVersion());
            jsonObject.put("browserType",getDeviceBrowserType());
            jsonObject.put("browserEngine",getDeviceBrowserEngine());
        } catch (JSONException jsonException) {
            YouboraLog.error(jsonException);
        }
        return jsonObject.toString();
    }

    /**
     * This Builder is used only for {@link DeviceInfo} class, since it has too many option parameters
     */
    public static class Builder {
        private String deviceModel;
        private String deviceBrand;
        private String deviceType;
        private String deviceName;
        private String deviceCode;
        private String deviceOsName;
        private String deviceOsVersion;
        private String deviceBrowserName;
        private String deviceBrowserVersion;
        private String deviceBrowserType;
        private String deviceBrowserEngine;

        public Builder() {
        }

        public Builder setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
            return this;
        }

        public Builder setDeviceBrand(String deviceBrand) {
            this.deviceBrand = deviceBrand;
            return this;
        }

        public Builder setDeviceType(String deviceType) {
            this.deviceType = deviceType;
            return this;
        }

        public Builder setDeviceName(String deviceName) {
            this.deviceName = deviceName;
            return this;
        }

        public Builder setDeviceCode(String deviceCode) {
            this.deviceCode = deviceCode;
            return this;
        }

        public Builder setDeviceOsName(String deviceOsName) {
            this.deviceOsName = deviceOsName;
            return this;
        }

        public Builder setDeviceOsVersion(String deviceOsVersion) {
            this.deviceOsVersion = deviceOsVersion;
            return this;
        }

        public Builder setDeviceBrowserName(String deviceBrowserName) {
            this.deviceBrowserName = deviceBrowserName;
            return this;
        }

        public Builder setDeviceBrowserVersion(String deviceBrowserVersion) {
            this.deviceBrowserVersion = deviceBrowserVersion;
            return this;
        }

        public Builder setDeviceBrowserType(String deviceBrowserType) {
            this.deviceBrowserType = deviceBrowserType;
            return this;
        }

        public Builder setDeviceBrowserEngine(String deviceBrowserEngine) {
            this.deviceBrowserEngine = deviceBrowserEngine;
            return this;
        }

        public DeviceInfo build() {
            DeviceInfo deviceInfo = new DeviceInfo();
            deviceInfo.deviceModel = this.deviceModel == null ? android.os.Build.MODEL : this.deviceModel;
            deviceInfo.deviceBrand = this.deviceBrand == null ? android.os.Build.BRAND : this.deviceBrand;
            deviceInfo.deviceType = this.deviceType;
            deviceInfo.deviceName = this.deviceName;
            deviceInfo.deviceCode = this.deviceCode;
            deviceInfo.deviceOsName = this.deviceOsName;
            deviceInfo.deviceOsVersion = this.deviceOsVersion == null ? android.os.Build.VERSION.RELEASE : this.deviceOsVersion;
            deviceInfo.deviceBrowserName = this.deviceBrowserName == null ? "" : this.deviceBrowserName;
            deviceInfo.deviceBrowserVersion = this.deviceBrowserVersion == null ? "" : this.deviceBrowserVersion;
            deviceInfo.deviceBrowserType = this.deviceBrowserType == null ? "" : this.deviceBrowserType;
            deviceInfo.deviceBrowserEngine = this.deviceBrowserEngine == null ? "" : this.deviceBrowserEngine;

            return deviceInfo;
        }
    }
}
