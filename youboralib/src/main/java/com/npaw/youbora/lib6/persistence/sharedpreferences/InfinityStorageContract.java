package com.npaw.youbora.lib6.persistence.sharedpreferences;

public interface InfinityStorageContract {
    void saveSessionId(String sessionId);
    String getSessionId();

    void saveContext(String context);
    String getContenxt();

    void saveLastActive();
    Long getLastActive();

    void saveFingerprint(String fingerprint);
    String getFingerPrint();
}
