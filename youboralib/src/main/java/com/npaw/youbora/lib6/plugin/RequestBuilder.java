package com.npaw.youbora.lib6.plugin;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.infinity.Infinity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.npaw.youbora.lib6.Constants.*;

/**
 * This class helps building params associated with each event: /start, /joinTime...
 * @author      Nice People at Work
 * @since       6.0
 */
public class RequestBuilder {

    /**
     * Plugin instance
     */
    private Plugin plugin;

    private Map<String, String> lastSent;

    /** Lists of params used by each service */
    private static final Map<String, String []> params = new HashMap<String, String []>() {{
        String[] startParams = new String[] {"accountCode", "username", "anonymousUser",
                "rendition", "title", "title2", "live", "mediaDuration", "mediaResource",
                "transactionCode", "properties", "cdn", "playerVersion", "param1", "param2",
                "param3", "param4", "param5", "param6", "param7", "param8", "param9", "param10",
                "pluginVersion", "pluginInfo", "isp", "connectionType", "ip", "obfuscateIp",
                "deviceCode", "preloadDuration", "player","deviceInfo", "userType",
                "streamingProtocol", "experiments", "param11", "param12", "param13", "param14",
                "param15", "param16", "param17", "param18", "param19", "param20", "householdId",
                "navContext", "smartswitchConfigCode", "smartswitchGroupCode",
                "smartswitchContractCode", "nodeType", "nodeHost", "appName", "appReleaseVersion"};

        String[] adStartParams = new String[] {"playhead", "adTitle", "adPosition", "adCampaign",
                "adResource", "adPlayerVersion", "adProperties", "adAdapterVersion", "extraparam1",
                "extraparam2", "extraparam3", "extraparam4", "extraparam5", "extraparam6",
                "extraparam7", "extraparam8", "extraparam9", "extraparam10"};

        put(SERVICE_DATA, new String[] {"system", "pluginVersion", "username", "isInfinity",
                "fingerprint"});

        put(SERVICE_INIT, startParams);
        put(SERVICE_START, startParams);
        put(SERVICE_JOIN, new String[] {"joinDuration", "playhead", "mediaDuration"});
        put(SERVICE_PAUSE, new String[] {"playhead"});
        put(SERVICE_RESUME, new String[] {"pauseDuration", "playhead"});
        put(SERVICE_SEEK, new String[] {"seekDuration", "playhead"});
        put(SERVICE_BUFFER, new String[] {"bufferDuration", "playhead"});
        put(SERVICE_STOP, new String[] {"pauseDuration", "bitrate", "playhead"});
        put(SERVICE_AD_START, adStartParams);
        put(SERVICE_AD_INIT, adStartParams);
        put(SERVICE_AD_JOIN, new String[] {"adPosition", "adJoinDuration", "adPlayhead",
                "playhead"});

        put(SERVICE_AD_PAUSE, new String[] {"adPosition", "adPlayhead", "playhead"});
        put(SERVICE_AD_RESUME, new String[] {"adPosition", "adPlayhead", "adPauseDuration",
                "playhead"});

        put(SERVICE_AD_BUFFER, new String[] {"adPosition", "adPlayhead", "adBufferDuration",
                "playhead"});

        put(SERVICE_AD_STOP, new String[] {"adPosition", "adPlayhead", "adBitrate",
                "adTotalDuration", "playhead"});

        put(SERVICE_AD_CLICK, new String[] {"adPosition", "adPlayhead", "adUrl", "playhead"});
        put(SERVICE_PING, new String[] {"droppedFrames", "playrate", "latency", "packetLoss",
                "packetSent"});

        List<String> startList = new ArrayList<>(Arrays.asList(startParams));
        startList.add("player");
        List<String> adStartList = new ArrayList<>(Arrays.asList(adStartParams));
        adStartList.add("adDuration");
        adStartList.add("adTotalDuration");
        adStartList.add("adPlayhead");
        //startList.toArray();
        put(SERVICE_ERROR, startList.toArray(new String[0]));
        put(SERVICE_AD_ERROR, adStartList.toArray(new String[0]));

        // Infinity
        put(SERVICE_SESSION_START, new String[] {"accountCode", "username", "navContext",
                "language", "pluginInfo", "appName", "appReleaseVersion"});
        put(SERVICE_SESSION_EVENT, new String[] {"accountCode", "navContext"});
        put(SERVICE_SESSION_NAV, new String[] {"username", "navContext"});
        put(SERVICE_SESSION_BEAT, new String[] {});
        put(SERVICE_SESSION_STOP, new String[] {});
    }};

    /** Lists of params used by each service (only if they are different) */
    private static final Map<String, String []> differentParams = new HashMap<String, String []>() {{
        put(SERVICE_JOIN, new String[] {"title", "title2", "live", "mediaDuration", "mediaResource"});
        put(SERVICE_AD_JOIN, new String[] {"adTitle", "adDuration", "adResource"});

    }};

    /** Array of entities that should be reported in pings if they change mid-view */
    private static final String[] pingEntities = new String[] {"rendition", "title", "title2",
            "live", "mediaDuration", "mediaResource", "param1", "param2", "param3", "param4", 
            "param5", "param6", "param7", "param8", "param9", "param10", "connectionType", 
            "deviceCode", "ip", "username", "cdn", "nodeHost", "nodeType", "nodeTypeString"};

    /** Array of

    /**
     * Constructor
     * @param plugin {@link Plugin} instance where to get the info from
     */
    public RequestBuilder(Plugin plugin) {
        this.plugin = plugin;
        lastSent = new HashMap<>();
    }

    /**
     * Adds to params Map all the entities specified that correspond to the given service.
     *
     * @param params Map of key:value entries
     * @param service The name of the service.
     * @return Map with built params
     */
    public Map<String, String> buildParams(Map<String, String> params, String service) {
        if (params == null) {
            params = new HashMap<>();
        }
        params = fetchParams(params, RequestBuilder.params.get(service), false);
        params = fetchParams(params, RequestBuilder.differentParams.get(service), true);
        // TODO Move this to every request not on buildParams
        long unixTime = System.currentTimeMillis();
        params.put("timemark",String.valueOf(unixTime));

        return params;
    }

    /**
     * Adds to params all the entities specified in paramList, unless they are already set.
     *
     * Convenience method to call {@link #fetchParams(Map, List, boolean)} with an array instead of
     * a list.
     *
     * @param params Map of params key:value.
     * @param paramList An array of params to fetch.
     * @param onlyDifferent If true, only fetches params that have changed since the last
     * @return fetched params
     */
    public Map<String, String> fetchParams(Map<String, String> params, String[] paramList, boolean onlyDifferent) {
        return fetchParams(params, paramList == null? null : Arrays.asList(paramList), onlyDifferent);
    }

    /**
     * Adds to params all the entities specified in paramList, unless they are already set.
     *
     * @param params Map of params key:value.
     * @param paramList A list of params to fetch.
     * @param onlyDifferent If true, only fetches params that have changed since the last
     * @return fetched params
     */
    public Map<String, String> fetchParams(Map<String, String> params, List<String> paramList, boolean onlyDifferent) {
        if (params == null) params = new HashMap<>();

        if (paramList != null) {
            for (String paramName : paramList) {
                if (params.get(paramName) != null) continue; // Param already informed

                String value = getParamValue(paramName);

                if (value != null && (!onlyDifferent
                        || !value.equals(getLastSent().get(paramName)))) {
                    params.put(paramName, value);
                    getLastSent().put(paramName, value);
                }
            }
        }

        return params;
    }

    /**
     * Creates an adnumber if it does not exist and stores it in lastSent. If it already exists,
     * it is incremented by 1.
     *
     * @return newly created adNumber
     */
    public String getNewAdNumber() {
        String sAdNumber = getLastSent().get("adNumber");
        if (sAdNumber != null) {
            String position = getLastSent().get("adPosition");
            if (position != null && position.equals(plugin.getAdPosition())) {
                // Increment
                try {
                    int num = Integer.parseInt(sAdNumber);
                    sAdNumber = Integer.toString(num + 1);
                } catch (Exception e) {
                    YouboraLog.error(e); // Should never happen
                }
            } else {
                sAdNumber = null;
            }
        }

        if (sAdNumber == null) sAdNumber = "1";

        getLastSent().put("adNumber", sAdNumber);

        return sAdNumber;
    }

    /**
     * Return changed entities since last check
     *
     * @return params
     */
    public Map<String, String> getChangedEntities() {
        return fetchParams(null, pingEntities, true);
    }

    /**
     * Get the actual value for any param asking the Plugin for it.
     * @param param the param name to fetch
     * @return the param value, or null if not available
     */

    private String getParamValue(String param) {
        String value = null;
        switch (param) {
            case "playhead":
                value = plugin.getPlayhead().toString();
                break;
            case "playrate":
                value = plugin.getPlayrate().toString();
                break;
            case "fps": {
                Double fps = plugin.getFramesPerSecond();
                if (fps != null) value = fps.toString();
            }   break;
            case "droppedFrames":
                value = plugin.getDroppedFrames().toString();
                break;
            case "mediaDuration":
                value = plugin.getDuration().toString();
                break;
            case "bitrate":
                value = plugin.getBitrate().toString();
                break;
            case "throughput":
                value = plugin.getThroughput().toString();
                break;
            case "rendition":
                value = plugin.getRendition();
                break;
            case "title":
                value = plugin.getTitle();
                break;
            case "title2":
                value = plugin.getProgram();
                break;
            case "streamingProtocol":
                value = plugin.getStreamingProtocol();
                break;
            case "live":
                value = plugin.getIsLive().toString();
                break;
            case "mediaResource":
                value = plugin.getResource();
                break;
            case "transactionCode":
                value = plugin.getTransactionCode();
                break;
            case "properties":
                value = plugin.getContentMetadata();
                break;
            case "playerVersion":
                value = plugin.getPlayerVersion();
                break;
            case "player":
                value = plugin.getPlayerName();
                break;
            case "cdn":
                value = plugin.getCdn();
                break;
            case "pluginVersion":
                value = plugin.getPluginVersion();
                break;
            case "param1":
                value = plugin.getCustomDimension1();
                break;
            case "param2":
                value = plugin.getCustomDimension2();
                break;
            case "param3":
                value = plugin.getCustomDimension3();
                break;
            case "param4":
                value = plugin.getCustomDimension4();
                break;
            case "param5":
                value = plugin.getCustomDimension5();
                break;
            case "param6":
                value = plugin.getCustomDimension6();
                break;
            case "param7":
                value = plugin.getCustomDimension7();
                break;
            case "param8":
                value = plugin.getCustomDimension8();
                break;
            case "param9":
                value = plugin.getCustomDimension9();
                break;
            case "param10":
                value = plugin.getCustomDimension10();
                break;
            case "param11":
                value = plugin.getCustomDimension11();
                break;
            case "param12":
                value = plugin.getCustomDimension12();
                break;
            case "param13":
                value = plugin.getCustomDimension13();
                break;
            case "param14":
                value = plugin.getCustomDimension14();
                break;
            case "param15":
                value = plugin.getCustomDimension15();
                break;
            case "param16":
                value = plugin.getCustomDimension16();
                break;
            case "param17":
                value = plugin.getCustomDimension17();
                break;
            case "param18":
                value = plugin.getCustomDimension18();
                break;
            case "param19":
                value = plugin.getCustomDimension19();
                break;
            case "param20":
                value = plugin.getCustomDimension20();
                break;
            case "extraparam1":
                value = plugin.getAdCustomDimension1();
                break;
            case "extraparam2":
                value = plugin.getAdCustomDimension2();
                break;
            case "extraparam3":
                value = plugin.getAdCustomDimension3();
                break;
            case "extraparam4":
                value = plugin.getAdCustomDimension4();
                break;
            case "extraparam5":
                value = plugin.getAdCustomDimension5();
                break;
            case "extraparam6":
                value = plugin.getAdCustomDimension6();
                break;
            case "extraparam7":
                value = plugin.getAdCustomDimension7();
                break;
            case "extraparam8":
                value = plugin.getAdCustomDimension8();
                break;
            case "extraparam9":
                value = plugin.getAdCustomDimension9();
                break;
            case "extraparam10":
                value = plugin.getAdCustomDimension10();
                break;
            case "adPosition":
                value = plugin.getAdPosition();
                break;
            case "adPlayhead": {
                Double ph = plugin.getAdPlayhead();
                if (ph != null) value = ph.toString();
            }   break;
            case "adDuration": {
                Double ph = plugin.getAdDuration();
                if (ph != null) value = ph.toString();
            }   break;
            case "adBitrate": {
                Long br = plugin.getAdBitrate();
                if (br != null) value = br.toString();
            }   break;
            case "adTitle":
                value = plugin.getAdTitle();
                break;
            case "adResource":
                value = plugin.getAdResource();
                break;
            case "adPlayerVersion":
                value = plugin.getAdPlayerVersion();
                break;
            case "adProperties":
                value = plugin.getAdMetadata();
                break;
            case "adAdapterVersion":
                value = plugin.getAdAdapterVersion();
                break;
            case "adCampaign":
                value = plugin.getAdCampaign();
                break;
            case "pluginInfo":
                value = plugin.getPluginInfo();
                break;
            case "isp":
                value = plugin.getIsp();
                break;
            case "connectionType":
                value = plugin.getConnectionType();
                break;
            case "ip":
                value = plugin.getIp();
                break;
            case "obfuscateIp":
                value = plugin.getObfuscateIp();
                break;
            case "deviceCode":
                value = plugin.getDeviceCode();
                break;
            case "system":
                value = plugin.getAccountCode();
                break;
            case "accountCode":
                value = plugin.getAccountCode();
                break;
            case "username":
                value = plugin.getUsername();
                break;
            case "anonymousUser":
                value = plugin.getAnonymousUser();
                break;
            case "userType":
                value = plugin.getUserType();
                break;
            case "preloadDuration": {
                long duration = plugin.getPreloadDuration();
                value = Long.toString(duration);
            }   break;
            case "joinDuration": {
                long duration = plugin.getJoinDuration();
                value = Long.toString(duration);
            }   break;
            case "bufferDuration": {
                long duration = plugin.getBufferDuration();
                value = Long.toString(duration);
            }   break;
            case "seekDuration": {
                long duration = plugin.getSeekDuration();
                value = Long.toString(duration);
            }   break;
            case "pauseDuration": {
                long duration = plugin.getPauseDuration();
                value = Long.toString(duration);
            }   break;
            case "adJoinDuration": {
                long duration = plugin.getAdJoinDuration();
                value = Long.toString(duration);
            }   break;
            case "adBufferDuration": {
                long duration = plugin.getAdBufferDuration();
                value = Long.toString(duration);
            }   break;
            case "adPauseDuration": {
                long duration = plugin.getAdPauseDuration();
                value = Long.toString(duration);
            }   break;
            case "adTotalDuration": {
                long duration = plugin.getAdTotalDuration();
                value = Long.toString(duration);
            }   break;
            case "nodeHost":
                value = plugin.getNodeHost();
                break;
            case "nodeType":
                value = plugin.getNodeType();
                break;
            case "nodeTypeString":
                value = plugin.getNodeTypeString();
                break;
            case "deviceInfo":{
                value = plugin.getDeviceInfoString();
                break;
            }
            case "experiments":{
                ArrayList<String> experimentsArray = plugin.getExperimentIds();
                if (experimentsArray == null ||
                        (experimentsArray != null && experimentsArray.size() == 0)) {
                    value = null;
                } else {
                    StringBuilder builder = new StringBuilder("[");
                    for (String id : experimentsArray)
                        builder.append(String.format("\"%s\",",id));

                    builder = builder.deleteCharAt(builder.length() - 1);
                    builder = builder.append("]");
                    value = builder.toString();
                }
                break;
            }
            case "latency":
                value = Double.toString(plugin.getLatency());
                break;
            case "packetLoss":
                value = Integer.toString(plugin.getPacketLoss());
                break;
            case "packetSent":
                value = Integer.toString(plugin.getPacketSent());
                break;
            case "householdId":
                value = plugin.getHouseholdId();
                break;
            case "navContext":
                value = Infinity.getInstance().getNavContext();
                break;
            case "language":
                value = plugin.getLanguage();
                break;
            case "sessions":
                value = YouboraUtil.stringifyList(plugin.getActiveSessions());
                break;
            case "isInfinity":
                value = plugin.getIsInfinity().toString();
                break;
            case "p2pDownloadedTraffic":
                value = plugin.getP2PTraffic().toString();
                break;
            case "cdnDownloadedTraffic":
                value = plugin.getCdnTraffic().toString();
                break;
            case "uploadTraffic":
                value = plugin.getUploadTraffic().toString();
                break;
            case "smartswitchConfigCode":
                value = plugin.getSmartSwitchConfigCode();
                break;
            case "smartswitchGroupCode":
                value = plugin.getSmartSwitchGroupCode();
                break;
            case "smartswitchContractCode":
                value = plugin.getSmartSwitchContractCode();
                break;
            case "appName":
                value = plugin.getAppName();
                break;
            case "appReleaseVersion":
                value = plugin.getAppReleaseVersion();
                break;
            case "fingerprint":
                value = plugin.getFingerprint();
                break;
            default:
                value = null;
        }
        return value;
    }

    /**
     * Map of last values sent in any request. This is used to find changes in those values
     * and report that some of them have changed. This map can be seen as the server state.
     * @return a Map with last values sent for each param
     */
    public Map<String, String> getLastSent() {
        return lastSent;
    }
}