package com.npaw.youbora.lib6.persistence;

import android.provider.BaseColumns;

/**
 * Created by Enrique on 25/01/2018.
 */

public final class OfflineContract {

    private OfflineContract() {}

    /* Inner class that defines the table contents */
    public static class OfflineEntry implements BaseColumns {

        private OfflineEntry() {}

        public static final String TABLE_NAME = "Event";
        public static final String COLUMN_NAME_UID = "uid";
        public static final String COLUMN_NAME_JSON_EVENTS = "json_events";
        public static final String COLUMN_NAME_DATE_UPDATE = "date_update";
        public static final String COLUMN_NAME_OFFLINE_ID = "offline_id";
    }
}
