package com.npaw.youbora.lib6.comm.transform;

import android.os.Handler;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser;
import com.npaw.youbora.lib6.comm.transform.resourceparse.HlsParser;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnTypeParser;
import com.npaw.youbora.lib6.plugin.Plugin;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import static com.npaw.youbora.lib6.Constants.SERVICE_START;

/**
 * Parses resource urls to get transportstreams and CDN-related info.
 * @author      Nice People at Work
 * @since       6.0
 */
public class ResourceTransform extends Transform {

    private Plugin plugin;
    private boolean hlsEnabled;
    private boolean cdnEnabled;
    private Queue<String> cdnList;
    private String cdnNameHeader;

    private String realResource;
    private String initResource;
    private String cdnName;
    private String cdnNodeHost;
    private String cdnNodeTypeString;
    private CdnTypeParser.Type cdnNodeType;

    private Runnable timeoutRunnable;
    private Handler timeoutHandler;

    private boolean isDone;

    /**
     * Constructor
     * @param plugin the plugin this ResourceTransform will use to get the info it needs
     */
    public ResourceTransform(Plugin plugin) {
        super();

        this.plugin = plugin;

        realResource = null;
        initResource = null;
        cdnName = null;
        cdnNodeHost = null;
        cdnNodeTypeString = null;
        cdnNodeType = null;

        isBusy = false;

        isDone = false;
    }

    /**
     * Get the resource. If the transform is done, the real (parsed) resource will be returned
     * Otherwise the initial one is returned.
     * @return the initial or parsed resource
     */
    public String getResource() {
        if (plugin != null && plugin.getOptions() != null
                && plugin.getOptions().getContentResource() != null
                && !plugin.getOptions().isParseHls())
            return plugin.getOptions().getContentResource();
        else if (realResource != null)
            return realResource;
        else
            return initResource;
    }

    /**
     * Get CDN name
     * @return the CDN name or null if unknown
     */
    public String getCdnName() {
        return cdnName;
    }

    /**
     * Get CDN node
     * @return the CDN node or null if unknown
     */
    public String getNodeHost() {
        return cdnNodeHost;
    }

    /**
     * Get CDN type string, as returned in the cdn header response
     * @return the CDN type string
     */
    public String getNodeTypeString() {
        return cdnNodeTypeString;
    }

    /**
     * Get CDN type, parsed from the type string
     * @return the CDN type
     */
    public String getNodeType() {
        if (cdnNodeType != null) {
            return Integer.toString(cdnNodeType.getValue());
        } else {
            return null;
        }
    }

    /**
     * Start the execution. Can be called more than once. If already running, it will be ignored,
     * if ended it will restart.
     * @param resource the original resource
     */
    public void init(String resource) {
        if (!isBusy && !isDone) {
            isBusy = true;

            hlsEnabled = plugin.isParseHls();
            cdnEnabled = plugin.isParseCdnNode();
            cdnList = new LinkedList<>(plugin.getParseCdnNodeList());
            cdnNameHeader = plugin.getParseCdnNodeNameHeader();
            if (cdnNameHeader != null) {
                CdnParser.setBalancerHeaderName(cdnNameHeader);
            }

            initResource = resource;

            setTimeout();

            if (hlsEnabled) {
                parseHls();
            } else if (cdnEnabled) {
                parseCdn();
            } else {
                done();
            }
        }
    }

    @Override
    protected void done() {
        isDone = true;
        super.done();
    }

    private void setTimeout() {
        // Abort operation after 3 seconds
        if (timeoutHandler == null) {
            timeoutHandler = createHandler();
        }

        if (timeoutRunnable == null) {
            timeoutRunnable = new Runnable() {
                @Override
                public void run() {
                    if (isBusy) {
                        done();
                        YouboraLog.warn("ResourceTransform has exceeded the maximum execution time (3s) and will be aborted.");
                    }
                }
            };
        }

        timeoutHandler.postDelayed(timeoutRunnable, (long) (3 * 1000));
    }

    Handler createHandler() {
        return new Handler();
    }

    private void parseHls() {

        HlsParser hlsParser = createHlsParser();

        hlsParser.addHlsTransformListener(new HlsParser.HlsTransformListener() {
            @Override
            public void onHlsTransformDone(String parsedResource) {
                realResource = parsedResource;
                if (cdnEnabled) {
                    parseCdn();
                } else {
                    done();
                }
            }
        });

        hlsParser.parse(initResource, null);
    }

    HlsParser createHlsParser() {
        return new HlsParser();
    }

    private void parseCdn() {

        if (!cdnList.isEmpty()) {
            String cdn = cdnList.remove();

            if (getNodeHost() != null) {
                done();
            }

            CdnParser cdnParser = createCdnParser(cdn);

            if (cdnParser == null) {
                parseCdn();
            } else {
                cdnParser.addCdnTransformListener(new CdnParser.CdnTransformListener() {
                    @Override
                    public void onCdnTransformDone(CdnParser cdnParser) {
                        cdnName = cdnParser.getCdnName();
                        cdnNodeHost = cdnParser.getNodeHost();
                        cdnNodeTypeString = cdnParser.getNodeTypeString();
                        cdnNodeType = cdnParser.getNodeType();

                        if (getNodeHost() != null) {
                            done();
                        } else {
                            parseCdn();
                        }
                    }
                });

                cdnParser.parse(getResource(), null);
            }

        } else {
            done();
        }
    }

    CdnParser createCdnParser(String cdn) {
        return CdnParser.create(cdn);
    }

    /**
     * {@inheritDoc}
     *
     * Replaces the resource and / or Cdn info for the /start service.
     */
    @Override
    public void parse(Request request) {
        if (SERVICE_START.equals(request.getService())) {

            Map<String, String> lastSent = plugin.getRequestBuilder().getLastSent();

            String resource = getResource();
            request.setParam("mediaResource", resource);
            lastSent.put("mediaResource", resource);


            if (cdnEnabled) {
                String cdn = (String) request.getParam("cdn");
                if (cdn == null) {
                    cdn = getCdnName();
                    request.setParam("cdn", cdn);
                }
                lastSent.put("cdn", cdn);

                request.setParam("nodeHost", getNodeHost());
                lastSent.put("nodeHost", getNodeHost());

                request.setParam("nodeType", getNodeType());
                lastSent.put("nodeType", getNodeType());

                request.setParam("nodeTypeString", getNodeTypeString());
                lastSent.put("nodeTypeString", getNodeTypeString());
            }
        }
    }
}
