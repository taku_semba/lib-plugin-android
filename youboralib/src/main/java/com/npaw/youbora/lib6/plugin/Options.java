package com.npaw.youbora.lib6.plugin;

import android.content.Intent;
import android.os.Bundle;

import com.npaw.youbora.lib6.YouboraLog;

import java.util.ArrayList;

import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_AKAMAI;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_CLOUDFRONT;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_FASTLY;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_HIGHWINDS;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_LEVEL3;
import static com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser.CDN_NAME_TELEFONICA;

/**
 * This class stores all the Youbora configuration settings.
 * Any value specified in this class, if set, will override the info the plugin is able to get on
 * its own.
 *
 * The only <b>required</b> option is the {@link #accountCode}.
 * @author      Nice People at Work
 * @since       6.0
 */
public class Options {

    /**
     * If enabled the plugin won't send NQS requests.
     * Default: true
     */
    private boolean enabled;

    /**
     * Define the security of NQS calls.
     * If true it will use "https://".
     * If false it will use "http://".
     * Default: true
     */
    private boolean httpSecure;

    /**
     * Host of the Fastdata service.
     */
    private String host;

    /**
     * NicePeopleAtWork account code that indicates the customer account.
     */
    private String accountCode;

    /**
     * User ID value inside your system.
     */
    private String username;

    /**
     * Anonymous user ID value inside your system.
     */
    private String anonymousUser;

    /**
     * If true the plugin will store the events and send them later when there's connection.
     */
    private boolean offline;

    /**
     * If true the plugin will use infinity.
     */
    private Boolean isInfinity;

    /**
     * If true the plugin will parse HLS files to use the first .ts file found as resource.
     * It might slow performance down.
     * Default: false
     */
    private boolean parseHls;

    /**
     * If defined, resource parse will try to fetch the CDN code from the custom header defined
     * by this property, e.g. "x-cdn-forward"
     */
    private String parseCdnNameHeader;

    /**
     * If true the plugin will query the CDN to retrieve the node name.
     * It might slow performance down.
     * Default: false
     */
    private boolean parseCdnNode;

    /**
     * List of CDN names to parse. This is only used when {@link #parseCdnNode} is enabled.
     * Order is respected when trying to match against a CDN.
     * Default: ["Akamai", "Cloudfront", "Level3", "Fastly", "Highwinds"].
     */
    private ArrayList<String> parseCdnNodeList;

    /**
     * List of experiment ids to use with SmartUsers
     */
    private ArrayList<String> experimentIds;

    /**
     * SmartSwitch config code
     */
    private String smartswitchConfigCode;

    /**
     * SmartSwitch group code
     */
    private String smartswitchGroupCode;

    /**
     * SmartSwitch contract code
     */
    private String smartswitchContractCode;

    /**
     * IP of the viewer/user, e.g. "48.15.16.23".
     */
    private String networkIP;

    /**
     * Name of the internet service provider of the viewer/user.
     */
    private String networkIsp;

    /**
     * See a list of codes in <a href="http://mapi.youbora.com:8081/connectionTypes">http://mapi.youbora.com:8081/connectionTypes</a>.
     */
    private String networkConnectionType;

    /**
     * Option to obfuscate the IP
     */
    private boolean obfuscateIp;

    /**
     * Youbora's device code. If specified it will rewrite info gotten from user agent.
     * See a list of codes in <a href="http://mapi.youbora.com:8081/devices">http://mapi.youbora.com:8081/devices</a>.
     */
    private String deviceCode;

    /**
     * What will be displayed as the device model on Youbora (provided by default with android.os.Build.MODEL if not set)
     */
    private String deviceModel;

    /**
     * What will be displayed as the device brand on Youbora (provided by default with android.os.Build.BRAND if not set)
     */
    private String deviceBrand;

    /**
     * What will be displayed as the device type on Youbora (pc, smartphone, stb, tv, etc.)
     */
    private String deviceType;

    /**
     * What will be displayed as the device name on Youbora (pc, smartphone, stb, tv, etc.)
     */
    private String deviceName;

    /**
     * OS name that will be displayed on Youbora
     */
    private String deviceOsName;

    /**
     * OS version that will be displayed on Youbora (provided by default with android.os.Build.VERSION.RELEASE if not set)
     */
    private String deviceOsVersion;

    /**
     * Flag to send the start by the adapter or not
     */
    private boolean autoStart;

    /**
     * Force init enabled.
     */
    private boolean forceInit;

    /**
     * URL/path of the current media resource.
     */
    private String contentResource;

    /**
     * true if the content is Live. false if VOD.
     */
    private Boolean contentIsLive;

    /**
     * Title of the media.
     */
    private String contentTitle;

    /**
     * Secondary title of the media. This could be program name, season, episode, etc.
     */
    private String program;

    /**
     * Duration of the media <b>in seconds</b>.
     */
    private Double contentDuration;

    /**
     * Custom unique code to identify the view.
     */
    private String contentTransactionCode;

    /**
     * Bitrate of the content in bits per second.
     */
    private Long contentBitrate;

    /**
     * Throughput of the client bandwidth in bits per second.
     */
    private Long contentThroughput;

    /**
     * Name or value of the current rendition (quality) of the content.
     */
    private String contentRendition;

    /**
     * Codename of the CDN where the content is streaming from.
     * See a list of codes in <a href="http://mapi.youbora.com:8081/cdns">http://mapi.youbora.com:8081/cdns</a>.
     */
    private String contentCdn;

    /**
     * Frames per second of the media being played.
     */
    private Double contentFps;

    /**
     * Disable seeks if content is live. Only applies if content is live, for VOD gets ignored
     */
    private Boolean contentIsLiveNoSeek;

    /**
     * Resource streaming protocol
     * Accepted values for this option are: HDS, HLS, MSS, DASH, RTMP, RTP, RTSP
     */
    private String contentResourceProtocol;

    /**
     * User type (e.g. Premium, Free, Unregistered, Registered
     */
    private String userType;

    /**
     * {@link Bundle} containing mixed extra information about the content like: director, parental rating,
     * device info or the audio channels.
     */
    private transient Bundle contentMetadata;

    /**
     * {@link Bundle} containing mixed extra information about the ads like: director, parental rating,
     * device info or the audio channels.
     */
    private transient Bundle adMetadata;

    /**
     * Ad title
     */
    private String adTitle;

    /**
     * Ad campaign
     */
    private String adCampaign;

    /**
     * Ad resource
     */
    private String adResource;

    /**
     *  If true, youbora blocks ad events and calculates jointime ignoring ad time.
     */
    private boolean ignoreAds;

    /**
     * Set to integer positive value indicating how many ads
     * will be shown as post-rolls if they do it after content player triggers stop event.
     */
    private int adsAfterStop;

    /**
     *  Stop the plugin automatically when the user goes to background
     */
    private boolean autoDetectBackground;

    /**
     * Custom dimension 1.
     */
    private String customDimension1;

    /**
     * Custom dimension 2.
     */
    private String customDimension2;

    /**
     * Custom dimension 3.
     */
    private String customDimension3;

    /**
     * Custom dimension 4.
     */
    private String customDimension4;

    /**
     * Custom dimension 5.
     */
    private String customDimension5;

    /**
     * Custom dimension 6.
     */
    private String customDimension6;

    /**
     * Custom dimension 7.
     */
    private String customDimension7;

    /**
     * Custom dimension 8.
     */
    private String customDimension8;

    /**
     * Custom dimension 9.
     */
    private String customDimension9;

    /**
     * Custom dimension 10.
     */
    private String customDimension10;

    /**
     * Custom dimension 11.
     */
    private String customDimension11;

    /**
     * Custom dimension 12.
     */
    private String customDimension12;

    /**
     * Custom dimension 13.
     */
    private String customDimension13;

    /**
     * Custom dimension 14.
     */
    private String customDimension14;

    /**
     * Custom dimension 15.
     */
    private String customDimension15;

    /**
     * Custom dimension 16.
     */
    private String customDimension16;

    /**
     * Custom dimension 17.
     */
    private String customDimension17;

    /**
     * Custom dimension 18.
     */
    private String customDimension18;

    /**
     * Custom dimension 19.
     */
    private String customDimension19;

    /**
     * Custom dimension 20.
     */
    private String customDimension20;

    /**
     * Custom ad custom dimension 1.
     */
    private String adCustomDimension1;

    /**
     * Custom ad custom dimension 2.
     */
    private String adCustomDimension2;

    /**
     * Custom ad custom dimension 3.
     */
    private String adCustomDimension3;

    /**
     * Custom ad custom dimension 4.
     */
    private String adCustomDimension4;

    /**
     * Custom ad custom dimension 5.
     */
    private String adCustomDimension5;

    /**
     * Custom ad custom dimension 6.
     */
    private String adCustomDimension6;

    /**
     * Custom ad custom dimension 7.
     */
    private String adCustomDimension7;

    /**
     * Custom ad custom dimension 8.
     */
    private String adCustomDimension8;

    /**
     * Custom ad custom dimension 9.
     */
    private String adCustomDimension9;

    /**
     * Custom ad custom dimension 10.
     */
    private String adCustomDimension10;

    /**
     * Name of the app
     */
    private String appName;

    /**
     * Version of the app
     */
    private String appReleaseVersion;

    /**
     * Delay start to wait for metadata
     */
    private boolean waitForMetadata;

    /**
     * Set option keys you want to wait for metadata, in order to work {@link #waitForMetadata}
     * must be set to true
     */
    private ArrayList<String> pendingMetadata;

    // Keys for Bundle
    public static final String KEY_ENABLED = "enabled";
    public static final String KEY_HTTP_SECURE = "httpSecure";
    public static final String KEY_HOST = "host";
    public static final String KEY_ACCOUNT_CODE = "config.accountCode";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_ANONYMOUS_USER = "anonymousUser";
    public static final String KEY_OFFLINE = "offline";
    public static final String KEY_IS_INFINITY = "isInfinity";
    public static final String KEY_BACKGROUND = "autoDetectBackground";
    public static final String KEY_AUTOSTART = "autoStart";
    public static final String KEY_FORCEINIT = "forceInit";
    public static final String KEY_USER_TYPE = "userType";
    public static final String KEY_EXPERIMENT_IDS = "experiments";
    public static final String KEY_SS_CONFIG_CODE = "smartswitch.configCode";
    public static final String KEY_SS_GROUP_CODE = "smartswitch.groupCode";
    public static final String KEY_SS_CONTRACT_CODE = "smartswitch.contractCode";

    public static final String KEY_PARSE_HLS = "parse.Hls";
    public static final String KEY_PARSE_CDN_NAME_HEADER = "parse.CdnNameHeader";
    public static final String KEY_PARSE_CDN_NODE = "parse.CdnNode";
    public static final String KEY_PARSE_CDN_NODE_LIST = "parse.CdnNodeList";

    public static final String KEY_NETWORK_IP = "network.IP";
    public static final String KEY_NETWORK_ISP = "network.Isp";
    public static final String KEY_NETWORK_CONNECTION_TYPE = "network.connectionType";
    public static final String KEY_NETWORK_OBFUSCATE_IP = "network.obfuscateIp";

    public static final String KEY_DEVICE_CODE = "device.code";
    public static final String KEY_DEVICE_MODEL = "device.model";
    public static final String KEY_DEVICE_BRAND = "device.brand";
    public static final String KEY_DEVICE_TYPE = "device.type";
    public static final String KEY_DEVICE_NAME = "device.name";
    public static final String KEY_DEVICE_OS_NAME = "device.osNme";
    public static final String KEY_DEVICE_OS_VERSION = "device.osVersion";

    public static final String KEY_CONTENT_RESOURCE = "content.resource";
    public static final String KEY_CONTENT_IS_LIVE = "content.isLive";
    public static final String KEY_CONTENT_TITLE = "content.title";
    public static final String KEY_CONTENT_PROGRAM = "content.program";
    public static final String KEY_CONTENT_DURATION = "content.duration";
    public static final String KEY_CONTENT_TRANSACTION_CODE = "content.transactionCode";
    public static final String KEY_CONTENT_BITRATE = "content.bitrate";
    public static final String KEY_CONTENT_THROUGHPUT = "content.throughput";
    public static final String KEY_CONTENT_RENDITION = "content.rendition";
    public static final String KEY_CONTENT_CDN = "content.cdn";
    public static final String KEY_CONTENT_FPS = "content.fps";
    public static final String KEY_CONTENT_STREAMING_PROTOCOL = "content.streamingProtocol";
    public static final String KEY_CONTENT_METADATA = "content.metadata";
    public static final String KEY_CONTENT_METRICS = "content.metrics";
    public static final String KEY_CONTENT_IS_LIVE_NO_SEEK = "content.isLiveNoSeek";

    public static final String KEY_SESSION_METRICS = "session.metrics";

    public static final String KEY_AD_METADATA = "ad.metadata";
    public static final String KEY_AD_IGNORE = "ad.ignore";
    public static final String KEY_ADS_AFTERSTOP = "ad.afterStop";
    public static final String KEY_AD_CAMPAIGN = "ad.campaign";
    public static final String KEY_AD_TITLE = "ad.title";
    public static final String KEY_AD_RESOURCE = "ad.resource";

    public static final String KEY_CUSTOM_DIMENSION_1 = "custom.dimension.1";
    public static final String KEY_CUSTOM_DIMENSION_2 = "custom.dimension.2";
    public static final String KEY_CUSTOM_DIMENSION_3 = "custom.dimension.3";
    public static final String KEY_CUSTOM_DIMENSION_4 = "custom.dimension.4";
    public static final String KEY_CUSTOM_DIMENSION_5 = "custom.dimension.5";
    public static final String KEY_CUSTOM_DIMENSION_6 = "custom.dimension.6";
    public static final String KEY_CUSTOM_DIMENSION_7 = "custom.dimension.7";
    public static final String KEY_CUSTOM_DIMENSION_8 = "custom.dimension.8";
    public static final String KEY_CUSTOM_DIMENSION_9 = "custom.dimension.9";
    public static final String KEY_CUSTOM_DIMENSION_10 = "custom.dimension.10";
    public static final String KEY_CUSTOM_DIMENSION_11 = "custom.dimension.11";
    public static final String KEY_CUSTOM_DIMENSION_12 = "custom.dimension.12";
    public static final String KEY_CUSTOM_DIMENSION_13 = "custom.dimension.13";
    public static final String KEY_CUSTOM_DIMENSION_14 = "custom.dimension.14";
    public static final String KEY_CUSTOM_DIMENSION_15 = "custom.dimension.15";
    public static final String KEY_CUSTOM_DIMENSION_16 = "custom.dimension.16";
    public static final String KEY_CUSTOM_DIMENSION_17 = "custom.dimension.17";
    public static final String KEY_CUSTOM_DIMENSION_18 = "custom.dimension.18";
    public static final String KEY_CUSTOM_DIMENSION_19 = "custom.dimension.19";
    public static final String KEY_CUSTOM_DIMENSION_20 = "custom.dimension.20";

    public static final String KEY_AD_CUSTOM_DIMENSION_1 = "ad.custom.dimension.1";
    public static final String KEY_AD_CUSTOM_DIMENSION_2 = "ad.custom.dimension.2";
    public static final String KEY_AD_CUSTOM_DIMENSION_3 = "ad.custom.dimension.3";
    public static final String KEY_AD_CUSTOM_DIMENSION_4 = "ad.custom.dimension.4";
    public static final String KEY_AD_CUSTOM_DIMENSION_5 = "ad.custom.dimension.5";
    public static final String KEY_AD_CUSTOM_DIMENSION_6 = "ad.custom.dimension.6";
    public static final String KEY_AD_CUSTOM_DIMENSION_7 = "ad.custom.dimension.7";
    public static final String KEY_AD_CUSTOM_DIMENSION_8 = "ad.custom.dimension.8";
    public static final String KEY_AD_CUSTOM_DIMENSION_9 = "ad.custom.dimension.9";
    public static final String KEY_AD_CUSTOM_DIMENSION_10 = "ad.custom.dimension.10";

    public static final String KEY_APP_NAME = "app.name";
    public static final String KEY_APP_RELEASE_VERSION = "app.release.version";

    public static final String KEY_WAIT_METADATA = "waitForMetadata";
    public static final String KEY_PENDING_METADATA = "pendingMetadata";

    /**
     * Constructor.
     * Sets the default field values.
     */
    public Options() {
        setEnabled(true);
        setHttpSecure(true);
        setHost("nqs.nice264.com");
        setAccountCode("nicetest");
        setUsername(null);
        setOffline(false);
        setAutoDetectBackground(true);
        setAutoStart(true);
        setUserType(null);
        setExperimentIds(new ArrayList<String>());
        setSmartSwitchConfigCode(null);
        setSmartSwitchGroupCode(null);
        setSmartSwitchContractCode(null);

        setParseHls(false);
        setParseCdnNameHeader("x-cdn-forward");
        setParseCdnNode(false);
        setParseCdnNodeList(new ArrayList<String>(5){{
            add(CDN_NAME_AKAMAI);
            add(CDN_NAME_CLOUDFRONT);
            add(CDN_NAME_LEVEL3);
            add(CDN_NAME_FASTLY);
            add(CDN_NAME_HIGHWINDS);
            add(CDN_NAME_TELEFONICA);
        }});

        setNetworkIP(null);
        setNetworkIsp(null);
        setNetworkConnectionType(null);
        setNetworkObfuscateIp(false);

        setDeviceCode(null);
        setDeviceModel(null);
        setDeviceBrand(null);
        setDeviceType(null);
        setDeviceOsName(null);
        setDeviceOsVersion(null);

        setContentResource(null);
        setContentIsLive(null);
        setContentTitle(null);
        setProgram(null);
        setContentDuration(null);
        setContentTransactionCode(null);
        setContentBitrate(null);
        setContentThroughput(null);
        setContentRendition(null);
        setContentCdn(null);
        setContentFps(null);
        setContentStreamingProtocol(null);
        setContentMetadata(new Bundle());
        setContentIsLiveNoSeek(null);

        setAdMetadata(new Bundle());
        setAdIgnore(false);
        setAdsAfterStop(0);
        setAdCampaign(null);
        setAdResource(null);
        setAdTitle(null);

        setCustomDimension1(null);
        setCustomDimension2(null);
        setCustomDimension3(null);
        setCustomDimension4(null);
        setCustomDimension5(null);
        setCustomDimension6(null);
        setCustomDimension7(null);
        setCustomDimension8(null);
        setCustomDimension9(null);
        setCustomDimension10(null);
        setCustomDimension11(null);
        setCustomDimension12(null);
        setCustomDimension13(null);
        setCustomDimension14(null);
        setCustomDimension15(null);
        setCustomDimension16(null);
        setCustomDimension17(null);
        setCustomDimension18(null);
        setCustomDimension19(null);
        setCustomDimension20(null);

        setAdCustomDimension1(null);
        setAdCustomDimension2(null);
        setAdCustomDimension3(null);
        setAdCustomDimension4(null);
        setAdCustomDimension5(null);
        setAdCustomDimension6(null);
        setAdCustomDimension7(null);
        setAdCustomDimension8(null);
        setAdCustomDimension9(null);
        setAdCustomDimension10(null);

        setAppName(null);
        setAppReleaseVersion(null);

        setWaitForMetadata(false);
        setPendingMetadata(new ArrayList<String>());

    }

    /**
     * Constructor.
     * It will populate the fields by reading the values form a {@link Bundle}
     * @param b {@link Bundle} where to read the values from
     */
    public Options(Bundle b) {
        this();

        if (b != null) {
            if (b.containsKey(KEY_ENABLED)) setEnabled(b.getBoolean(KEY_ENABLED));
            if (b.containsKey(KEY_HTTP_SECURE)) setHttpSecure(b.getBoolean(KEY_HTTP_SECURE));
            if (b.containsKey(KEY_HOST)) setHost(b.getString(KEY_HOST));
            if (b.containsKey(KEY_ACCOUNT_CODE)) setAccountCode(b.getString(KEY_ACCOUNT_CODE));
            if (b.containsKey(KEY_USERNAME)) setUsername(b.getString(KEY_USERNAME));
            if (b.containsKey(KEY_ANONYMOUS_USER)) setAnonymousUser(b.getString(KEY_ANONYMOUS_USER));
            if (b.containsKey(KEY_OFFLINE)) setOffline(b.getBoolean(KEY_OFFLINE));
            if (b.containsKey(KEY_IS_INFINITY)) setIsInfinity(b.getBoolean(KEY_IS_INFINITY));
            if (b.containsKey(KEY_BACKGROUND)) setAutoDetectBackground(b.getBoolean(KEY_BACKGROUND));
            if (b.containsKey(KEY_AUTOSTART)) setAutoStart(b.getBoolean(KEY_AUTOSTART));
            if (b.containsKey(KEY_FORCEINIT)) setForceInit(b.getBoolean(KEY_FORCEINIT));
            if (b.containsKey(KEY_USER_TYPE)) setUserType(b.getString(KEY_USER_TYPE));
            if (b.containsKey(KEY_EXPERIMENT_IDS)) setExperimentIds(b.getStringArrayList(KEY_EXPERIMENT_IDS));
            if (b.containsKey(KEY_SS_CONFIG_CODE)) setSmartSwitchConfigCode(b.getString(KEY_SS_CONFIG_CODE));
            if (b.containsKey(KEY_SS_GROUP_CODE)) setSmartSwitchConfigCode(b.getString(KEY_SS_GROUP_CODE));
            if (b.containsKey(KEY_SS_CONTRACT_CODE)) setSmartSwitchConfigCode(b.getString(KEY_SS_CONTRACT_CODE));

            if (b.containsKey(KEY_PARSE_HLS)) setParseHls(b.getBoolean(KEY_PARSE_HLS));
            if (b.containsKey(KEY_PARSE_CDN_NAME_HEADER)) setParseCdnNameHeader(b.getString(KEY_PARSE_CDN_NAME_HEADER));
            if (b.containsKey(KEY_PARSE_CDN_NODE)) setParseCdnNode(b.getBoolean(KEY_PARSE_CDN_NODE));
            if (b.containsKey(KEY_PARSE_CDN_NODE_LIST)) setParseCdnNodeList(b.getStringArrayList(KEY_PARSE_CDN_NODE_LIST));

            if (b.containsKey(KEY_NETWORK_IP)) setNetworkIP(b.getString(KEY_NETWORK_IP));
            if (b.containsKey(KEY_NETWORK_ISP)) setNetworkIsp(b.getString(KEY_NETWORK_ISP));
            if (b.containsKey(KEY_NETWORK_CONNECTION_TYPE)) setNetworkConnectionType(b.getString(KEY_NETWORK_CONNECTION_TYPE));
            if (b.containsKey(KEY_NETWORK_OBFUSCATE_IP)) setNetworkObfuscateIp(b.getBoolean(KEY_NETWORK_OBFUSCATE_IP));

            if (b.containsKey(KEY_DEVICE_CODE)) setDeviceCode(b.getString(KEY_DEVICE_CODE));
            if (b.containsKey(KEY_DEVICE_MODEL)) setDeviceModel(b.getString(KEY_DEVICE_MODEL));
            if (b.containsKey(KEY_DEVICE_BRAND)) setDeviceBrand(b.getString(KEY_DEVICE_BRAND));
            if (b.containsKey(KEY_DEVICE_TYPE)) setDeviceType(b.getString(KEY_DEVICE_TYPE));
            if (b.containsKey(KEY_DEVICE_OS_NAME)) setDeviceOsName(b.getString(KEY_DEVICE_OS_NAME));
            if (b.containsKey(KEY_DEVICE_OS_VERSION)) setDeviceOsVersion(b.getString(KEY_DEVICE_OS_VERSION));

            if (b.containsKey(KEY_CONTENT_RESOURCE)) setContentResource(b.getString(KEY_CONTENT_RESOURCE));
            if (b.containsKey(KEY_CONTENT_IS_LIVE)) setContentIsLive(b.getBoolean(KEY_CONTENT_IS_LIVE));
            if (b.containsKey(KEY_CONTENT_TITLE)) setContentTitle(b.getString(KEY_CONTENT_TITLE));
            if (b.containsKey(KEY_CONTENT_PROGRAM)) setProgram(b.getString(KEY_CONTENT_PROGRAM));
            if (b.containsKey(KEY_CONTENT_DURATION)) setContentDuration(b.getDouble(KEY_CONTENT_DURATION));
            if (b.containsKey(KEY_CONTENT_TRANSACTION_CODE)) setContentTransactionCode(b.getString(KEY_CONTENT_TRANSACTION_CODE));
            if (b.containsKey(KEY_CONTENT_BITRATE)) setContentBitrate(b.getLong(KEY_CONTENT_BITRATE));
            if (b.containsKey(KEY_CONTENT_THROUGHPUT)) setContentThroughput(b.getLong(KEY_CONTENT_THROUGHPUT));
            if (b.containsKey(KEY_CONTENT_RENDITION)) setContentRendition(b.getString(KEY_CONTENT_RENDITION));
            if (b.containsKey(KEY_CONTENT_CDN)) setContentCdn(b.getString(KEY_CONTENT_CDN));
            if (b.containsKey(KEY_CONTENT_FPS)) setContentFps(b.getDouble(KEY_CONTENT_FPS));
            if (b.containsKey(KEY_CONTENT_STREAMING_PROTOCOL)) setContentStreamingProtocol(b.getString(KEY_CONTENT_STREAMING_PROTOCOL));
            if (b.containsKey(KEY_CONTENT_METADATA)) setContentMetadata(b.getBundle(KEY_CONTENT_METADATA));
            if (b.containsKey(KEY_CONTENT_IS_LIVE_NO_SEEK)) setContentIsLiveNoSeek(b.getBoolean(KEY_CONTENT_IS_LIVE_NO_SEEK));

            if (b.containsKey(KEY_AD_METADATA)) setAdMetadata(b.getBundle(KEY_AD_METADATA));
            if (b.containsKey(KEY_AD_IGNORE)) setAdIgnore(b.getBoolean(KEY_AD_IGNORE));
            if (b.containsKey(KEY_ADS_AFTERSTOP)) setAdsAfterStop(b.getInt(KEY_ADS_AFTERSTOP));
            if (b.containsKey(KEY_AD_CAMPAIGN)) setAdCampaign(b.getString(KEY_AD_CAMPAIGN));
            if (b.containsKey(KEY_AD_RESOURCE)) setAdResource(b.getString(KEY_AD_RESOURCE));
            if (b.containsKey(KEY_AD_TITLE)) setAdTitle(b.getString(KEY_AD_TITLE));

            if (b.containsKey(KEY_CUSTOM_DIMENSION_1)) setCustomDimension1(b.getString(KEY_CUSTOM_DIMENSION_1));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_2)) setCustomDimension2(b.getString(KEY_CUSTOM_DIMENSION_2));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_3)) setCustomDimension3(b.getString(KEY_CUSTOM_DIMENSION_3));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_4)) setCustomDimension4(b.getString(KEY_CUSTOM_DIMENSION_4));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_5)) setCustomDimension5(b.getString(KEY_CUSTOM_DIMENSION_5));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_6)) setCustomDimension6(b.getString(KEY_CUSTOM_DIMENSION_6));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_7)) setCustomDimension7(b.getString(KEY_CUSTOM_DIMENSION_7));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_8)) setCustomDimension8(b.getString(KEY_CUSTOM_DIMENSION_8));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_9)) setCustomDimension9(b.getString(KEY_CUSTOM_DIMENSION_9));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_10)) setCustomDimension10(b.getString(KEY_CUSTOM_DIMENSION_10));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_11)) setCustomDimension11(b.getString(KEY_CUSTOM_DIMENSION_11));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_12)) setCustomDimension12(b.getString(KEY_CUSTOM_DIMENSION_12));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_13)) setCustomDimension13(b.getString(KEY_CUSTOM_DIMENSION_13));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_14)) setCustomDimension14(b.getString(KEY_CUSTOM_DIMENSION_14));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_15)) setCustomDimension15(b.getString(KEY_CUSTOM_DIMENSION_15));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_16)) setCustomDimension16(b.getString(KEY_CUSTOM_DIMENSION_16));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_17)) setCustomDimension17(b.getString(KEY_CUSTOM_DIMENSION_17));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_18)) setCustomDimension18(b.getString(KEY_CUSTOM_DIMENSION_18));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_19)) setCustomDimension19(b.getString(KEY_CUSTOM_DIMENSION_19));
            if (b.containsKey(KEY_CUSTOM_DIMENSION_20)) setCustomDimension20(b.getString(KEY_CUSTOM_DIMENSION_20));

            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_1)) setAdCustomDimension1(b.getString(KEY_AD_CUSTOM_DIMENSION_1));
            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_2)) setAdCustomDimension2(b.getString(KEY_AD_CUSTOM_DIMENSION_2));
            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_3)) setAdCustomDimension3(b.getString(KEY_AD_CUSTOM_DIMENSION_3));
            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_4)) setAdCustomDimension4(b.getString(KEY_AD_CUSTOM_DIMENSION_4));
            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_5)) setAdCustomDimension5(b.getString(KEY_AD_CUSTOM_DIMENSION_5));
            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_6)) setAdCustomDimension6(b.getString(KEY_AD_CUSTOM_DIMENSION_6));
            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_7)) setAdCustomDimension7(b.getString(KEY_AD_CUSTOM_DIMENSION_7));
            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_8)) setAdCustomDimension8(b.getString(KEY_AD_CUSTOM_DIMENSION_8));
            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_9)) setAdCustomDimension9(b.getString(KEY_AD_CUSTOM_DIMENSION_9));
            if (b.containsKey(KEY_AD_CUSTOM_DIMENSION_10)) setAdCustomDimension10(b.getString(KEY_AD_CUSTOM_DIMENSION_10));

            if (b.containsKey(KEY_APP_NAME)) setAppName(b.getString(KEY_APP_NAME));
            if (b.containsKey(KEY_APP_RELEASE_VERSION))
                setAppReleaseVersion(b.getString(KEY_APP_RELEASE_VERSION));

            if (b.containsKey(KEY_WAIT_METADATA)) setWaitForMetadata(b.getBoolean(KEY_WAIT_METADATA));
            if (b.containsKey(KEY_PENDING_METADATA)) setPendingMetadata(b.getStringArrayList(KEY_PENDING_METADATA));
        }
    }

    /**
     * Convert this Options instance into a {@link Bundle} representation.
     *
     * To get the Options object from a bundle use the {@link #Options(Bundle)} constructor.
     * This can be useful to carry the options in an {@link Intent}.
     * @return a {@link Bundle} with the YouboraOptions.
     */
    public Bundle toBundle() {
        Bundle b = new Bundle();

        b.putBoolean(KEY_ENABLED, isEnabled());
        b.putBoolean(KEY_HTTP_SECURE, isHttpSecure());
        if (getHost() != null) b.putString(KEY_HOST, getHost());
        if (getAccountCode() != null) b.putString(KEY_ACCOUNT_CODE, getAccountCode());
        if (getUsername() != null) b.putString(KEY_USERNAME, getUsername());
        if (getAnonymousUser() != null) b.putString(KEY_ANONYMOUS_USER, getAnonymousUser());
        b.putBoolean(KEY_OFFLINE, isOffline());
        if (getIsInfinity() != null) b.putBoolean(KEY_IS_INFINITY, getIsInfinity());
        b.putBoolean(KEY_BACKGROUND, isAutoDetectBackground());
        b.putBoolean(KEY_AUTOSTART, isAutoStart());
        b.putBoolean(KEY_FORCEINIT, isForceInit());
        if(getUserType() != null) b.putString(KEY_USER_TYPE, getUserType());
        if(getExperimentIds() != null) b.putStringArrayList(KEY_EXPERIMENT_IDS, getExperimentIds());
        if(getSmartSwitchConfigCode() != null) b.getString(KEY_SS_CONFIG_CODE, getSmartSwitchConfigCode());
        if(getSmartSwitchGroupCode() != null) b.getString(KEY_SS_GROUP_CODE, getSmartSwitchGroupCode());
        if(getSmartSwitchContractCode() != null) b.getString(KEY_SS_CONTRACT_CODE, getSmartSwitchContractCode());

        b.putBoolean(KEY_PARSE_HLS, isParseHls());
        if (getParseCdnNameHeader() != null) b.putString(KEY_PARSE_CDN_NAME_HEADER, getParseCdnNameHeader());
        b.putBoolean(KEY_PARSE_CDN_NODE, isParseCdnNode());
        if (getParseCdnNodeList() != null) b.putStringArrayList(KEY_PARSE_CDN_NODE_LIST, getParseCdnNodeList());

        if (getNetworkIP() != null) b.putString(KEY_NETWORK_IP, getNetworkIP());
        if (getNetworkIsp() != null) b.putString(KEY_NETWORK_ISP, getNetworkIsp());
        if (getNetworkConnectionType() != null) b.putString(KEY_NETWORK_CONNECTION_TYPE, getNetworkConnectionType());
        b.putBoolean(KEY_NETWORK_OBFUSCATE_IP, getNetworkObfuscateIp());

        if (getDeviceCode() != null) b.putString(KEY_DEVICE_CODE, getDeviceCode());
        if (getDeviceModel() != null) b.putString(KEY_DEVICE_MODEL, getDeviceModel());
        if (getDeviceBrand() != null) b.putString(KEY_DEVICE_BRAND, getDeviceBrand());
        if (getDeviceType() != null) b.putString(KEY_DEVICE_TYPE, getDeviceType());
        if (getDeviceOsName() != null) b.putString(KEY_DEVICE_OS_NAME, getDeviceOsName());
        if (getDeviceOsVersion() != null) b.putString(KEY_DEVICE_OS_VERSION, getDeviceOsVersion());

        if (getContentResource() != null) b.putString(KEY_CONTENT_RESOURCE, getContentResource());
        if (getContentIsLive() != null) b.putBoolean(KEY_CONTENT_IS_LIVE, getContentIsLive());
        if (getContentTitle() != null) b.putString(KEY_CONTENT_TITLE, getContentTitle());
        if (getProgram() != null) b.putString(KEY_CONTENT_PROGRAM, getProgram());
        if (getContentDuration() != null) b.putDouble(KEY_CONTENT_DURATION, getContentDuration());
        if (getContentTransactionCode() != null) b.putString(KEY_CONTENT_TRANSACTION_CODE, getContentTransactionCode());
        if (getContentBitrate() != null) b.putLong(KEY_CONTENT_BITRATE, getContentBitrate());
        if (getContentThroughput() != null) b.putLong(KEY_CONTENT_THROUGHPUT, getContentThroughput());
        if (getContentRendition() != null) b.putString(KEY_CONTENT_RENDITION, getContentRendition());
        if (getContentCdn() != null) b.putString(KEY_CONTENT_CDN, getContentCdn());
        if (getContentFps() != null) b.putDouble(KEY_CONTENT_FPS, getContentFps());
        if (getContentStreamingProtocol() != null) b.putString(KEY_CONTENT_STREAMING_PROTOCOL, getContentStreamingProtocol());
        if (getContentMetadata() != null) b.putBundle(KEY_CONTENT_METADATA, getContentMetadata());
        if (getContentIsLiveNoSeek() != null) b.putBoolean(KEY_CONTENT_IS_LIVE_NO_SEEK, getContentIsLiveNoSeek());

        if (getAdMetadata() != null) b.putBundle(KEY_AD_METADATA, getAdMetadata());
        b.putBoolean(KEY_AD_IGNORE, getAdIgnore());
        b.putInt(KEY_ADS_AFTERSTOP, getAdsAfterStop());
        if(getAdCampaign() != null) b.putString(KEY_AD_CAMPAIGN, getAdCampaign());
        if(getAdTitle() != null) b.putString(KEY_AD_TITLE, getAdTitle());
        if(getAdResource() != null) b.putString(KEY_AD_RESOURCE, getAdResource());

        if (getCustomDimension1() != null) b.putString(KEY_CUSTOM_DIMENSION_1, getCustomDimension1());
        if (getCustomDimension2() != null) b.putString(KEY_CUSTOM_DIMENSION_2, getCustomDimension2());
        if (getCustomDimension3() != null) b.putString(KEY_CUSTOM_DIMENSION_3, getCustomDimension3());
        if (getCustomDimension4() != null) b.putString(KEY_CUSTOM_DIMENSION_4, getCustomDimension4());
        if (getCustomDimension5() != null) b.putString(KEY_CUSTOM_DIMENSION_5, getCustomDimension5());
        if (getCustomDimension6() != null) b.putString(KEY_CUSTOM_DIMENSION_6, getCustomDimension6());
        if (getCustomDimension7() != null) b.putString(KEY_CUSTOM_DIMENSION_7, getCustomDimension7());
        if (getCustomDimension8() != null) b.putString(KEY_CUSTOM_DIMENSION_8, getCustomDimension8());
        if (getCustomDimension9() != null) b.putString(KEY_CUSTOM_DIMENSION_9, getCustomDimension9());
        if (getCustomDimension10() != null) b.putString(KEY_CUSTOM_DIMENSION_10, getCustomDimension10());
        if (getCustomDimension11() != null) b.putString(KEY_CUSTOM_DIMENSION_11, getCustomDimension11());
        if (getCustomDimension12() != null) b.putString(KEY_CUSTOM_DIMENSION_12, getCustomDimension12());
        if (getCustomDimension13() != null) b.putString(KEY_CUSTOM_DIMENSION_13, getCustomDimension13());
        if (getCustomDimension14() != null) b.putString(KEY_CUSTOM_DIMENSION_14, getCustomDimension14());
        if (getCustomDimension15() != null) b.putString(KEY_CUSTOM_DIMENSION_15, getCustomDimension15());
        if (getCustomDimension16() != null) b.putString(KEY_CUSTOM_DIMENSION_16, getCustomDimension16());
        if (getCustomDimension17() != null) b.putString(KEY_CUSTOM_DIMENSION_17, getCustomDimension17());
        if (getCustomDimension18() != null) b.putString(KEY_CUSTOM_DIMENSION_18, getCustomDimension18());
        if (getCustomDimension19() != null) b.putString(KEY_CUSTOM_DIMENSION_19, getCustomDimension19());
        if (getCustomDimension20() != null) b.putString(KEY_CUSTOM_DIMENSION_20, getCustomDimension20());

        if (getAdCustomDimension1() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_1, getAdCustomDimension1());
        if (getAdCustomDimension2() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_2, getAdCustomDimension2());
        if (getAdCustomDimension3() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_3, getAdCustomDimension3());
        if (getAdCustomDimension4() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_4, getAdCustomDimension4());
        if (getAdCustomDimension5() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_5, getAdCustomDimension5());
        if (getAdCustomDimension6() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_6, getAdCustomDimension6());
        if (getAdCustomDimension7() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_7, getAdCustomDimension7());
        if (getAdCustomDimension8() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_8, getAdCustomDimension8());
        if (getAdCustomDimension9() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_9, getAdCustomDimension9());
        if (getAdCustomDimension10() != null) b.putString(KEY_AD_CUSTOM_DIMENSION_10, getAdCustomDimension10());

        if (getAppName() != null) b.putString(KEY_APP_NAME, getAppName());
        if (getAppReleaseVersion() != null)
            b.putString(KEY_APP_RELEASE_VERSION, getAppReleaseVersion());

        b.putBoolean(KEY_WAIT_METADATA, getWaitForMetadata());
        if (getPendingMetadata() != null) b.putStringArrayList(KEY_PENDING_METADATA, getPendingMetadata());

        return b;
    }

    /**
     * Getter for enabled
     * @return true if Youbora is enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Enables or disables analytics reporting
     * @param enabled the new value
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Getter for HTTPS
     * @return true if HTTPS is enabled
     */
    public boolean isHttpSecure() {
        return httpSecure;
    }

    /**
     * Enables or disables analytics https
     * @param httpSecure the new value
     */
    public void setHttpSecure(boolean httpSecure) {
        this.httpSecure = httpSecure;
    }

    /**
     * Getter for the host
     * @return the NQS host
     */
    public String getHost() {
        return host;
    }

    /**
     * Setter for the host
     * @param host the new value
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Getter for the accountCode
     * @return the accountCode
     */

    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Setter for the accountCode
     * @param accountCode the new value
     */
    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    /**
     * Getter for the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter for the username
     * @param username the new value
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter for the anonymousUser
     * @return the anonymousUser
     */
    public String getAnonymousUser() {
        return anonymousUser;
    }

    /**
     * Setter for the anonymousUser
     * @param anonymousUser the new value
     */
    public void setAnonymousUser(String anonymousUser) {
        this.anonymousUser = anonymousUser;
    }

    /**
     * Getter for the userType
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * Setter for the userType
     * @param userType the new value
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * Getter for the offline flag
     * @return true if offline tracking is on
     */
    public boolean isOffline() {
        return offline;
    }

    /**
     * Setter for the offline flag
     * @param offline the new value
     */
    public void setOffline(boolean offline) {
        this.offline = offline;
    }

    /**
     * Getter for the isInfinity flag
     * @return true if infinity is on
     */
    public Boolean getIsInfinity() {
        return isInfinity;
    }

    /**
     * Setter for the isInfinity flag
     * @param isInfinity the new value
     */
    public void setIsInfinity(Boolean isInfinity) {
        this.isInfinity = isInfinity;
    }

    /**
     * Getter for experiment ids
     * @return an ArrayList with all experiment ids
     */
    public ArrayList<String> getExperimentIds() {
        return experimentIds;
    }

    /**
     * Setter for experiment ids
     * @param experimentIds the new value
     */
    public void setExperimentIds(ArrayList<String> experimentIds) {
        this.experimentIds = experimentIds;
    }

    /**
     * Getter for SmartSwitch config code
     * @return SmartSwitch config code
     */
    public String getSmartSwitchConfigCode() {
        return smartswitchConfigCode;
    }

    /**
     * Setter for SmartSwitch config code
     * @param smartSwitchConfigCode SmartSwitch config code
     */
    public void setSmartSwitchConfigCode(String smartSwitchConfigCode) {
        this.smartswitchConfigCode = smartSwitchConfigCode;
    }

    /**
     * Getter for SmartSwitch group code
     * @return SmartSwitch group code
     */
    public String getSmartSwitchGroupCode() {
        return smartswitchGroupCode;
    }

    /**
     * Setter for SmartSwitch cgroup code
     * @param smartswitchGroupCode SmartSwitch group code
     */
    public void setSmartSwitchGroupCode(String smartswitchGroupCode) {
        this.smartswitchGroupCode = smartswitchGroupCode;
    }

    /**
     * Getter for SmartSwitch contract code
     * @return SmartSwitch contract code
     */
    public String getSmartSwitchContractCode() {
        return smartswitchContractCode;
    }

    /**
     * Setter for SmartSwitch contract code
     * @param smartswitchContractCode SmartSwitch contract code
     */
    public void setSmartSwitchContractCode(String smartswitchContractCode) {
        this.smartswitchContractCode = smartswitchContractCode;
    }

    /**
     * Getter for the parseHls flag
     * @return true if parseHls is enabled
     */
    public boolean isParseHls() {
        return parseHls;
    }

    /**
     * Setter for the parseHls
     * @param parseHls the new value
     */
    public void setParseHls(boolean parseHls) {
        this.parseHls = parseHls;
    }

    /**
     * Getter for the parseCdnNameHeader
     * @return the parseCdnNameHeader
     */
    public String getParseCdnNameHeader() {
        return parseCdnNameHeader;
    }

    /**
     * Setter for the parseCdnNameHeader
     * @param parseCdnNameHeader the new value
     */
    public void setParseCdnNameHeader(String parseCdnNameHeader) {
        this.parseCdnNameHeader = parseCdnNameHeader;
    }

    /**
     * Getter for the parseCdnNode flag
     * @return true if parseCdnNode is enabled
     */
    public boolean isParseCdnNode() {
        return parseCdnNode;
    }

    /**
     * Setter for the parseCdnNode
     * @param parseCdnNode the new value
     */
    public void setParseCdnNode(boolean parseCdnNode) {
        this.parseCdnNode = parseCdnNode;
    }

    /**
     * Getter for the parseCdnNodeList list
     * @return the Cdns to parse
     */
    public ArrayList<String> getParseCdnNodeList() {
        return parseCdnNodeList;
    }

    /**
     * Setter for the parseCdnNodeList list
     * @param parseCdnNodeList the new list
     */
    public void setParseCdnNodeList(ArrayList<String> parseCdnNodeList) {
        this.parseCdnNodeList = parseCdnNodeList;
    }

    /**
     * Getter for the networkIP
     * @return the networkIP
     */
    public String getNetworkIP() {
        return networkIP;
    }

    /**
     * Setter for the networkIP
     * @param networkIP the new value
     */
    public void setNetworkIP(String networkIP) {
        this.networkIP = networkIP;
    }

    /**
     * Getter for the networkIsp
     * @return the networkIsp
     */
    public String getNetworkIsp() {
        return networkIsp;
    }

    /**
     * Setter for the networkIsp
     * @param networkIsp the new value
     */
    public void setNetworkIsp(String networkIsp) {
        this.networkIsp = networkIsp;
    }

    /**
     * Getter for the networkConnectionType
     * @return the networkConnectionType
     */
    public String getNetworkConnectionType() {
        return networkConnectionType;
    }

    /**
     * Setter for the networkConnectionType
     * @param networkConnectionType the new value
     */
    public void setNetworkConnectionType(String networkConnectionType) {
        this.networkConnectionType = networkConnectionType;
    }

    /**
     * Setter for wanting to obfuscate IP
     * @param obfuscateIp the new value
     */
    public void setNetworkObfuscateIp(boolean obfuscateIp){
        this.obfuscateIp = obfuscateIp;
    }

    /**
     * Getter for the obfuscateIp option
     * @return if IP should be ofuscated or not
     */
    public boolean getNetworkObfuscateIp(){
        return obfuscateIp;
    }

    /**
     * Getter for the deviceCode
     * @return the deviceCode
     */
    public String getDeviceCode() {
        return deviceCode;
    }

    /**
     * Setter for the deviceCode
     * @param deviceCode the new value
     */
    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    /**
     * Getter for the deviceModel
     * @return deviceModel
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * Setter for the deviceModel
     * @param deviceModel the new value
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    /**
     * Getter for the deviceBrand
     * @return deviceBrand
     */
    public String getDeviceBrand() {
        return deviceBrand;
    }

    /**
     * Setter for the deviceBrand
     * @param deviceBrand the new value
     */
    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }

    /**
     * Getter for the deviceType
     * @return deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Setter for the deviceType
     * @param deviceType the new value
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * Getter for the deviceOsName
     * @return deviceOsName
     */
    public String getDeviceOsName() {
        return deviceOsName;
    }

    /**
     * Setter for the deviceOsName
     * @param deviceOsName the new value
     */
    public void setDeviceOsName(String deviceOsName) {
        this.deviceOsName = deviceOsName;
    }

    /**
     * Getter for the deviceOsVersion
     * @return deviceOsVersion
     */
    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    /**
     * Setter for the deviceOsVersion
     * @param deviceOsVersion
     */
    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    /**
     * Getter for the contentResource
     * @return the contentResource
     */
    public String getContentResource() {
        return contentResource;
    }

    /**
     * Setter for the contentResource
     * @param contentResource the new value
     */
    public void setContentResource(String contentResource) {
        this.contentResource = contentResource;
    }

    /**
     * Getter for the contentIsLive flag
     * @return the contentIsLive
     */
    public Boolean getContentIsLive() {
        return contentIsLive;
    }

    /**
     * Setter for the contentIsLive
     * @param contentIsLive the new value
     */
    public void setContentIsLive(Boolean contentIsLive) {
        this.contentIsLive = contentIsLive;
    }

    /**
     * Getter for the contentTitle
     * @return the contentTitle
     */
    public String getContentTitle() {
        return contentTitle;
    }

    /**
     * Setter for the contentTitle
     * @param contentTitle the new value
     */
    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    /**
     * Getter for the contentTitle2
     * @return the contentTitle2
     * @deprecated use {@link #getProgram()} instead.
     */
    @Deprecated
    public String getContentTitle2() {
        return program;
    }

    /**
     * Setter for the contentTitle2
     * @param contentTitle2 the new value
     * @deprecated use {@link #setProgram(String)} instead.
     */
    @Deprecated
    public void setContentTitle2(String contentTitle2) {
        this.program = contentTitle2;
    }

    /**
     * program getter
     * @return program
     */
    public String getProgram() {
        return program;
    }

    /**
     * program setter
     * @param program's new value
     */
    public void setProgram(String program) {
        this.program = program;
    }

    /**
     * Getter for the contentDuration
     * @return the contentDuration
     */
    public Double getContentDuration() {
        return contentDuration;
    }

    /**
     * Setter for the contentDuration
     * @param contentDuration the new value
     */
    public void setContentDuration(Double contentDuration) {
        this.contentDuration = contentDuration;
    }

    /**
     * Getter for the contentTransactionCode
     * @return the contentTransactionCode
     */
    public String getContentTransactionCode() {
        return contentTransactionCode;
    }

    /**
     * Setter for the contentTransactionCode
     * @param contentTransactionCode the new value
     */
    public void setContentTransactionCode(String contentTransactionCode) {
        this.contentTransactionCode = contentTransactionCode;
    }

    /**
     * Getter for the contentBitrate
     * @return the contentBitrate
     */
    public Long getContentBitrate() {
        return contentBitrate;
    }

    /**
     * Setter for the contentBitrate
     * @param contentBitrate the new value
     */
    public void setContentBitrate(Long contentBitrate) {
        this.contentBitrate = contentBitrate;
    }

    /**
     * Getter for the contentThroughput
     * @return the contentThroughput
     */
    public Long getContentThroughput() {
        return contentThroughput;
    }

    /**
     * Setter for the contentThroughput
     * @param contentThroughput the new value
     */
    public void setContentThroughput(Long contentThroughput) {
        this.contentThroughput = contentThroughput;
    }

    /**
     * Getter for the contentRendition
     * @return the contentRendition
     */
    public String getContentRendition() {
        return contentRendition;
    }

    /**
     * Setter for the contentRendition
     * @param contentRendition the new value
     */
    public void setContentRendition(String contentRendition) {
        this.contentRendition = contentRendition;
    }

    /**
     * Getter for the contentCdn
     * @return the contentCdn
     */
    public String getContentCdn() {
        return contentCdn;
    }

    /**
     * Setter for the contentCdn
     * @param contentCdn the new value
     */
    public void setContentCdn(String contentCdn) {
        this.contentCdn = contentCdn;
    }

    /**
     * Getter for the contentFps
     * @return the contentFps
     */
    public Double getContentFps() {
        return contentFps;
    }

    /**
     * Setter for the contentFps
     * @param contentFps the new value
     */
    public void setContentFps(Double contentFps) {
        this.contentFps = contentFps;
    }

    /**
     * Getter for the contentIsLiveNoSeek
     * @return the contentIsLiveNoSeek
     */
    public Boolean getContentIsLiveNoSeek() {
        return contentIsLiveNoSeek;
    }

    /**
     * Setter for the contentIsLiveNoSeek
     * @param contentIsLiveNoSeek the new value
     */
    public void setContentIsLiveNoSeek(Boolean contentIsLiveNoSeek) {
        this.contentIsLiveNoSeek = contentIsLiveNoSeek;
    }

    /**
     * Getter for the contentResourceProtocol, accepted types are HDS, HLS, MSS, DASH, RTMP, RTP, RTSP
     * @return the contentResourceProtocol
     */
    public String getContentStreamingProtocol() {
        if(contentResourceProtocol == null){
            return null;
        }
        if (contentResourceProtocol.matches("HDS|HLS|MSS|DASH|RTMP|RTP|RTSP")){
            YouboraLog.warn("contentStreamingProtocol has a not valid value");

            return contentResourceProtocol;
        }
        YouboraLog.warn("contentStreamingProtocol has a not valid value");
        return null;
    }

    /**
     * Setter for the contentResourceProtocol
     * @param contentResourceProtocol the new value
     */
    public void setContentStreamingProtocol(String contentResourceProtocol) {
        this.contentResourceProtocol = contentResourceProtocol;
    }

    /**
     * Getter for the contentMetadata
     * @return the contentMetadata
     */
    public Bundle getContentMetadata() {
        return contentMetadata;
    }

    /**
     * Setter for the contentMetadata
     * @param contentMetadata the new value
     */
    public void setContentMetadata(Bundle contentMetadata) {
        this.contentMetadata = contentMetadata;
    }

    /**
     * Getter for the adMetadata
     * @return the adMetadata
     */
    public Bundle getAdMetadata() {
        return adMetadata;
    }

    /**
     * Setter for the adMetadata
     * @param adMetadata the new value
     */
    public void setAdMetadata(Bundle adMetadata) {
        this.adMetadata = adMetadata;
    }

    /**
     * Getter for the ignoreAds
     * @return the ignoreAds
     */
    public boolean getAdIgnore() {
        return ignoreAds;
    }

    /**
     * Setter for the ignoreAds
     * @param ignoreAds the new value
     */
    public void setAdIgnore(Boolean ignoreAds) {
        this.ignoreAds = ignoreAds;
    }

    /**
     * Getter for adAfterStop
     * @return the adAfterStop
     */
    public int getAdsAfterStop(){
        return adsAfterStop;
    }

    /**
     * Setter for adAfterStop
     * @param adAfterStop
     */
    public void setAdsAfterStop(int adAfterStop){
        this.adsAfterStop = adAfterStop;
    }

    /**
     * Getter for adCampaign
     * @return the adCampaign
     */
    public String getAdCampaign(){
        return adCampaign;
    }

    /**
     * Setter for adCampaign
     * @param adCampaign
     */
    public void setAdCampaign(String adCampaign){
        this.adCampaign = adCampaign;
    }

    /**
     * Getter for adTitle
     * @return the adTitle
     */
    public String getAdTitle(){
        return adTitle;
    }

    /**
     * Setter for adTitle
     * @param adTitle
     */
    public void setAdTitle(String adTitle){
        this.adTitle = adTitle;
    }

    /**
     * Getter for adResource
     * @return the adResource
     */
    public String getAdResource(){
        return adResource;
    }

    /**
     * Setter for adResource
     * @param adResource
     */
    public void setAdResource(String adResource){
        this.adResource = adResource;
    }

    /**
     * Setter for the auto background detection
     * @param autoDetectBackground the new value
     */
    public void setAutoDetectBackground(boolean autoDetectBackground){
        this.autoDetectBackground = autoDetectBackground;
    }

    /**
     * Getter for autoDetectBackground
     * @return autoBackground flag
     */
    public boolean isAutoDetectBackground(){
        return autoDetectBackground;
    }

    /**
     * Setter for the auto start feature
     * @param autoStart the new value
     */
    public void setAutoStart(boolean autoStart){
        this.autoStart = autoStart;
    }

    /**
     * Getter for autoStart
     * @return autoStart flag
     */
    public boolean isAutoStart(){
        return autoStart;
    }

    /**
     * Setter for the force init feature
     * @param forceInit the new value
     */
    public void setForceInit(boolean forceInit){
        this.forceInit = forceInit;
    }

    /**
     * Getter for forceInit
     * @return forceInit flag
     */
    public boolean isForceInit(){
        return forceInit;
    }

    /**
     * Getter for the extraparam1
     * @return the extraparam1
     * @deprecated use {@link #getCustomDimension1()} instead.
     */
    @Deprecated
    public String getExtraparam1() { return customDimension1; }

    /**
     * Setter for the extraparam1
     * @param extraparam1 the new value
     * @deprecated use {@link #setCustomDimension1(String)} instead.
     */
    @Deprecated
    public void setExtraparam1(String extraparam1) { this.customDimension1 = extraparam1; }

    /**
     * Getter for the extraparam2
     * @return the extraparam2
     * @deprecated use {@link #getCustomDimension2()} instead.
     */
    @Deprecated
    public String getExtraparam2() { return customDimension2; }

    /**
     * Setter for the extraparam2
     * @param extraparam2 the new value
     * @deprecated use {@link #setCustomDimension2(String)} instead.
     */
    @Deprecated
    public void setExtraparam2(String extraparam2) { this.customDimension2 = extraparam2; }

    /**
     * Getter for the extraparam3
     * @return the extraparam3
     * @deprecated use {@link #getCustomDimension3()} instead.
     */
    @Deprecated
    public String getExtraparam3() { return customDimension3; }

    /**
     * Setter for the extraparam3
     * @param extraparam3 the new value
     * @deprecated use {@link #setCustomDimension3(String)} instead.
     */
    @Deprecated
    public void setExtraparam3(String extraparam3) { this.customDimension3 = extraparam3; }

    /**
     * Getter for the extraparam4
     * @return the extraparam4
     * @deprecated use {@link #getCustomDimension4()} instead.
     */
    @Deprecated
    public String getExtraparam4() { return customDimension4; }

    /**
     * Setter for the extraparam4
     * @param extraparam4 the new value
     * @deprecated use {@link #setCustomDimension4(String)} instead.
     */
    @Deprecated
    public void setExtraparam4(String extraparam4) { this.customDimension4 = extraparam4; }

    /**
     * Getter for the extraparam5
     * @return the extraparam5
     * @deprecated use {@link #getCustomDimension5()} instead.
     */
    @Deprecated
    public String getExtraparam5() { return customDimension5; }

    /**
     * Setter for the extraparam5
     * @param extraparam5 the new value
     * @deprecated use {@link #setCustomDimension5(String)} instead.
     */
    @Deprecated
    public void setExtraparam5(String extraparam5) { this.customDimension5 = extraparam5; }

    /**
     * Getter for the extraparam6
     * @return the extraparam6
     * @deprecated use {@link #getCustomDimension6()} instead.
     */
    @Deprecated
    public String getExtraparam6() { return customDimension6; }

    /**
     * Setter for the extraparam6
     * @param extraparam6 the new value
     * @deprecated use {@link #setCustomDimension6(String)} instead.
     */
    @Deprecated
    public void setExtraparam6(String extraparam6) { this.customDimension6 = extraparam6; }

    /**
     * Getter for the extraparam7
     * @return the extraparam7
     * @deprecated use {@link #getCustomDimension7()} instead.
     */
    @Deprecated
    public String getExtraparam7() { return customDimension7; }

    /**
     * Setter for the extraparam7
     * @param extraparam7 the new value
     * @deprecated use {@link #setCustomDimension7(String)} instead.
     */
    @Deprecated
    public void setExtraparam7(String extraparam7) { this.customDimension7 = extraparam7; }

    /**
     * Getter for the extraparam8
     * @return the extraparam8
     * @deprecated use {@link #getCustomDimension8()} instead.
     */
    @Deprecated
    public String getExtraparam8() { return customDimension8; }

    /**
     * Setter for the extraparam8
     * @param extraparam8 the new value
     * @deprecated use {@link #setCustomDimension8(String)} instead.
     */
    @Deprecated
    public void setExtraparam8(String extraparam8) { this.customDimension8 = extraparam8; }

    /**
     * Getter for the extraparam9
     * @return the extraparam9
     * @deprecated use {@link #getCustomDimension9()} instead.
     */
    @Deprecated
    public String getExtraparam9() { return customDimension9; }

    /**
     * Setter for the extraparam9
     * @param extraparam9 the new value
     * @deprecated use {@link #setCustomDimension9(String)} instead.
     */
    @Deprecated
    public void setExtraparam9(String extraparam9) { this.customDimension9 = extraparam9; }

    /**
     * Getter for the extraparam10
     * @return the extraparam10
     * @deprecated use {@link #getCustomDimension10()} instead.
     */
    @Deprecated
    public String getExtraparam10() { return customDimension10; }

    /**
     * Setter for the extraparam10
     * @param extraparam10 the new value
     * @deprecated use {@link #setCustomDimension10(String)} instead.
     */
    @Deprecated
    public void setExtraparam10(String extraparam10) { this.customDimension10 = extraparam10; }

    /**
     * Getter for the extraparam11
     * @return the extraparam11
     * @deprecated use {@link #getCustomDimension11()} instead.
     */
    @Deprecated
    public String getExtraparam11() { return customDimension11; }

    /**
     * Setter for the extraparam11
     * @param extraparam11 the new value
     * @deprecated use {@link #setCustomDimension11(String)} instead.
     */
    @Deprecated
    public void setExtraparam11(String extraparam11) { this.customDimension11 = extraparam11; }

    /**
     * Getter for the extraparam12
     * @return the extraparam12
     * @deprecated use {@link #getCustomDimension12()} instead.
     */
    @Deprecated
    public String getExtraparam12() { return customDimension12; }

    /**
     * Setter for the extraparam12
     * @param extraparam12 the new value
     * @deprecated use {@link #setCustomDimension12(String)} instead.
     */
    @Deprecated
    public void setExtraparam12(String extraparam12) { this.customDimension12 = extraparam12; }

    /**
     * Getter for the extraparam13
     * @return the extraparam13
     * @deprecated use {@link #getCustomDimension13()} instead.
     */
    @Deprecated
    public String getExtraparam13() { return customDimension13; }

    /**
     * Setter for the extraparam13
     * @param extraparam13 the new value
     * @deprecated use {@link #setCustomDimension13(String)} instead.
     */
    @Deprecated
    public void setExtraparam13(String extraparam13) { this.customDimension13 = extraparam13; }

    /**
     * Getter for the extraparam4
     * @return the extraparam14
     * @deprecated use {@link #getCustomDimension14()} instead.
     */
    @Deprecated
    public String getExtraparam14() { return customDimension14; }

    /**
     * Setter for the extraparam4
     * @param extraparam14 the new value
     * @deprecated use {@link #setCustomDimension14(String)} instead.
     */
    @Deprecated
    public void setExtraparam14(String extraparam14) { this.customDimension14 = extraparam14; }

    /**
     * Getter for the extraparam15
     * @return the extraparam15
     * @deprecated use {@link #getCustomDimension15()} instead.
     */
    @Deprecated
    public String getExtraparam15() { return customDimension15; }

    /**
     * Setter for the extraparam15
     * @param extraparam15 the new value
     * @deprecated use {@link #setCustomDimension15(String)} instead.
     */
    @Deprecated
    public void setExtraparam15(String extraparam15) { this.customDimension15 = extraparam15; }

    /**
     * Getter for the extraparam16
     * @return the extraparam16
     * @deprecated use {@link #getCustomDimension16 instead.
     */
    @Deprecated
    public String getExtraparam16() { return customDimension16; }

    /**
     * Setter for the extraparam6
     * @param extraparam16 the new value
     * @deprecated use {@link #setCustomDimension16(String)} instead.
     */
    @Deprecated
    public void setExtraparam16(String extraparam16) { this.customDimension16 = extraparam16; }

    /**
     * Getter for the extraparam17
     * @return the extraparam17
     * @deprecated use {@link #getCustomDimension17()} instead.
     */
    @Deprecated
    public String getExtraparam17() { return customDimension17; }

    /**
     * Setter for the extraparam17
     * @param extraparam17 the new value
     * @deprecated use {@link #setCustomDimension17(String)} instead.
     */
    @Deprecated
    public void setExtraparam17(String extraparam17) { this.customDimension17 = extraparam17; }

    /**
     * Getter for the extraparam18
     * @return the extraparam18
     * @deprecated use {@link #getCustomDimension18()} instead.
     */
    @Deprecated
    public String getExtraparam18() { return customDimension18; }

    /**
     * Setter for the extraparam18
     * @param extraparam18 the new value
     * @deprecated use {@link #setCustomDimension18(String)} instead.
     */
    @Deprecated
    public void setExtraparam18(String extraparam18) { this.customDimension18 = extraparam18; }

    /**
     * Getter for the extraparam19
     * @return the extraparam19
     * @deprecated use {@link #getCustomDimension19()} instead.
     */
    @Deprecated
    public String getExtraparam19() { return customDimension19; }

    /**
     * Setter for the extraparam19
     * @param extraparam19 the new value
     * @deprecated use {@link #setCustomDimension19(String)} instead.
     */
    @Deprecated
    public void setExtraparam19(String extraparam19) { this.customDimension19 = extraparam19; }

    /**
     * Getter for the extraparam20
     * @return the extraparam20
     * @deprecated use {@link #getCustomDimension20()} instead.
     */
    @Deprecated
    public String getExtraparam20() { return customDimension20; }

    /**
     * Setter for the extraparam20
     * @param extraparam20 the new value
     * @deprecated use {@link #setCustomDimension20(String)} instead.
     */
    @Deprecated
    public void setExtraparam20(String extraparam20) { this.customDimension20 = extraparam20; }

    /**
     * Getter for the adExtraparam1
     * @return the adExtraparam1
     * @deprecated use {@link #getAdCustomDimension1()} instead.
     */
    @Deprecated
    public String getAdExtraparam1() { return adCustomDimension1; }

    /**
     * Setter for the extraparam1
     * @param AdExtraparam1 the new value
     * @deprecated use {@link #setAdCustomDimension1(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam1(String AdExtraparam1) { this.adCustomDimension1 = AdExtraparam1; }

    /**
     * Getter for the adExtraparam2
     * @return the adExtraparam2
     * @deprecated use {@link #getAdCustomDimension2()} instead.
     */
    @Deprecated
    public String getAdExtraparam2() { return adCustomDimension2; }

    /**
     * Setter for the extraparam2
     * @param AdExtraparam2 the new value
     * @deprecated use {@link #setAdCustomDimension2(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam2(String AdExtraparam2) { this.adCustomDimension2 = AdExtraparam2; }

    /**
     * Getter for the adExtraparam3
     * @return the adExtraparam3
     * @deprecated use {@link #getAdCustomDimension3()} instead.
     */
    @Deprecated
    public String getAdExtraparam3() { return adCustomDimension3; }

    /**
     * Setter for the adExtraparam3
     * @param AdExtraparam3 the new value
     * @deprecated use {@link #setAdCustomDimension3(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam3(String AdExtraparam3) { this.adCustomDimension3 = AdExtraparam3; }

    /**
     * Getter for the extraparam4
     * @return the extraparam4
     * @deprecated use {@link #getAdCustomDimension4()} instead.
     */
    @Deprecated
    public String getAdExtraparam4() { return adCustomDimension4; }

    /**
     * Setter for the adExtraparam4
     * @param AdExtraparam4 the new value
     * @deprecated use {@link #setAdCustomDimension4(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam4(String AdExtraparam4) { this.adCustomDimension4 = AdExtraparam4; }

    /**
     * Getter for the adExtraparam5
     * @return the adExtraparam5
     * @deprecated use {@link #getAdCustomDimension5()} instead.
     */
    @Deprecated
    public String getAdExtraparam5() { return adCustomDimension5; }

    /**
     * Setter for the adExtraparam5
     * @param AdExtraparam5 the new value
     * @deprecated use {@link #setAdCustomDimension5(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam5(String AdExtraparam5) { this.adCustomDimension5 = AdExtraparam5; }

    /**
     * Getter for the adExtraparam6
     * @return the adExtraparam6
     * @deprecated use {@link #getAdCustomDimension6()} instead.
     */
    @Deprecated
    public String getAdExtraparam6() { return adCustomDimension6; }

    /**
     * Setter for the extraparam6
     * @param AdExtraparam6 the new value
     * @deprecated use {@link #setAdCustomDimension6(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam6(String AdExtraparam6) { this.adCustomDimension6 = AdExtraparam6; }

    /**
     * Getter for the adExtraparam7
     * @return the adExtraparam7
     * @deprecated use {@link #getAdCustomDimension7()} instead.
     */
    @Deprecated
    public String getAdExtraparam7() { return adCustomDimension7; }

    /**
     * Setter for the adExtraparam7
     * @param AdExtraparam7 the new value
     * @deprecated use {@link #setAdCustomDimension7(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam7(String AdExtraparam7) { this.adCustomDimension7 = AdExtraparam7; }

    /**
     * Getter for the adExtraparam8
     * @return the adExtraparam8
     * @deprecated use {@link #getAdCustomDimension8()} instead.
     */
    @Deprecated
    public String getAdExtraparam8() { return adCustomDimension8; }

    /**
     * Setter for the adExtraparam8
     * @param AdExtraparam8 the new value
     * @deprecated use {@link #setAdCustomDimension8(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam8(String AdExtraparam8) { this.adCustomDimension8 = AdExtraparam8; }

    /**
     * Getter for the adExtraparam9
     * @return the adExtraparam9
     * @deprecated use {@link #getAdCustomDimension9()} instead.
     */
    @Deprecated
    public String getAdExtraparam9() { return adCustomDimension9; }

    /**
     * Setter for the adExtraparam9
     * @param AdExtraparam9 the new value
     * @deprecated use {@link #setAdCustomDimension9(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam9(String AdExtraparam9) { this.adCustomDimension9 = AdExtraparam9; }

    /**
     * Getter for the adExtraparam10
     * @return the adExtraparam10
     * @deprecated use {@link #getAdCustomDimension10()} instead.
     */
    @Deprecated
    public String getAdExtraparam10() { return adCustomDimension10; }

    /**
     * Setter for the adExtraparam10
     * @param AdExtraparam10 the new value
     * @deprecated use {@link #setAdCustomDimension10(String)} instead.
     */
    @Deprecated
    public void setAdExtraparam10(String AdExtraparam10) {
        this.adCustomDimension10 = AdExtraparam10;
    }

    /**
     * Getter for the customDimension1
     * @return customDimension1
     */
    public String getCustomDimension1() { return customDimension1; }

    /**
     * Setter for the customDimension1
     * @param customDimension1 new value
     */
    public void setCustomDimension1(String customDimension1) {
        this.customDimension1 = customDimension1;
    }

    /**
     * Getter for the customDimension2
     * @return customDimension2
     */
    public String getCustomDimension2() { return customDimension2; }

    /**
     * Setter for the customDimension2
     * @param customDimension2 new value
     */
    public void setCustomDimension2(String customDimension2) {
        this.customDimension2 = customDimension2;
    }

    /**
     * Getter for the customDimension3
     * @return customDimension3
     */
    public String getCustomDimension3() { return customDimension3; }

    /**
     * Setter for the customDimension3
     * @param customDimension3 new value
     */
    public void setCustomDimension3(String customDimension3) {
        this.customDimension3 = customDimension3;
    }

    /**
     * Getter for the customDimension4
     * @return customDimension4
     */
    public String getCustomDimension4() { return customDimension4; }

    /**
     * Setter for the customDimension4
     * @param customDimension4 new value
     */
    public void setCustomDimension4(String customDimension4) {
        this.customDimension4 = customDimension4;
    }

    /**
     * Getter for the customDimension5
     * @return customDimension5
     */
    public String getCustomDimension5() { return customDimension5; }

    /**
     * Setter for the customDimension5
     * @param customDimension5 new value
     */
    public void setCustomDimension5(String customDimension5) {
        this.customDimension5 = customDimension5;
    }

    /**
     * Getter for the customDimension6
     * @return customDimension6
     */
    public String getCustomDimension6() { return customDimension6; }

    /**
     * Setter for the customDimension6
     * @param customDimension6 new value
     */
    public void setCustomDimension6(String customDimension6) {
        this.customDimension6 = customDimension6;
    }

    /**
     * Getter for the customDimension7
     * @return customDimension7
     */
    public String getCustomDimension7() { return customDimension7; }

    /**
     * Setter for the customDimension7
     * @param customDimension7 new value
     */
    public void setCustomDimension7(String customDimension7) {
        this.customDimension7 = customDimension7;
    }

    /**
     * Getter for the customDimension8
     * @return customDimension8
     */
    public String getCustomDimension8() { return customDimension8; }

    /**
     * Setter for the customDimension8
     * @param customDimension8 new value
     */
    public void setCustomDimension8(String customDimension8) {
        this.customDimension8 = customDimension8;
    }

    /**
     * Getter for the customDimension9
     * @return customDimension9
     */
    public String getCustomDimension9() { return customDimension9; }

    /**
     * Setter for the customDimension9
     * @param customDimension9 new value
     */
    public void setCustomDimension9(String customDimension9) {
        this.customDimension9 = customDimension9;
    }

    /**
     * Getter for the customDimension10
     * @return customDimension10
     */
    public String getCustomDimension10() { return customDimension10; }

    /**
     * Setter for the customDimension10
     * @param customDimension10 new value
     */
    public void setCustomDimension10(String customDimension10) {
        this.customDimension10 = customDimension10;
    }

    /**
     * Getter for the customDimension11
     * @return customDimension11
     */
    public String getCustomDimension11() { return customDimension11; }

    /**
     * Setter for the customDimension11
     * @param customDimension11 new value
     */
    public void setCustomDimension11(String customDimension11) {
        this.customDimension11 = customDimension11;
    }

    /**
     * Getter for the customDimension12
     * @return customDimension12
     */
    public String getCustomDimension12() { return customDimension12; }

    /**
     * Setter for the customDimension12
     * @param customDimension12 new value
     */
    public void setCustomDimension12(String customDimension12) {
        this.customDimension12 = customDimension12;
    }

    /**
     * Getter for the customDimension13
     * @return customDimension13
     */
    public String getCustomDimension13() { return customDimension13; }

    /**
     * Setter for the customDimension13
     * @param customDimension13 new value
     */
    public void setCustomDimension13(String customDimension13) {
        this.customDimension13 = customDimension13;
    }

    /**
     * Getter for the customDimension4
     * @return customDimension14
     */
    public String getCustomDimension14() { return customDimension14; }

    /**
     * Setter for the customDimension4
     * @param customDimension14 new value
     */
    public void setCustomDimension14(String customDimension14) {
        this.customDimension14 = customDimension14;
    }

    /**
     * Getter for the customDimension15
     * @return customDimension15
     */
    public String getCustomDimension15() { return customDimension15; }

    /**
     * Setter for the customDimension15
     * @param customDimension15 new value
     */
    public void setCustomDimension15(String customDimension15) {
        this.customDimension15 = customDimension15;
    }

    /**
     * Getter for the customDimension16
     * @return customDimension16
     */
    public String getCustomDimension16() { return customDimension16; }

    /**
     * Setter for the customDimension6
     * @param customDimension16 new value
     */
    public void setCustomDimension16(String customDimension16) {
        this.customDimension16 = customDimension16;
    }

    /**
     * Getter for the customDimension17
     * @return customDimension17
     */
    public String getCustomDimension17() { return customDimension17; }

    /**
     * Setter for the customDimension17
     * @param customDimension17 new value
     */
    public void setCustomDimension17(String customDimension17) {
        this.customDimension17 = customDimension17;
    }

    /**
     * Getter for the customDimension18
     * @return customDimension18
     */
    public String getCustomDimension18() { return customDimension18; }

    /**
     * Setter for the customDimension18
     * @param customDimension18 new value
     */
    public void setCustomDimension18(String customDimension18) {
        this.customDimension18 = customDimension18;
    }

    /**
     * Getter for the customDimension19
     * @return customDimension19
     */
    public String getCustomDimension19() { return customDimension19; }

    /**
     * Setter for the customDimension19
     * @param customDimension19 new value
     */
    public void setCustomDimension19(String customDimension19) {
        this.customDimension19 = customDimension19;
    }

    /**
     * Getter for the customDimension20
     * @return customDimension20
     */
    public String getCustomDimension20() { return customDimension20; }

    /**
     * Setter for the customDimension20
     * @param customDimension20 new value
     */
    public void setCustomDimension20(String customDimension20) {
        this.customDimension20 = customDimension20;
    }

    /**
     * Getter for the adCustomDimension1
     * @return adCustomDimension1
     */
    public String getAdCustomDimension1() { return adCustomDimension1; }

    /**
     * Setter for the adCustomDimension1
     * @param adCustomDimension1 new value
     */
    public void setAdCustomDimension1(String adCustomDimension1) {
        this.adCustomDimension1 = adCustomDimension1;
    }

    /**
     * Getter for the adCustomDimension2
     * @return adCustomDimension2
     */
    public String getAdCustomDimension2() { return adCustomDimension2; }

    /**
     * Setter for the adCustomDimension2
     * @param adCustomDimension2 new value
     */
    public void setAdCustomDimension2(String adCustomDimension2) {
        this.adCustomDimension2 = adCustomDimension2;
    }

    /**
     * Getter for the adCustomDimension3
     * @return adCustomDimension3
     */
    public String getAdCustomDimension3() { return adCustomDimension3; }

    /**
     * Setter for the adCustomDimension3
     * @param adCustomDimension3 new value
     */
    public void setAdCustomDimension3(String adCustomDimension3) {
        this.adCustomDimension3 = adCustomDimension3;
    }

    /**
     * Getter for the adCustomDimension4
     * @return adCustomDimension4
     */
    public String getAdCustomDimension4() { return adCustomDimension4; }

    /**
     * Setter for the adCustomDimension4
     * @param adCustomDimension4 new value
     */
    public void setAdCustomDimension4(String adCustomDimension4) {
        this.adCustomDimension4 = adCustomDimension4;
    }

    /**
     * Getter for the adCustomDimension5
     * @return adCustomDimension5
     */
    public String getAdCustomDimension5() { return adCustomDimension5; }

    /**
     * Setter for the adCustomDimension5
     * @param adCustomDimension5 new value
     */
    public void setAdCustomDimension5(String adCustomDimension5) {
        this.adCustomDimension5 = adCustomDimension5;
    }

    /**
     * Getter for the adCustomDimension6
     * @return adCustomDimension6
     */
    public String getAdCustomDimension6() { return adCustomDimension6; }

    /**
     * Setter for the adCustomDimension6
     * @param adCustomDimension6 new value
     */
    public void setAdCustomDimension6(String adCustomDimension6) {
        this.adCustomDimension6 = adCustomDimension6;
    }

    /**
     * Getter for the adCustomDimension7
     * @return adCustomDimension7
     */
    public String getAdCustomDimension7() { return adCustomDimension7; }

    /**
     * Setter for the adCustomDimension7
     * @param adCustomDimension7 new value
     */
    public void setAdCustomDimension7(String adCustomDimension7) {
        this.adCustomDimension7 = adCustomDimension7;
    }

    /**
     * Getter for the adCustomDimension8
     * @return adCustomDimension8
     */
    public String getAdCustomDimension8() { return adCustomDimension8; }

    /**
     * Setter for the adCustomDimension8
     * @param adCustomDimension8 new value
     */
    public void setAdCustomDimension8(String adCustomDimension8) {
        this.adCustomDimension8 = adCustomDimension8;
    }

    /**
     * Getter for the adCustomDimension9
     * @return adCustomDimension9
     */
    public String getAdCustomDimension9() { return adCustomDimension9; }

    /**
     * Setter for the adCustomDimension9
     * @param adCustomDimension9 new value
     */
    public void setAdCustomDimension9(String adCustomDimension9) {
        this.adCustomDimension9 = adCustomDimension9;
    }

    /**
     * Getter for the adCustomDimension10
     * @return adCustomDimension10
     */
    public String getAdCustomDimension10() { return adCustomDimension10; }

    /**
     * Setter for the adCustomDimension10
     * @param adCustomDimension10 new value
     */
    public void setAdCustomDimension10(String adCustomDimension10) {
        this.adCustomDimension10 = adCustomDimension10;
    }

    /**
     * Getter for the app name
     * @return appName
     */
    public String getAppName() {
        return appName;
    }

    /**
     * Setter for the app name
     * @param appName
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     * Getter for the app release version
     * @return appReleaseVersion
     */
    public String getAppReleaseVersion() {
        return appReleaseVersion;
    }

    /**
     * Setter for the app release version
     * @param appReleaseVersion
     */
    public void setAppReleaseVersion(String appReleaseVersion) {
        this.appReleaseVersion = appReleaseVersion;
    }

    /**
     * Getter for the wait for metadata flag
     */
    public boolean getWaitForMetadata() {
        return waitForMetadata;
    }

    /**
     * Setter for the need to wait for metadata or not
     * @param waitForMetadata
     */
    public void setWaitForMetadata(boolean waitForMetadata) {
        this.waitForMetadata = waitForMetadata;
    }

    /**
     * Getter for the Array of keys the start is going to be delayed
     */
    public ArrayList<String> getPendingMetadata() {
        return pendingMetadata;
    }

    /**
     * Setter for the Array of keys the start is going to be delayed
     * @param pendingMetadata
     */
    public void setPendingMetadata(ArrayList<String> pendingMetadata) {
        this.pendingMetadata = pendingMetadata;
    }
}
