package com.npaw.youbora.lib6.plugin;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import com.npaw.youbora.lib6.BuildConfig;
import com.npaw.youbora.lib6.Chrono;
import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.DeviceInfo;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.Timer;
import com.npaw.youbora.lib6.YouboraPermissionUtils;
import com.npaw.youbora.lib6.YouboraUtil;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;
import com.npaw.youbora.lib6.comm.Communication;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.FlowTransform;
import com.npaw.youbora.lib6.comm.transform.OfflineTransform;
import com.npaw.youbora.lib6.comm.transform.ResourceTransform;
import com.npaw.youbora.lib6.comm.transform.Transform;
import com.npaw.youbora.lib6.comm.transform.ViewTransform;
import com.npaw.youbora.lib6.infinity.Infinity;
import com.npaw.youbora.lib6.persistence.AppDatabaseSingleton;
import com.npaw.youbora.lib6.persistence.EventDataSource;
import com.npaw.youbora.lib6.persistence.entity.Event;
import com.npaw.youbora.lib6.persistence.sharedpreferences.InfinitySharedPreferencesManager;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import static com.npaw.youbora.lib6.Constants.*;

/**
 * This is the main class of video analytics. You may want one instance for each video you want
 * to track. Will need {@link PlayerAdapter}s for both content and ads, manage options and general flow.
 * @author      Nice People at Work
 * @since       6.0
 */
public class Plugin {

    private ResourceTransform resourceTransform;
    private ViewTransform viewTransform;
    private RequestBuilder requestBuilder;
    private Timer pingTimer;
    private Timer beatTimer;
    private Options options;
    private PlayerAdapter adapter;
    private PlayerAdapter adsAdapter;
    private String lastServiceSent;
    private Long lastServiceTimemark;

    // Necessary for Foreground / Background mechanic

    /** Infinity initial variables **/
    private String startScreenName;
    private Map<String, String> startDimensions;
    private String startParentId;

    private Activity currentActivity;

    /** Application context, mostly used for offline **/
    private Context context;
    /** Activity reference just to check that the events come from our activity **/
    private Activity activity;
    /** Reference to the activity callbacks **/
    private Application.ActivityLifecycleCallbacks activityLifecycleCallbacks;

    /** Indicates that /init has been called. */
    private boolean isInitiated;
    /** Indicates that /start has been called. */
    private boolean isStarted;
    /** Indicates that /adStart has been called. */
    private boolean isAdStarted;
    /** Indicates that the content is preloading. */
    private boolean isPreloading;
    /** Chrono for preload times. */
    private Chrono preloadChrono;
    /** Chrono for init to join time. */
    private Chrono initChrono;

    /**
     * Instance of {@link Communication} used to send {@link Request}s.
     */
    public Communication comm;

    // "Will send" Listeners
    private List<WillSendRequestListener> willSendInitListeners;
    private List<WillSendRequestListener> willSendStartListeners;
    private List<WillSendRequestListener> willSendJoinListeners;
    private List<WillSendRequestListener> willSendPauseListeners;
    private List<WillSendRequestListener> willSendResumeListeners;
    private List<WillSendRequestListener> willSendSeekListeners;
    private List<WillSendRequestListener> willSendBufferListeners;
    private List<WillSendRequestListener> willSendErrorListeners;
    private List<WillSendRequestListener> willSendStopListeners;
    private List<WillSendRequestListener> willSendPingListeners;

    private List<WillSendRequestListener> willSendAdInitListeners;
    private List<WillSendRequestListener> willSendAdStartListeners;
    private List<WillSendRequestListener> willSendAdJoinListeners;
    private List<WillSendRequestListener> willSendAdClickListeners;
    private List<WillSendRequestListener> willSendAdPauseListeners;
    private List<WillSendRequestListener> willSendAdResumeListeners;
    private List<WillSendRequestListener> willSendAdBufferListeners;
    private List<WillSendRequestListener> willSendAdStopListeners;
    private List<WillSendRequestListener> willSendAdErrorListeners;

    private List<WillSendRequestListener> willSendSessionStartListeners;
    private List<WillSendRequestListener> willSendSessionStopListeners;
    private List<WillSendRequestListener> willSendSessionNavListeners;
    private List<WillSendRequestListener> willSendSessionEventListeners;
    private List<WillSendRequestListener> willSendSessionBeatListeners;

    /**
     * Constructor. Same as calling {@link #Plugin(Options, PlayerAdapter, Activity, Context)} with
     * a null adapter, activity and context
     * @param options instance of {@link Options}
     * @deprecated use {@link #Plugin(Options, Context)} instead.
     */
    public Plugin(Options options) { this(options, null, null, null); }

    /**
     * Constructor. Same as calling {@link #Plugin(Options, PlayerAdapter, Activity, Context)} with
     * a null adapter and activity
     * @param options instance of {@link Options}
     * @param context instance of {@link Context}
     */
    public Plugin(Options options, Context context) {
        this(options, null, null, context);
    }

    /**
     * Constructor. Same as calling {@link #Plugin(Options, PlayerAdapter, Activity, Context)} with
     * a null adapter
     * @param options instance of {@link Options}
     * @param activity instance of {@link Activity}
     */
    public Plugin(Options options, Activity activity) {
        this(options, null, activity, activity.getApplicationContext());
    }

    /**
     * Constructor. Same as calling {@link #Plugin(Options, PlayerAdapter, Activity, Context)} with
     * a null activity and context
     * @param options instance of {@link Options}
     * @param adapter instance of {@link PlayerAdapter}. Can also be specified afterwards with
     * {@link #setAdapter(PlayerAdapter)}.
     */
    public Plugin(Options options, PlayerAdapter adapter) {
        this(options, adapter, null, null);
    }

    /**
     * Constructor. Same as calling {@link #Plugin(Options, PlayerAdapter, Activity, Context)} with
     * a null activity
     * @param options instance of {@link Options}
     * @param adapter instance of {@link PlayerAdapter}. Can also be specified afterwards with
     * {@link #setAdapter(PlayerAdapter)}.
     * @param context instance of {@link Context}
     */
    public Plugin(Options options, PlayerAdapter adapter, Context context) {
        this(options, adapter, null, context);
    }

    /**
     * Constructor. Same as calling {@link #Plugin(Options, PlayerAdapter, Activity, Context)}
     * @param options instance of {@link Options}
     * @param adapter instance of {@link PlayerAdapter}. Can also be specified afterwards with
     * {@link #setAdapter(PlayerAdapter)}.
     * @param activity instance of {@link Activity}
     */
    public Plugin(Options options, PlayerAdapter adapter, Activity activity) {
        this(options, adapter, activity, activity.getApplicationContext());
    }

    /**
     * Constructor
     * @param options instance of {@link Options}
     * @param adapter instance of {@link PlayerAdapter}. Can also be specified afterwards with
     * {@link #setAdapter(PlayerAdapter)}.
     * @param context instance of {@link Context}
     */
    public Plugin(Options options, PlayerAdapter adapter, Activity activity, Context context) {
        setActivity(activity);
        setApplicationContext(context);

        if (options == null) {
            YouboraLog.warn("Options is null");
            options = createOptions();
        }

        preloadChrono = createChrono();
        initChrono = createChrono();
        this.options = options;

        if (adapter != null) {
            setAdapter(adapter);
        }

        pingTimer = createTimer(new Timer.TimerEventListener() {
            @Override
            public void onTimerEvent(long delta) {
                sendPing(delta);
                //We "use" the ping timer to check if any metadata was missing too
                if (isExtraMetadataReady()) startListener(null);
            }
        }, 5000);
        beatTimer = createBeatTimer(new Timer.TimerEventListener() {
            @Override
            public void onTimerEvent(long delta) {
                sendBeat(delta);
            }
        }, 30000);

        requestBuilder = createRequestBuilder(this);
        resourceTransform = createResourceTransform(this);

        initializeViewTransform();
    }

    private void initializeViewTransform() {
        viewTransform = createViewTransform(this);

        viewTransform.addTransformDoneListener(new Transform.TransformDoneListener() {
            @Override
            public void onTransformDone(Transform transform) {
                int interval = getOptions().isOffline() ? 60 : viewTransform.fastDataConfig.pingTime;
                pingTimer.setInterval(interval * 1000);
                if (!getOptions().isOffline()) {
                    interval = viewTransform.fastDataConfig.beatTime;
                    beatTimer.setInterval(interval * 1000);
                }
            }
        });

        viewTransform.init();
    }

    /**
     * Resets plugin to initial state
     */
    private void reset() {
        stopPings();

        resourceTransform = createResourceTransform(this);

        isInitiated = false;
        isStarted = false;
        isAdStarted = false;
        isPreloading = false;
        initChrono.reset();
        preloadChrono.reset();
    }

    /**
     * Returns the {@link ViewTransform} instance used by the Plugin
     * @return the ViewTransform instance
     */
    public ViewTransform getViewTransform() {
        return viewTransform;
    }

    /**
     * Returns the {@link ResourceTransform} instance used by the Plugin
     * @return the ResourceTransform instance
     */
    public ResourceTransform getResourceTransform() {
        return resourceTransform;
    }

    // --------------------------------- Instance builders -----------------------------------------
    Chrono createChrono() {
        return new Chrono();
    }

    Options createOptions() {
        return new Options();
    }

    Timer createTimer(Timer.TimerEventListener listener, long interval) {
        return new Timer(listener, interval);
    }

    Timer createBeatTimer(Timer.TimerEventListener listener, long interval) {
        return new Timer(listener, interval);
    }

    RequestBuilder createRequestBuilder(Plugin plugin) {
        return new RequestBuilder(plugin);
    }

    ResourceTransform createResourceTransform(Plugin plugin) {
        return new ResourceTransform(plugin);
    }

    ViewTransform createViewTransform(Plugin plugin) {
        return new ViewTransform(plugin);
    }

    Communication createCommunication(){
        return new Communication();
    }

    OfflineTransform createOfflineTransform() {
        return new OfflineTransform();
    }

    FlowTransform createFlowTransform() {
        return new FlowTransform();
    }

    Request createRequest(String host, String service) {
        return new Request(host, service);
    }

    private Boolean isSessionExpired() {
        boolean val = false;

        if (viewTransform.fastDataConfig.expirationTime != null)
            val = Infinity.getInstance().getLastSent() != null && (Infinity.getInstance().getLastSent()
                + viewTransform.fastDataConfig.expirationTime * 1000) < System.currentTimeMillis();

        return val;
    }

    // ------------------------------------- Options ----------------------------------------------
    /**
     * Update the options. This will replace the whole Options object. If you want to change only
     * a few options call {@link #getOptions()} and modify the received object.
     * @param options The {@link Options} to set
     */
    public void setOptions(Options options) {
        this.options = options;
    }

    /**
     * Returns the current plugin Options.
     *
     * This method returns the actual options instance, so any changes performed on them will
     * automatically be reflected without the need to call {@link #setOptions(Options)}
     * @return the options
     */
    public Options getOptions() {
        return options;
    }

    // ------------------------------------- Adapters ----------------------------------------------
    /**
     * Sets an adapter for video content.
     *
     * @param adapter The adapter to set
     */
    public void setAdapter(PlayerAdapter adapter) {

        removeAdapter(false);

        if (adapter != null) {

            this.adapter = adapter;
            adapter.setPlugin(this);

            // Register listeners
            adapter.addEventListener(eventListener);

            registerForActivityCallbacks();

        } else {
            YouboraLog.error("Adapter is null in setAdapter");
        }
    }

    /**
     * Returns the current adapter or null if not set
     * @return adapter
     */
    public PlayerAdapter getAdapter() {
        return adapter;
    }

    /**
     * Removes the current adapter. Stopping pings if no ads adapter.
     */
    public void removeAdapter() {
        removeAdapter(true);
    }


    /**
     * Removes adapter. Fires stop if needed. Calls {@link PlayerAdapter#dispose()}
     * @param stopPings if pings should stop
     */
    public void removeAdapter(boolean stopPings) {
        if (adapter != null) {
            adapter.dispose();

            adapter.setPlugin(null);

            // Remove listeners
            adapter.removeEventListener(eventListener);

            unregisterForActivityCallbacks();

            adapter = null;
        }

        if (stopPings && adsAdapter == null) {
            fireStop();
        }
    }

    /**
     * Sets an adapter for ads.
     *
     * @param adsAdapter adapter to set
     */
    public void setAdsAdapter(PlayerAdapter adsAdapter) {

        if (adsAdapter == null) {
            YouboraLog.error("Adapter is null in setAdsAdapter");
        } else if (adsAdapter.getPlugin() != null) {
            YouboraLog.warn("Adapters can only be added to a single plugin");
        } else {
            removeAdsAdapter(false);

            this.adsAdapter = adsAdapter;
            this.adsAdapter.setPlugin(this);

            adsAdapter.addEventListener(adEventListener);
        }
    }

    /**
     * Returns the current ads adapter or null
     * @return the current ads adapter or null
     */
    public PlayerAdapter getAdsAdapter() {
        return adsAdapter;
    }

    /**
     * Removes the current ads adapter. Stopping pings if no adapter.
     */
    public void removeAdsAdapter() {
        removeAdsAdapter(true);
    }

    /**
     * Removes ads adapter. Fires stop if needed. Calls {@link PlayerAdapter#dispose()}
     * @param stopPings if pings should stop
     */
    public void removeAdsAdapter(boolean stopPings) {
        if (adsAdapter != null) {
            adsAdapter.dispose();

            adsAdapter.setPlugin(null);

            adsAdapter.removeEventListener(adEventListener);

            adsAdapter = null;
        }

        if (stopPings && adapter == null) {
            fireStop();
        }

    }

    // -------------------------------------- INFINITY ----------------------------------------------
    /**
     * Returns the infinity instance object as singleton, since we need to use the same for all
     * the application
     */
    public Infinity getInfinity(Context context) {
        if(!Infinity.isInitiated){
            Infinity.getInstance().setPlugin(this);
            Infinity.getInstance().setContext(context);
            Infinity.getInstance().addEventListener(infinityEventListener);
            Infinity.getInstance().setViewTransform(viewTransform);
        }
        return Infinity.getInstance();
    }

    public Infinity getInfinity() {
        if (!Infinity.isInitiated && getApplicationContext() != null) {
            return getInfinity(getApplicationContext());
        }
        return Infinity.getInstance();
    }

    // -------------------------------------- DISABLE ----------------------------------------------
    /**
     * Disable request sending.
     * Same as calling {@link Options#setEnabled(boolean)} with false
     */
    public void disable() {
        options.setEnabled(false);
    }

    /**
     * Re-enable request sending.
     * Same as calling {@link Options#setEnabled(boolean)} with true
     */
    public void enable() {
        options.setEnabled(true);
    }

    // -------------------------------------- ACTIVITY CALLBACKS -----------------------------------

    /**
     * Registers the activity to its events for background
     */
    private void registerForActivityCallbacks() {

        if (getActivity() != null && activityLifecycleCallbacks == null) {
            if (currentActivity == null) currentActivity = getActivity();

            activityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {

                @Override
                public void onActivityCreated(Activity activity, Bundle bundle) { }

                @Override
                public void onActivityStarted(Activity activity) {
                    if (getOptions() != null && getOptions().getIsInfinity() != null &&
                            getOptions().getIsInfinity() == Boolean.TRUE &&
                            currentActivity == activity) {
                        YouboraLog.debug("Foreground event detected, resuming beats...");

                        if (getInfinity().getFlags().isStarted() && !isSessionExpired()) {
                            if (beatTimer.getChrono().getStartTime() != null)
                                sendBeat(Chrono.getNow() - beatTimer.getChrono().getStartTime());

                            startBeats();
                        } else {
                            getInfinity().getFlags().reset();
                            initializeViewTransform();
                            getInfinity().setViewTransform(viewTransform);
                            getInfinity().begin(startScreenName, startDimensions, startParentId);
                        }
                    }

                    currentActivity = activity;
                }

                @Override
                public void onActivityResumed(Activity activity) { }

                @Override
                public void onActivityPaused(Activity activity) { }

                @Override
                public void onActivityStopped(Activity activity) {
                    if (getOptions().isAutoDetectBackground() && getActivity() == activity) {
                        if (getAdsAdapter() != null && getAdsAdapter().getFlags().isStarted())
                            getAdsAdapter().fireStop();

                        fireStop();
                    }

                    if (getOptions() != null && getOptions().getIsInfinity() != null &&
                            getOptions().getIsInfinity() == Boolean.TRUE &&
                            currentActivity == activity) {
                        YouboraLog.debug("Background event detected, stopping beats...");

                        if (getInfinity().getFlags().isStarted()) {
                            if (beatTimer.getChrono().getStartTime() != null)
                                sendBeat(Chrono.getNow() - beatTimer.getChrono().getStartTime());

                            stopBeats();
                        }
                    }
                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) { }

                @Override
                public void onActivityDestroyed(Activity activity) { }
            };

            getActivity().getApplication().registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
        } else if (getActivity() == null) {
                YouboraLog.error("The plugin could not send stop, plugin.setActivity must be " +
                        "called before setting the adapter");
        }
    }

    /**
     * Unregisters the activity to its events for background
     */
    private void unregisterForActivityCallbacks() {
        if (getActivity() != null) {
            getActivity().getApplication().unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
            activityLifecycleCallbacks = null;
        }

    }

    // ------------------------------------- LISTENERS ---------------------------------------------

    /**
     * Perform the necessary operations to build a {@link Request} and sends it with the corresponding
     * params. Also calls the listeners passed by param just before sending the Request.
     * @param listenerList list of {@link WillSendRequestListener}s that will be invoked to notify
     *                     that the Request is about to be sent
     * @param service the service that is being sent
     * @param params optional Map of pre-set params that will be also sent in the Request
     * @param method optional {@link Request} method
     * @param body optional {@link Request} body, only if using {@link Request#METHOD_POST}
     * @param successListener callback {@link com.npaw.youbora.lib6.comm.Request.RequestSuccessListener} that will be invoked
     *                        when a Request has been successful
     * @param successListenerParams {@link Map<String,Object>} with params to return if request successful
     */
    private void send(List<WillSendRequestListener> listenerList, String service,
                      Map<String, String> params, String method, String body,
                      Request.RequestSuccessListener successListener,
                      Map<String, Object> successListenerParams) {


        params = requestBuilder.buildParams(params, service);

        if (listenerList != null) {
            for (Plugin.WillSendRequestListener listener : listenerList) {
                try {
                    listener.willSendRequest(service, this, params);
                } catch (Exception e) {
                    YouboraLog.error("Exception while calling willSendRequest");
                    YouboraLog.error(e);
                }
            }
        }

        if (comm != null && params != null && options.isEnabled()) {
            Request r = createRequest(null, service);

            Map<String, Object> m = new HashMap<>();
            m.putAll(params);
            r.setParams(m);
            r.setMethod(method);
            r.setBody(body);

            lastServiceSent = r.getService();
            lastServiceTimemark = System.currentTimeMillis();

            comm.sendRequest(r, successListener, successListenerParams);
        }
    }

    /**
     * Perform the necessary operations to build a {@link Request} and sends it with the corresponding
     * params. Also calls the listeners passed by param just before sending the Request.
     * @param listenerList list of {@link WillSendRequestListener}s that will be invoked to notify
     *                     that the Request is about to be sent
     * @param service the service that is being sent
     * @param params optional Map of pre-set params that will be also sent in the Request
     */
    private void send(List<WillSendRequestListener> listenerList, String service, Map<String, String> params) {
        send(listenerList, service, params, Request.METHOD_GET, null, null, null);
    }

    /**
     * Perform the necessary operations to build a {@link Request} and sends it with the corresponding
     * params. Also calls the listeners passed by param just before sending the Request.
     * @param listenerList list of {@link WillSendRequestListener}s that will be invoked to notify
     *                     that the Request is about to be sent
     * @param service the service that is being sent
     * @param params optional Map of pre-set params that will be also sent in the Request
     */
    private void sendInfinity(List<WillSendRequestListener> listenerList, String service, Map<String, String> params) {
        /*if(Infinity.getInstance().getLastSent() + (5 * 1000) < Chrono.getNow()){
            viewTransform.nextView();
        }*/
        params = requestBuilder.buildParams(params, service);

        if (listenerList != null) {
            for (Plugin.WillSendRequestListener listener : listenerList) {
                try {
                    listener.willSendRequest(service, this, params);
                } catch (Exception e) {
                    YouboraLog.error("Exception while calling willSendRequest");
                    YouboraLog.error(e);
                }
            }
        }

        if (getInfinity().getCommunication() != null && params != null && options.isEnabled()) {
            Request r = createRequest(null, service);

            Map<String, Object> m = new HashMap<>();
            m.putAll(params);
            r.setParams(m);

            lastServiceSent = r.getService();

            getInfinity().getCommunication().sendRequest(r, null, null);
        }
    }

    /**
     * If the current resource is not null this will trigger the resource parsing.
     * If resource parsing has already started or has ended, it should do nothing.
     */
    private void startResourceParsing() {
        String resource = getResource();

        if (resource != null)
            resourceTransform.init(resource);
    }

    /**
     * Initializes comm and its transforms.
     */
    public void initComm() {
        comm = createCommunication();

        comm.addTransform(createFlowTransform());
        comm.addTransform(resourceTransform);

        if (options.isOffline()) {
            if (getApplicationContext() != null) {
                AppDatabaseSingleton.init(getApplicationContext());
                comm.addTransform(createOfflineTransform());
            } else {
                YouboraLog.notice("To use the offline feature you have to set the application context");
            }
        } else {
            comm.addTransform(viewTransform);
        }
    }

    /**
     * Initializes comm with the desired transforms
     * @param transforms List of transforms to be added to {@link Communication} object
     */
    public void initComm(List<Transform> transforms) {
        comm = createCommunication();

        for (Transform transform : transforms)
            comm.addTransform(transform);
    }

    /**
     * Starts preloading state and chronos.
     */
    public void firePreloadBegin() {
        if (!isPreloading) {
            isPreloading = true;
            preloadChrono.start();
        }
    }

    /**
     * Ends preloading state and chronos.
     */
    public void firePreloadEnd() {
        if (isPreloading) {
            isPreloading = false;
            preloadChrono.stop();
        }
    }

    /**
     * Same as calling {@link #fireInit(Map)} with a {@code null} param
     */
    public void fireInit() { fireInit(null); }

    /**
     * Sends /init. Should be called once the user has requested the content. Does not need
     * a working adapter or player to work. It won't sent start if isInitiated is true.
     *
     * @param params Map of params to add to the request.
     */
    public void fireInit(Map<String, String> params) {
        if (!isInitiated && !isStarted) {
            viewTransform.nextView();
            initComm();
            startPings();

            isInitiated = true;
            initChrono.start();

            sendInit(params);
            registerForActivityCallbacks();
        }

        startResourceParsing();
    }

    private void sendInit(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_INIT);
        send(willSendInitListeners, SERVICE_INIT, params);
        String titleOrResource = params != null ? params.get("title") : "unknown";

        if (titleOrResource == null)
            titleOrResource = params.get("mediaResource");

        YouboraLog.notice(SERVICE_INIT + " " + titleOrResource);
    }

    public void fireOfflineEvents() { fireOfflineEvents(null); }

    private void fireOfflineEvents(Map<String, String> params) {
        if (getOptions().isOffline()) {
            YouboraLog.error("To send offline events, offline option must be disabled");
            return;
        }

        if (getAdapter() != null && getAdapter().getFlags() != null && getAdapter().getFlags().isStarted()
                && getAdsAdapter() != null && getAdsAdapter().getFlags().isStarted()) {
            YouboraLog.error("Adapters have to be stopped");
            return;
        }

        comm = createCommunication();
        comm.addTransform(viewTransform);

        try {
            final EventDataSource dataSource = new EventDataSource(getApplicationContext());
            dataSource.initDatabase();
            dataSource.getAllEvents(new EventDataSource.QuerySuccessListener() {
                @Override
                public void onQueryResolved(Object queryResult) {
                    List<Event> events = (List<Event>) queryResult;
                    if(events.size() == 0){
                        YouboraLog.debug("No offline events, skipping...");
                        return;
                    }
                    dataSource.getLastId(new EventDataSource.QuerySuccessListener() {
                        @Override
                        public void onQueryResolved(Object queryResult) {
                            int lastId = (Integer) queryResult;
                            for(int k = 0 ; k < lastId + 1 ; k++){
                                dataSource.getByOfflineId(k, new EventDataSource.QuerySuccessListener() {
                                    @Override
                                    public void onQueryResolved(Object queryResult) {
                                        List<Event> events = (List<Event>) queryResult;
                                        String eventsString = generateJsonString(events);
                                        if (events != null && events.size() > 0){
                                            sendOfflineEvents(eventsString, events.get(0).getOfflineId());
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } catch (Exception e) {
            YouboraLog.error(e);
        }
    }

    private String generateJsonString(List<Event> events) {
        String jsonString = "[";
        String stringFormat = "%s,%s";

        for (Event event : events) {
            stringFormat = jsonString.equals("[") ? "%s%s" : "%s,%s";
            jsonString = String.format(stringFormat,jsonString,event.getJsonEvents());
        }

        return String.format("%s]",jsonString);
    }

    private void sendOfflineEvents(String params, int offlineId) {
        HashMap<String,Object> listenerParams = new HashMap<>();
        listenerParams.put(Constants.SUCCESS_LISTENER_OFFLINE_ID, offlineId);

        AppDatabaseSingleton.init(getApplicationContext());
        final EventDataSource dataSource = new EventDataSource();
        dataSource.initDatabase();

        Request.RequestSuccessListener sucessListener = new Request.RequestSuccessListener(){

            @Override
            public void onRequestSuccess(HttpURLConnection connection, String response, Map<String, Object> listenerParams) {
                int offlineId = (int)listenerParams.get(Constants.SUCCESS_LISTENER_OFFLINE_ID);
                dataSource.deleteEvents(offlineId, new EventDataSource.QuerySuccessListener() {
                    @Override
                    public void onQueryResolved(Object queryResult) {
                        YouboraLog.debug("Offline events deleted (it's fake, remember to change it)");
                    }
                });
            }
        };

        send(null, SERVICE_OFFLINE_EVENTS, null, Request.METHOD_POST, params, sucessListener, listenerParams);
    }

    /**
     * Basic error handler. msg, code, errorMetadata and level params can be included in the params
     * argument.
     * @param params params to add to the request. If it is null default values will be added.
     */
    public void fireError(Map<String, String> params) {
        sendError(YouboraUtil.buildErrorParams(params));
    }

    /**
     * Sends a non-fatal error (with {@code level = "error"}).
     * @param msg Error message (should be unique for the code)
     * @param code Error code reported
     * @param errorMetadata Extra error info, if available.
     */
    public void fireError(String msg, String code, String errorMetadata) {
        sendError(YouboraUtil.buildErrorParams(msg, code, errorMetadata, "error"));
    }

    /**
     * Sends a fatal error (with {@code level = "error"}).
     * @param msg Error message (should be unique for the code)
     * @param code Error code reported
     * @param errorMetadata Extra error info, if available.
     * @param ex Exception to filter by in adapter to prevent duplicates
     */
    public void fireFatalError(String msg, String code, String errorMetadata, Exception ex) {
        if (adapter != null) {
            if (ex != null)
                adapter.fireFatalError(msg, code, errorMetadata, ex);
            else
                adapter.fireFatalError(msg,code,errorMetadata);
        } else {
            sendError(YouboraUtil.buildErrorParams(msg, code, errorMetadata, ""));
            //fireError(YouboraUtil.buildErrorParams(msg, code, errorMetadata, ""));
        }

        fireStop();
    }

    /**
     * Sends stop request
     */
    public void fireStop() {
        if (getAdapter() != null && getAdapter().getFlags().isStarted())
            getAdapter().fireStop();
        else
            fireStop(null);
    }

    /**
     * Sends stop in case of no adapter
     * @param params
     */
    public void fireStop(Map<String, String> params) {
        if (isInitiated)
            stopListener(params);
    }

    private void startListener(Map<String, String> params) {
        if ((!isInitiated && !isStarted) || SERVICE_ERROR.equals(lastServiceSent)) {
            viewTransform.nextView();
            initComm();
            startPings();
        }

        startResourceParsing();

        if (isInitiated && getAdapter().getFlags().isJoined() && !isStarted && isExtraMetadataReady()) {
            sendStart(params);
        }

        if (!isInitiated && !isForceInit()
                && getTitle() != null && getResource() != null && getIsLive() != null
                && isLiveOrNotNullDuration() && !isStarted && isExtraMetadataReady())
            sendStart(params);
        else if (!isInitiated)
            fireInit(params);
    }

    private boolean isLiveOrNotNullDuration() {
        return getIsLive() || (getDuration() != null && getDuration() != 0.0);
    }

    /**
     * Sends /init service. Should be called once the user has requested the content.
     *
     * Does not need an adapter or player to work.
     *
     * @param params Map of params to add to the request.
     */
    private void sendStart(Map<String, String> params) {
        /*if(getIsLive() != null){
            if(getIsLive() || getOptions().getContentIsLive()){
                if (params == null) {
                    params = new HashMap<>();
                }
            }
        }*/

        params = requestBuilder.buildParams(params, SERVICE_START);
        send(willSendStartListeners, SERVICE_START, params);

        String titleOrResource = getTitle();

        if (titleOrResource == null)
            titleOrResource = getResource();

        YouboraLog.notice(SERVICE_START + " " + titleOrResource);
        isStarted = true;
    }

    private void joinListener(Map<String, String> params) {
        if (adsAdapter == null || !adsAdapter.getFlags().isStarted()) {
            if (isInitiated && !isStarted && !getOptions().getWaitForMetadata())
                sendStart(new HashMap<String, String>());

            sendJoin(params);
        } else {
            // Revert join state
            if (adapter != null) {
                if (adapter.getMonitor() != null) adapter.getMonitor().stop();
                adapter.getFlags().setJoined(false);
                adapter.getChronos().join.setStopTime(null);
            }
        }

        /*if(adsAdapter != null && adsAdapter.getFlags() != null && adsAdapter.getFlags().isAdInitiated()){
            adsAdapter.getFlags().reset();
            adsAdapter.getChronos().reset();
        }*/
    }

    private void sendJoin(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_JOIN);
        send(willSendJoinListeners, SERVICE_JOIN, params);
        YouboraLog.notice(SERVICE_JOIN + " " + params.get("joinDuration") + "ms");
    }

    private void pauseListener(Map<String, String> params) {
        if (adapter != null) {
            if (adapter.getFlags().isBuffering() ||
                    adapter.getFlags().isSeeking() ||
                    (adsAdapter != null && adsAdapter.getFlags().isStarted())) {
                adapter.getChronos().pause.reset();
            }
        }

        sendPause(params);
    }

    private void sendPause(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_PAUSE);
        send(willSendPauseListeners, SERVICE_PAUSE, params);
        YouboraLog.notice(SERVICE_PAUSE+ " at " + params.get("playhead") + "s");
    }

    private void resumeListener(Map<String, String> params) { sendResume(params); }

    private void sendResume(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_RESUME);
        send(willSendResumeListeners, SERVICE_RESUME, params);
        YouboraLog.notice(SERVICE_RESUME+ " " + params.get("pauseDuration") + "ms");
    }

    private void seekBeginListener() {
        if (adapter != null && adapter.getFlags().isPaused())
            adapter.getChronos().pause.reset();

        YouboraLog.notice("Seek Begin");
    }

    private void seekEndListener(Map<String, String> params) { sendSeekEnd(params); }

    private void sendSeekEnd(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_SEEK);
        send(willSendSeekListeners, SERVICE_SEEK, params);
        YouboraLog.notice(SERVICE_SEEK + " to " + params.get("playhead") + " in " + params.get("seekDuration") + "ms");
    }

    private void bufferBeginListener() {
        if (adapter != null && adapter.getFlags().isPaused())
            adapter.getChronos().pause.reset();

        YouboraLog.notice("Buffer begin");
    }

    private void bufferEndListener(Map<String, String> params) { sendBufferEnd(params); }

    private void sendBufferEnd(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_BUFFER);
        send(willSendBufferListeners, SERVICE_BUFFER, params);
        YouboraLog.notice(SERVICE_BUFFER + " to " + params.get("playhead") + " in " + params.get("bufferDuration") + "ms");
    }

    private void errorListener(Map<String, String> params) {
        boolean isFatal = "fatal".equals(params.get("errorLevel"));

        params.remove("errorLevel");

        sendError(params);

        if (isFatal)
            reset();
    }

    private void sendError(Map<String, String> params) {
        if((!isInitiated
                && (getAdapter() == null || (getAdapter() != null && !getAdapter().getFlags().isStarted())))
                && !((lastServiceSent != null && lastServiceTimemark != null)
                && (lastServiceSent.equals(Constants.SERVICE_STOP)
                && lastServiceTimemark + 500 > System.currentTimeMillis())))
            viewTransform.nextView();

        if(comm == null)
            initComm();

        startResourceParsing();
        params = requestBuilder.buildParams(params, SERVICE_ERROR);
        send(willSendErrorListeners, SERVICE_ERROR, params);
        YouboraLog.notice(SERVICE_ERROR + " " + " " + params.get("errorCode"));
        /*if(lastErrorCode == null){
            lastErrorTimeStamp = System.currentTimeMillis();
            lastErrorCode = params.get("errorCode");
        }*/
    }

    private void stopListener(Map<String, String> params) {
        //TODO Check this
        //if(adsAdapter != null && adsAdapter.getFlags().isStarted()) adsAdapter.fireStop();
        sendStop(params);
        reset();
    }

    private void sendStop(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_STOP);
        send(willSendStopListeners, SERVICE_STOP, params);
        requestBuilder.getLastSent().put("adNumber", null);
        YouboraLog.notice(SERVICE_STOP + " at " + params.get("playhead"));
    }

    // Ads
    private void adInitListener(Map<String, String> params) {
        if (adapter != null && adsAdapter != null) {
            adapter.fireSeekEnd();
            adapter.fireBufferEnd();
            if (adapter.getFlags().isPaused())
                adapter.getChronos().pause.reset();
        }

        sendAdInit(params);
    }

    private void sendAdInit(Map<String, String> params) {
        String realNumber = requestBuilder.getNewAdNumber();
        params = requestBuilder.buildParams(params, SERVICE_AD_INIT);
        params.put("adNumber", realNumber);
        params.put("adDuration","0");
        params.put("adPlayhead","0");
        adsAdapter.getFlags().setAdInitiated(true);
        send(willSendAdInitListeners, SERVICE_AD_INIT, params);
        YouboraLog.notice(SERVICE_AD_INIT + " " + params.get("position") + params.get("adNumber") + " at " + params.get("playhead") + "s");
    }

    private void adStartListener(Map<String, String> params) {
        if (adapter != null) {
            adapter.fireSeekEnd();
            adapter.fireBufferEnd();
            if (adapter.getFlags().isPaused())
                adapter.getChronos().pause.reset();
        }

        if (!isInitiated && !isStarted)
            fireInit();

        if (getAdDuration() != null && getAdTitle() != null && getAdResource() != null
                && !adsAdapter.getFlags().isAdInitiated())
            sendAdStart(params);
        else if (!adsAdapter.getFlags().isAdInitiated())
            sendAdInit(params);
    }

    private void sendAdStart(Map<String, String> params) {
        startPings();
        String realNumber = adsAdapter.getFlags().isAdInitiated() ?
                requestBuilder.getLastSent().get("adNumber") : requestBuilder.getNewAdNumber();
        params = requestBuilder.buildParams(params, SERVICE_AD_START);
        params.put("adNumber", realNumber);
        send(willSendAdStartListeners, SERVICE_AD_START, params);
        YouboraLog.notice(SERVICE_AD_START + " " + params.get("adPosition") +
                params.get("adNumber") + " at " + params.get("playhead") + "s");
        isAdStarted = true;
    }

    private void adJoinListener(Map<String, String> params) {
        if (adsAdapter.getFlags().isAdInitiated() && !isAdStarted)
            sendAdStart(params);
        sendAdJoin(params);
    }

    private void sendAdJoin(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_JOIN);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdJoinListeners, SERVICE_AD_JOIN, params);
        YouboraLog.notice(SERVICE_AD_JOIN + " " + params.get("adJoinDuration") + "ms");
    }

    private void adPauseListener(Map<String, String> params) {
        sendAdPause(params);
    }

    private void sendAdPause(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_PAUSE);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdPauseListeners, SERVICE_AD_PAUSE, params);
        YouboraLog.notice(SERVICE_AD_PAUSE + " at " + params.get("adPlayhead") + "s");
    }

    private void adResumeListener(Map<String, String> params) {
        sendAdResume(params);
    }

    private void sendAdResume(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_RESUME);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdResumeListeners, SERVICE_AD_RESUME, params);
        YouboraLog.notice(SERVICE_AD_RESUME + " " + params.get("adPauseDuration") + "ms");
    }

    private void adBufferBeginListener() {
        if (adsAdapter != null && adsAdapter.getFlags().isPaused()) {
            adsAdapter.getChronos().pause.reset();
        }
        YouboraLog.notice("Ad Buffer Begin");
    }

    private void adBufferEndListener(Map<String, String> params) {
        sendAdBufferEnd(params);
    }

    private void sendAdBufferEnd(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_BUFFER);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdBufferListeners, SERVICE_AD_BUFFER, params);
        YouboraLog.notice(SERVICE_AD_BUFFER + " " + params.get("adBufferDuration") + "s");
    }

    private void adStopListener(Map<String, String> params) {
        // Remove time from joinDuration, "delaying" the start time

        if (!(adapter != null && adapter.getFlags().isJoined()) && adsAdapter != null) {
            // Chrono realJoinChrono = isInitiated ? initChrono : adapter.getChronos().join;
            Chrono realJoinChrono = initChrono;

            if (adapter != null && adapter.getChronos() != null && !isInitiated)
                realJoinChrono = adapter.getChronos().join;

            Long startTime = realJoinChrono.getStartTime();

            if (startTime == null)
                startTime = Chrono.getNow();

            Long adDeltaTime = adsAdapter.getChronos().total.getDeltaTime();

            if (adDeltaTime == -1)
                adDeltaTime = Chrono.getNow();

            startTime = Math.min(startTime + adDeltaTime, Chrono.getNow());
            realJoinChrono.setStartTime(startTime);
        }

        sendAdStop(params);
    }

    private void sendAdStop(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_STOP);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdStopListeners, SERVICE_AD_STOP, params);
        YouboraLog.notice(SERVICE_AD_STOP + " " + params.get("adTotalDuration") + "ms");
        isAdStarted = false;
    }

    private void adClickListener(Map<String, String> params){
        sendAdClick(params);
    }

    private void sendAdClick(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_AD_CLICK);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdClickListeners, SERVICE_AD_CLICK, params);
        YouboraLog.notice(SERVICE_AD_CLICK + " " + params.get("adPlayhead") + "ms");
    }

    private void adAllAdsCompleted(Map<String, String> params) {
        if(getAdapter() != null && getAdapter().getFlags() != null && !getAdapter().getFlags().isStarted()) stopPings();
    }

    private void adErrorListener(Map<String, String> params){
        sendAdError(params);
    }

    private void sendAdError(Map<String, String> params) {
        if (!isInitiated && !isStarted)
            initComm();

        startResourceParsing();
        params = requestBuilder.buildParams(params, SERVICE_AD_ERROR);
        params.put("adNumber", requestBuilder.getLastSent().get("adNumber"));
        send(willSendAdErrorListeners, SERVICE_AD_ERROR, params);
        YouboraLog.notice(SERVICE_AD_ERROR + " " + " " + params.get("errorCode"));
    }

    // ----------------------------------------- Infinity listeners --------------------------------

    private void sessionStartListener(String screenName, Map<String, String> dimensions,
                                      String parentId){
        viewTransform.nextView();

        startScreenName = screenName;
        startDimensions = dimensions;
        startParentId = parentId;

        Map<String, String> params = new LinkedHashMap<>();
        params.put("dimensions", YouboraUtil.stringifyMap(dimensions));
        params.put("page", screenName);
        params.put("route", screenName);

        registerForActivityCallbacks();

        sendSessionStart(params);
    }

    private void sendSessionStart(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_SESSION_START);
        sendInfinity(willSendSessionStartListeners, SERVICE_SESSION_START, params);
        startBeats();
        YouboraLog.notice(SERVICE_SESSION_START);
    }

    private void sessionStopListener(Map<String, String> params) {
        unregisterForActivityCallbacks();
        sendSessionStop(params);
    }

    private void sendSessionStop(Map<String, String> params) {
        params = requestBuilder.buildParams(params, SERVICE_SESSION_STOP);
        getInfinity().removeActiveSession(params.get("code"));
        sendInfinity(willSendSessionStartListeners, SERVICE_SESSION_STOP, params);
        stopBeats();
        YouboraLog.notice(SERVICE_SESSION_STOP);
    }

    private void sessionNavListener(String screenName) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("page", screenName);
        params.put("route", screenName);
        sendSessionNav(params);
    }

    private void sendSessionNav(Map<String, String> params){
        params = requestBuilder.buildParams(params, SERVICE_SESSION_NAV);
        sendInfinity(willSendSessionNavListeners, SERVICE_SESSION_NAV, params);
        YouboraLog.notice(SERVICE_SESSION_NAV);

        if(beatTimer != null){
            long time = beatTimer.getChrono().getStartTime() != null ?
                    (Chrono.getNow() - beatTimer.getChrono().getStartTime()) : 0;
            sendBeat(time);
            beatTimer.getChrono().setStartTime(time);
        }
    }

    private void sessionEventListener(Map<String, String> dimensions, Map<String, Double> values,
                                      String eventName){
        Map<String, String> params = new LinkedHashMap<>();
        params.put("dimensions", YouboraUtil.stringifyMap(dimensions));
        params.put("values", YouboraUtil.stringifyMap(values));
        params.put("name", eventName);
        sendSessionEvent(params);
    }

    private void sendSessionEvent(Map<String, String> params){
        params = requestBuilder.buildParams(params, SERVICE_SESSION_EVENT);
        sendInfinity(willSendSessionEventListeners, SERVICE_SESSION_EVENT, params);
        YouboraLog.notice(SERVICE_SESSION_EVENT);
    }

    // ----------------------------------------- BEATS ---------------------------------------------
    private void startBeats() {
        if (!beatTimer.isRunning()) {
            beatTimer.start();
        }
    }

    private void stopBeats() {
        beatTimer.stop();
    }

    private void sendBeat(long diffTime) {
        if (viewTransform.fastDataConfig.code != null) {
            Map<String, String> params = new HashMap<>();
            params.put("diffTime", Long.toString(diffTime));

            if (getInfinity().getActiveSessions() == null)
                getInfinity().addActiveSession(this.viewTransform.fastDataConfig.code);

            List<String> paramList = new LinkedList<>();
            paramList.add("sessions");
            params = requestBuilder.fetchParams(params, paramList, false);

            sendInfinity(willSendSessionBeatListeners, SERVICE_SESSION_BEAT, params);
            YouboraLog.debug(SERVICE_SESSION_BEAT);
        }
    }

    // ----------------------------------------- PINGS ---------------------------------------------
    private void startPings() {
        if (!pingTimer.isRunning()) {
            pingTimer.start();
        }
    }

    private void stopPings() {
        pingTimer.stop();
    }

    private void sendPing(long diffTime) {
        Map<String, String> params = new HashMap<>();
        params.put("diffTime", Long.toString(diffTime));
        Map<String, String> changedEntities = requestBuilder.getChangedEntities();
        if (changedEntities != null && !changedEntities.isEmpty()) {
            params.put("entities", YouboraUtil.stringifyMap(changedEntities));
        }

        List<String> paramList = new LinkedList<>();

        if (adapter != null) {

            if (adapter.getFlags().isPaused()) {
                paramList.add("pauseDuration");
            } else {
                paramList.add("bitrate");
                paramList.add("throughput");
                paramList.add("fps");

                if (adsAdapter != null && adsAdapter.getFlags().isStarted()) {
                    paramList.add("adBitrate");
                }
            }

            if (adapter.getFlags().isJoined()) {
                paramList.add("playhead");
            }

            if (adapter.getFlags().isBuffering()) {
                paramList.add("bufferDuration");
            }

            if (adapter.getFlags().isSeeking()) {
                paramList.add("seekDuration");
            }

            if (adapter.getIsP2PEnabled() != null && adapter.getIsP2PEnabled()) {
                paramList.add("p2pDownloadedTraffic");
                paramList.add("cdnDownloadedTraffic");
                paramList.add("uploadTraffic");
            }
        }

        if (adsAdapter != null) {
            if (adsAdapter.getFlags().isStarted()) {
                paramList.add("adPlayhead");
                paramList.add("playhead");
            }

            if (adsAdapter.getFlags().isBuffering()) {
                paramList.add("adBufferDuration");
            }
        }

        params = requestBuilder.fetchParams(params, paramList, false);

        send(willSendPingListeners, SERVICE_PING, params);
        YouboraLog.debug(SERVICE_PING);
    }

    // ------------------------------------- INFO GETTERS ------------------------------------------
    /**
     * Returns the host where all the NQS traces are sent.
     * @return the gost
     */
    public String getHost() {
        return YouboraUtil.addProtocol(YouboraUtil.stripProtocol(options.getHost()), options.isHttpSecure());
    }

    /**
     * Returns the parse HLS flag
     * @return the parse HLS flag
     */
    public boolean isParseHls() {
        return options.isParseHls();
    }

    /**
     * Returns the parse Cdn node flag
     * @return the parse Cdn node flag
     */
    public boolean isParseCdnNode() {
        return options.isParseCdnNode();
    }

    /**
     * Returns the list of Cdn names that are enabled for the {@link ResourceTransform}.
     * @return the cdn list
     */
    public List<String> getParseCdnNodeList() {
        return options.getParseCdnNodeList();
    }

    /**
     * Returns the Cdn name header. This value is later passed to
     * {@link com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser#setBalancerHeaderName(String)}.
     * @return the cdn name header.
     */
    public String getParseCdnNodeNameHeader() {
        return options.getParseCdnNameHeader();
    }

    /**
     * Returns the content's playhead in seconds
     * @return the content's playhead
     */
    public Double getPlayhead() {
        Double ph = null;
        if (adapter != null) {
            try {
                ph = adapter.getPlayhead();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayhead");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(ph, 0.0);
    }

    /**
     * Returns the content's Playrate
     * @return the content's Playrate
     */
    public Double getPlayrate() {
        Double val = null;
        if (adapter != null) {
            try {
                val = adapter.getPlayrate();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayrate");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 1.0);
    }

    /**
     * Returns the content's FPS
     * @return the content's FPS
     */
    public Double getFramesPerSecond() {
        Double val = options.getContentFps();
        if (val == null && adapter != null) {
            try {
                val = adapter.getFramesPerSecond();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getFramesPerSecond");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the content's dropped frames
     * @return the content's dropped frames
     */
    public Integer getDroppedFrames() {
        Integer val = null;
        if (adapter != null) {
            try {
                val = adapter.getDroppedFrames();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getDroppedFrames");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, 0);
    }

    /**
     * Returns the content's duration in seconds
     * @return the content's duration
     */
    public Double getDuration() {
        Double val = options.getContentDuration();
        if (val == null && adapter != null) {
            try {
                if(getIsLive() || adapter.getDuration() == null){
                    val = 0.0;
                }else{
                    val = adapter.getDuration();
                }
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getDuration");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0.0);
    }

    /**
     * Returns the content's bitrate in bits per second
     * @return the content's bitrate
     */
    public Long getBitrate() {
        Long val = options.getContentBitrate();
        if (val == null && adapter != null) {
            try {
                val = adapter.getBitrate();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getBitrate");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    /**
     * Returns the content's throughput in bits per second
     * @return the content's throughput
     */
    public Long getThroughput() {
        Long val = options.getContentThroughput();
        if (val == null && adapter != null) {
            try {
                val = adapter.getThroughput();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getThroughput");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    /**
     * Returns the content's rendition
     * @return the content's rendition
     */
    public String getRendition() {
        String val = options.getContentRendition();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getRendition();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getRendition");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the content's title
     * @return the content's title
     */
    public String getTitle() {
        String val = options.getContentTitle();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getTitle();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getTitle");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the content's title2
     * @return the content's title2
     * @deprecated use {@link #getProgram()} instead.
     */
    public String getTitle2() {
        String val = options.getProgram();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getProgram();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getTitle2");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns content's program
     * @return program
     */
    public String getProgram() {
        String val = options.getProgram();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getProgram();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getProgram");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns defined streaming protocol
     * @return the content's streaming protocol
     */
    public String getStreamingProtocol() {
        return options.getContentStreamingProtocol();
    }

    /**
     * Returns whether the content is live or not
     * @return whether the content is live or not
     */
    public Boolean getIsLive() {
        Boolean val = options.getContentIsLive();
        if (val == null && adapter != null) {
            try {
                val = adapter.getIsLive();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getIsLive");
                YouboraLog.error(e);
            }
        }

        return val != null ? val : false;
    }

    /**
     * Returns the content's resource after being parsed by the {@link ResourceTransform}
     * @return the content's resource
     */
    public String getResource() {
        String val = null;
        if (!resourceTransform.isBlocking(null)) {
            val = resourceTransform.getResource();
        }

        if (val == null) {
            val = getOriginalResource();
        }

        return val;
    }

    /**
     * Returns the content's original resource (before being parsed by the {@link ResourceTransform})
     * @return the content's original resource
     */
    public String getOriginalResource() {
        String val = options.getContentResource();
        if ((val == null || val.length() == 0) && adapter != null) {
            try {
                val = adapter.getResource();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getResource");
                YouboraLog.error(e);
            }
        }

        if (val != null && val.length() == 0) {
            val = null;
        }

        return val;
    }

    /**
     * Returns the transaction code
     * @return the transaction code
     */
    public String getTransactionCode() {
        return options.getContentTransactionCode();
    }

    /**
     * Returns experiments list
     * @return all set experiments
     */
    public ArrayList<String> getExperimentIds(){
        return options.getExperimentIds();
    }

    /**
     * Returns the content metadata
     * @return the content metadata
     */
    public String getContentMetadata() {
        return YouboraUtil.stringifyBundle(options.getContentMetadata());
    }

    /**
     * Returns the version of the player that is used to play the content
     * @return the player version
     */
    public String getPlayerVersion() {
        String val = null;
        if (adapter != null) {
            try {
                val = adapter.getPlayerVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayerVersion");
                YouboraLog.error(e);
            }
        }

        if (val == null) val = "";

        return val;
    }

    /**
     * Returns the name of the player that is used to play the content
     * @return the player name
     */
    public String getPlayerName() {
        String val = null;
        if (adapter != null) {
            try {
                val = adapter.getPlayerName();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPlayerName");
                YouboraLog.error(e);
            }
        }

        if (val == null) val = "";

        return val;
    }

    /**
     * Returns the content cdn
     * @return the content cdn
     */
    public String getCdn() {
        String cdn = null;
        if (!resourceTransform.isBlocking(null)) {
            cdn = resourceTransform.getCdnName();
        }
        if (cdn == null) {
            cdn = options.getContentCdn();
        }
        return cdn;
    }

    /**
     * Returns current latency from player
     * @return current video latency
     */
    public Double getLatency() {
        Double val = null;
        if (adapter != null) {
            try {
                val = adapter.getLatency();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getLatency");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0.0);
    }

    /**
     * Returns current packages being lost
     * @return current video latency
     */
    public Integer getPacketLoss() {
        Integer val = null;
        if (adapter != null) {
            try {
                val = adapter.getPacketLoss();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPacketLoss");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0);
    }

    /**
     * Returns current packages being send
     * @return current video latency
     */
    public Integer getPacketSent(){
        Integer val = null;
        if (adapter != null) {
            try {
                val = adapter.getPacketSent();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getPacketLoss");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0);
    }

    /**
     * Returns the PluginVersion
     * @return the PluginVersion
     */
    public String getPluginVersion() {
        String ret = getAdapterVersion();
        if (ret == null) {
            ret = BuildConfig.VERSION_NAME + "-adapterless";
        }
        return ret;
    }

    /**
     * Returns the content {@link PlayerAdapter} version if available
     * @return the content Adapter version
     */
    private String getAdapterVersion() {
        String val = null;
        if (adapter != null) {
            try {
                val = adapter.getVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdapterVersion");
                YouboraLog.error(e);
            }
        }
        return val;
    }

    /**
     * Returns content's Extraparam1
     * @return extraparam 1 value
     * @deprecated use {@link #getCustomDimension1()} instead.
     */
    public String getExtraparam1() { return options.getCustomDimension1(); }

    /**
     * Returns content's Extraparam2
     * @return extraparam 2 value
     * @deprecated use {@link #getCustomDimension2()} instead.
     */
    public String getExtraparam2() { return options.getCustomDimension2(); }

    /**
     * Returns content's Extraparam
     * @return extraparam 3 value
     * @deprecated use {@link #getCustomDimension3()} instead.
     */
    public String getExtraparam3() { return options.getCustomDimension3(); }

    /**
     * Returns content's Extraparam4
     * @return extraparam 4 value
     * @deprecated use {@link #getCustomDimension4()} instead.
     */
    public String getExtraparam4() { return options.getCustomDimension4(); }

    /**
     * Returns content's Extraparam5
     * @return extraparam 5 value
     * @deprecated use {@link #getCustomDimension5()} instead.
     */
    public String getExtraparam5() { return options.getCustomDimension5(); }

    /**
     * Returns content's Extraparam6
     * @return extraparam 6 value
     * @deprecated use {@link #getCustomDimension6()} instead.
     */
    public String getExtraparam6() { return options.getCustomDimension6(); }

    /**
     * Returns content's Extraparam7
     * @return extraparam 7 value
     * @deprecated use {@link #getCustomDimension7()} instead.
     */
    public String getExtraparam7() { return options.getCustomDimension7(); }

    /**
     * Returns content's Extraparam8
     * @return extraparam 8 value
     * @deprecated use {@link #getCustomDimension8()} instead.
     */
    public String getExtraparam8() { return options.getCustomDimension8(); }

    /**
     * Returns content's Extraparam9
     * @return extraparam 9 value
     * @deprecated use {@link #getCustomDimension9()} instead.
     */
    public String getExtraparam9() { return options.getCustomDimension9(); }

    /**
     * Returns content's Extraparam10
     * @return extraparam 10 value
     * @deprecated use {@link #getCustomDimension10()} instead.
     */
    public String getExtraparam10() { return options.getCustomDimension10(); }

    /**
     * Returns content's Extraparam11
     * @return extraparam 11 value
     * @deprecated use {@link #getCustomDimension11()} instead.
     */
    public String getExtraparam11() { return options.getCustomDimension11(); }

    /**
     * Returns content's Extraparam12
     * @return extraparam 12 value
     * @deprecated use {@link #getCustomDimension12()} instead.
     */
    public String getExtraparam12() { return options.getCustomDimension12(); }

    /**
     * Returns content's Extraparam13
     * @return extraparam 13 value
     * @deprecated use {@link #getCustomDimension13()} instead.
     */
    public String getExtraparam13() { return options.getCustomDimension13(); }

    /**
     * Returns content's Extraparam14
     * @return extraparam 14 value
     * @deprecated use {@link #getCustomDimension14()} instead.
     */
    public String getExtraparam14() { return options.getCustomDimension14(); }

    /**
     * Returns content's Extraparam15
     * @return extraparam 15 value
     * @deprecated use {@link #getCustomDimension15()} instead.
     */
    public String getExtraparam15() { return options.getCustomDimension15(); }

    /**
     * Returns content's Extraparam16
     * @return extraparam 16 value
     * @deprecated use {@link #getCustomDimension16()} instead.
     */
    public String getExtraparam16() { return options.getCustomDimension16(); }

    /**
     * Returns content's Extraparam17
     * @return extraparam 17 value
     * @deprecated use {@link #getCustomDimension17()} instead.
     */
    public String getExtraparam17() { return options.getCustomDimension17(); }

    /**
     * Returns content's Extraparam18
     * @return extraparam 18 value
     * @deprecated use {@link #getCustomDimension18()} instead.
     */
    public String getExtraparam18() { return options.getCustomDimension18(); }

    /**
     * Returns content's Extraparam19
     * @return extraparam 19 value
     * @deprecated use {@link #getCustomDimension19()} instead.
     */
    public String getExtraparam19() { return options.getCustomDimension19(); }

    /**
     * Returns content's Extraparam20
     * @return extraparam 20 value
     * @deprecated use {@link #getCustomDimension20()} instead.
     */
    public String getExtraparam20() { return options.getCustomDimension20(); }

    /**
     * Returns ad's Extraparam1
     * @return extraparam 1 value
     * @deprecated use {@link #getAdCustomDimension1()} instead.
     */
    public String getAdExtraparam1() { return options.getAdCustomDimension1(); }

    /**
     * Returns ad's Extraparam2
     * @return extraparam 2 value
     * @deprecated use {@link #getAdCustomDimension2()} instead.
     */
    public String getAdExtraparam2() { return options.getAdCustomDimension2(); }

    /**
     * Returns ad's Extraparam
     * @return extraparam 3 value
     * @deprecated use {@link #getAdCustomDimension3()} instead.
     */
    public String getAdExtraparam3() { return options.getAdCustomDimension3(); }

    /**
     * Returns ad's Extraparam4
     * @return extraparam 4 value
     * @deprecated use {@link #getAdCustomDimension4()} instead.
     */
    public String getAdExtraparam4() { return options.getAdCustomDimension4(); }

    /**
     * Returns ad's Extraparam5
     * @return extraparam 5 value
     * @deprecated use {@link #getAdCustomDimension5()} instead.
     */
    public String getAdExtraparam5() { return options.getAdCustomDimension5(); }

    /**
     * Returns ad's Extraparam6
     * @return extraparam 6 value
     * @deprecated use {@link #getAdCustomDimension6()} instead.
     */
    public String getAdExtraparam6() { return options.getAdCustomDimension6(); }

    /**
     * Returns ad's Extraparam7
     * @return extraparam 7 value
     * @deprecated use {@link #getAdCustomDimension7()} instead.
     */
    public String getAdExtraparam7() { return options.getAdCustomDimension7(); }

    /**
     * Returns ad's Extraparam8
     * @return extraparam 8 value
     * @deprecated use {@link #getAdCustomDimension8()} instead.
     */
    public String getAdExtraparam8() { return options.getAdCustomDimension8(); }

    /**
     * Returns ad's Extraparam9
     * @return extraparam 9 value
     * @deprecated use {@link #getAdCustomDimension9()} instead.
     */
    public String getAdExtraparam9() { return options.getAdCustomDimension9(); }

    /**
     * Returns ad's Extraparam10
     * @return extraparam 10 value
     * @deprecated use {@link #getAdCustomDimension10()} instead.
     */
    public String getAdExtraparam10() { return options.getAdCustomDimension10(); }

    /**
     * Returns content's customDimension1
     * @return customDimension1
     */
    public String getCustomDimension1() { return options.getCustomDimension1(); }

    /**
     * Returns content's customDimension2
     * @return customDimension2
     */
    public String getCustomDimension2() { return options.getCustomDimension2(); }

    /**
     * Returns content's customDimension
     * @return customDimension3
     */
    public String getCustomDimension3() { return options.getCustomDimension3(); }

    /**
     * Returns content's customDimension4
     * @return customDimension4
     */
    public String getCustomDimension4() { return options.getCustomDimension4(); }

    /**
     * Returns content's customDimension5
     * @return customDimension5
     */
    public String getCustomDimension5() { return options.getCustomDimension5(); }

    /**
     * Returns content's customDimension6
     * @return customDimension6
     */
    public String getCustomDimension6() { return options.getCustomDimension6(); }

    /**
     * Returns content's customDimension7
     * @return customDimension7
     */
    public String getCustomDimension7() { return options.getCustomDimension7(); }

    /**
     * Returns content's customDimension8
     * @return customDimension8
     */
    public String getCustomDimension8() { return options.getCustomDimension8(); }

    /**
     * Returns content's customDimension9
     * @return customDimension9
     */
    public String getCustomDimension9() { return options.getCustomDimension9(); }

    /**
     * Returns content's customDimension10
     * @return customDimension10
     */
    public String getCustomDimension10() { return options.getCustomDimension10(); }

    /**
     * Returns content's customDimension11
     * @return customDimension11
     */
    public String getCustomDimension11() { return options.getCustomDimension11(); }

    /**
     * Returns content's customDimension12
     * @return customDimension12
     */
    public String getCustomDimension12() { return options.getCustomDimension12(); }

    /**
     * Returns content's customDimension13
     * @return customDimension13
     */
    public String getCustomDimension13() { return options.getCustomDimension13(); }

    /**
     * Returns content's customDimension14
     * @return customDimension14
     */
    public String getCustomDimension14() { return options.getCustomDimension14(); }

    /**
     * Returns content's customDimension15
     * @return customDimension15
     */
    public String getCustomDimension15() { return options.getCustomDimension15(); }

    /**
     * Returns content's customDimension16
     * @return customDimension16
     */
    public String getCustomDimension16() { return options.getCustomDimension16(); }

    /**
     * Returns content's customDimension17
     * @return customDimension17
     */
    public String getCustomDimension17() { return options.getCustomDimension17(); }

    /**
     * Returns content's customDimension18
     * @return customDimension18
     */
    public String getCustomDimension18() { return options.getCustomDimension18(); }

    /**
     * Returns content's customDimension19
     * @return customDimension19
     */
    public String getCustomDimension19() { return options.getCustomDimension19(); }

    /**
     * Returns content's customDimension20
     * @return customDimension20
     */
    public String getCustomDimension20() { return options.getCustomDimension20(); }

    /**
     * Returns ad's customDimension1
     * @return adCustomDimension1
     */
    public String getAdCustomDimension1() { return options.getAdCustomDimension1(); }

    /**
     * Returns ad's customDimension2
     * @return adCustomDimension2
     */
    public String getAdCustomDimension2() { return options.getAdCustomDimension2(); }

    /**
     * Returns ad's customDimension
     * @return adCustomDimension3
     */
    public String getAdCustomDimension3() { return options.getAdCustomDimension3(); }

    /**
     * Returns ad's customDimension4
     * @return adCustomDimension4
     */
    public String getAdCustomDimension4() { return options.getAdCustomDimension4(); }

    /**
     * Returns ad's customDimension5
     * @return adCustomDimension5
     */
    public String getAdCustomDimension5() { return options.getAdCustomDimension5(); }

    /**
     * Returns ad's customDimension6
     * @return adCustomDimension6
     */
    public String getAdCustomDimension6() { return options.getAdCustomDimension6(); }

    /**
     * Returns ad's customDimension7
     * @return adCustomDimension7
     */
    public String getAdCustomDimension7() { return options.getAdCustomDimension7(); }

    /**
     * Returns ad's customDimension8
     * @return adCustomDimension8
     */
    public String getAdCustomDimension8() { return options.getAdCustomDimension8(); }

    /**
     * Returns ad's customDimension9
     * @return adCustomDimension9
     */
    public String getAdCustomDimension9() { return options.getAdCustomDimension9(); }

    /**
     * Returns ad's customDimension10
     * @return adCustomDimension10
     */
    public String getAdCustomDimension10() { return options.getAdCustomDimension10(); }

    /**
     * Returns the app name
     * @return appName
     */
    public String getAppName() { return options.getAppName(); }

    /**
     * Returns the app version
     * @return appReleaseVersion
     */
    public String getAppReleaseVersion() { return options.getAppReleaseVersion(); }

    /**
     * Returns the version of the player that is used to play the ad(s)
     * @return the player version
     */
    public String getAdPlayerVersion() {
        String val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getPlayerVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdPlayerVersion");
                YouboraLog.error(e);
            }
        }

        if (val == null) val = "";

        return val;
    }

    /**
     * Returns the ad position as YOUBORA expects it; "pre", "mid", "post" or "unknown"
     * @return the ad position
     */
    public String getAdPosition() {
        PlayerAdapter.AdPosition pos = PlayerAdapter.AdPosition.UNKNOWN;
        if (adsAdapter != null) {
            try {
                pos = adsAdapter.getPosition();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdPosition");
                YouboraLog.error(e);
            }finally {
                if(pos == null ) pos = PlayerAdapter.AdPosition.UNKNOWN;
            }
        }

        if (pos == PlayerAdapter.AdPosition.UNKNOWN && adapter != null) {
            pos = adapter.getFlags().isJoined()? PlayerAdapter.AdPosition.MID : PlayerAdapter.AdPosition.PRE;
        }

        String position;

        switch (pos) {
            case PRE:
                position = "pre";
                break;
            case MID:
                position = "mid";
                break;
            case POST:
                position = "post";
                break;
            case UNKNOWN:
            default:
                position = "unknown";
        }

        return position;
    }

    /**
     * Returns ad's playhead in seconds
     * @return ad's playhead
     */
    public Double getAdPlayhead() {
        Double ph = null;
        if (adsAdapter != null) {
            try {
                ph = adsAdapter.getPlayhead();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdPlayhead");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(ph, 0.0);
    }

    /**
     * Returns ad's duration in seconds
     * @return ad's duration
     */
    public Double getAdDuration() {
        Double val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getDuration();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdDuration");
                YouboraLog.error(e);
            }
        }
        return YouboraUtil.parseNumber(val, 0.0);
    }

    /**
     * Returns ad's bitrate in bits per second
     * @return ad's bitrate
     */
    public Long getAdBitrate() {
        Long val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getBitrate();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdBitrate");
                YouboraLog.error(e);
            }
        }

        return YouboraUtil.parseNumber(val, -1L);
    }

    /**
     * Returns ad's campaign
     * @return ad's campaign
     */
    public String getAdCampaign() {
        return options.getAdCampaign();
    }

    /**
     * Returns ad's title
     * @return ad's title
     */
    public String getAdTitle() {
        String val = options.getAdTitle();
        if ((val == null || val.length() == 0) && adsAdapter != null) {
            try {
                val = adsAdapter.getTitle();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdTitle");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns ad's resource
     * @return ad's resource
     */
    public String getAdResource() {
        String val = options.getAdResource();
        if ((val == null || val.length() == 0) && adsAdapter != null) {
            try {
                val = adsAdapter.getResource();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdResource");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the ad adapter version
     * @return the ad adapter version
     */
    public String getAdAdapterVersion() {
        String val = null;
        if (adsAdapter != null) {
            try {
                val = adsAdapter.getVersion();
            } catch (Exception e) {
                YouboraLog.warn("An error occurred while calling getAdAdapterVersion");
                YouboraLog.error(e);
            }
        }

        return val;
    }

    /**
     * Returns the ad metadata
     * @return the ad metadata
     */
    public String getAdMetadata() {
        return YouboraUtil.stringifyBundle(options.getAdMetadata());
    }

    /**
     * Returns a json-formatted string with plugin info
     * @return plugin info
     */
    public String getPluginInfo() {
        Map<String, String> info = new HashMap<>(3);
        info.put("lib", BuildConfig.VERSION_NAME);
        info.put("adapter", getAdapterVersion());
        info.put("adAdapter", getAdAdapterVersion());
        return YouboraUtil.stringifyMap(info);
    }

    /**
     * Returns the Ip
     * @return the Ip
     */
    public String getIp() {
        return options.getNetworkIP();
    }

    public String getObfuscateIp(){
        return String.valueOf(options.getNetworkObfuscateIp());
    }

    /**
     * Returns the Isp
     * @return the Isp
     */
    public String getIsp() {
        return options.getNetworkIsp();
    }

    /**
     * Returns the connection type
     * @return the conneciton type
     */
    public String getConnectionType() {
        return options.getNetworkConnectionType();
    }

    /**
     * Returns the device code
     * @return the device code
     */
    public String getDeviceCode() {
        return options.getDeviceCode();
    }

    /**
     * Returns the account code
     * @return the account code
     */
    public String getAccountCode() {
        return options.getAccountCode();
    }

    /**
     * Returns the username
     * @return the username
     */
    public String getUsername() {
        return options.getUsername();
    }

    /**
     * Returns the anonymouser
     * @return the anonymouser
     */
    public String getAnonymousUser() {
        return options.getAnonymousUser();
    }

    /**
     * Returns if is forceInit
     */
    public boolean isForceInit() { return options.isForceInit(); }

    /**
     * Returns the userType
     * @return the userType
     */
    public String getUserType() {
        return options.getUserType();
    }

    /**
     * Get CDN node
     * @return the CDN node or null if unknown
     */
    public String getNodeHost() {
        return resourceTransform.getNodeHost();
    }

    /**
     * Get CDN type, parsed from the type string
     * @return the CDN type
     */
    public String getNodeType() {
        return resourceTransform.getNodeType();
    }

    /**
     * Get CDN type string, as returned in the cdn header response
     * @return the CDN type string
     */
    public String getNodeTypeString() {
        return resourceTransform.getNodeTypeString();
    }

    /**
     * Get Device info String
     */
    public String getDeviceInfoString() {
        DeviceInfo deviceInfo = new DeviceInfo.Builder()
                .setDeviceBrand(options.getDeviceBrand())
                .setDeviceModel(options.getDeviceModel())
                .setDeviceType(options.getDeviceType())
                .setDeviceCode(getDeviceCode())
                .setDeviceOsName(options.getDeviceOsName())
                .setDeviceOsVersion(options.getDeviceOsVersion())
                .build();
        return deviceInfo.mapToJSONString();
    }

    // ---------------------------------------- Background --------------------------------------------
    /**
     * Setter for the activity to listen to callback events (needed for background detection)
     */
    public void setActivity(Activity activity){
        this.activity = activity;
    }

    /**
     * Getter for the activity (needed for background detection)
     * @return Application context
     */
    public Activity getActivity(){
        return activity;
    }

    // ---------------------------------------- CHRONOS --------------------------------------------
    /**
     * Returns preload chrono delta time
     * @return the preload duration
     */
    public long getPreloadDuration() {
        return preloadChrono.getDeltaTime(false);
    }

    /**
     * Returns init chrono delta time
     * @return the init duration
     */
    public long getInitDuration() {
        return initChrono.getDeltaTime(false);
    }

    /**
     * Returns JoinDuration chrono delta time
     * @return the join duration
     */
    public long getJoinDuration() {
        if (isInitiated) return getInitDuration();
        return adapter != null ? adapter.getChronos().join.getDeltaTime(false) : -1;
    }

    /**
     * Returns BufferDuration chrono delta time
     * @return the buffer duration
     */
    public long getBufferDuration() {
        return adapter != null ? adapter.getChronos().buffer.getDeltaTime(false) : -1;
    }

    /**
     *  Returns SeekDuration chrono delta time
     * @return the seek duration
     */
    public long getSeekDuration() {
        return adapter != null ? adapter.getChronos().seek.getDeltaTime(false) : -1;
    }

    /**
     * Returns pauseDuration chrono delta time
     * @return the pause duration
     */
    public long getPauseDuration() {
        return adapter != null ? adapter.getChronos().pause.getDeltaTime(false) : -1;
    }

    /**
     * Returns AdJoinDuration chrono delta time
     * @return the ad join duration
     */
    public long getAdJoinDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().join.getDeltaTime(false) : -1;
    }

    /**
     * Returns AdBufferDuration chrono delta time
     * @return the ad buffer duration
     */
    public long getAdBufferDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().buffer.getDeltaTime(false) : -1;
    }

    /**
     * Returns AdPauseDuration chrono delta time
     * @return the ad pause duration
     */
    public long getAdPauseDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().pause.getDeltaTime(false) : -1;
    }

    /**
     * Returns total totalAdDuration chrono delta time
     * @return the total ad duration
     */
    public long getAdTotalDuration() {
        return adsAdapter != null ? adsAdapter.getChronos().total.getDeltaTime(false) : -1;
    }

    /**
     * Returns household id
     * @return Household Id
     */
    public String getHouseholdId(){
        String val = null;

        if(getAdapter() != null){
            try{
                val = getAdapter().getHouseholdId();
            }catch(Exception ex){
                YouboraLog.debug("An error occurred while calling getHouseholdId");
                YouboraLog.error(ex);
            }
        }
        return val;
    }

    public Integer getCdnTraffic() {
        Integer val = null;

        if (getAdapter() != null) {
            try {
                val = getAdapter().getCdnTraffic();
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getCdnTraffic");
                YouboraLog.error(ex);
            }
        }
        return YouboraUtil.parseNumber(val, 0);
    }

    public Integer getP2PTraffic() {
        Integer val = null;

        if (getAdapter() != null) {
            try {
                val = getAdapter().getP2PTraffic();
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getP2PTraffic");
                YouboraLog.error(ex);
            }
        }
        return YouboraUtil.parseNumber(val, 0);
    }

    public Boolean getIsP2PEnabled() {
        Boolean val = null;

        if (getAdapter() != null) {
            try {
                val = getAdapter().getIsP2PEnabled();
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getIsP2PEnabled");
                YouboraLog.error(ex);
            }
        }
        return val;
    }

    public Integer getUploadTraffic() {
        Integer val = null;

        if (getAdapter() != null) {
            try {
                val = getAdapter().getUploadTraffic();
            } catch (Exception ex) {
                YouboraLog.debug("An error occurred while calling getUploadTraffic");
                YouboraLog.error(ex);
            }
        }
        return YouboraUtil.parseNumber(val, 0);
    }

    /**
     * Returns current device language in language-COUNTRYCODE format
     * @return Current language
     */
    public String getLanguage() {
        String locale = Locale.getDefault().toString();
        return locale.replace('_', '-');
    }

    /**
     * Returns the {@link RequestBuilder} instance that this Plugin is using
     * @return the {@link RequestBuilder} instance that this Plugin is using
     */
    public RequestBuilder getRequestBuilder() {
        return requestBuilder;
    }

    /**
     * Returns infinity current NavContext
     * @return {@link Infinity} NavContext
     */
    public String getNavContext(){
        return getInfinity().getNavContext();
    }

    /**
     * Returns all active session id
     * @return all active session id
     */
    public List<String> getActiveSessions() {
        return getInfinity().getActiveSessions();
    }

    public Boolean getIsInfinity() {

        Boolean val = options.getIsInfinity();
        return val == null ? Boolean.FALSE : val;
    }

    /**
     * Returns SmartSwitch config code
     * @return smartSwitch config code
     */
    public String getSmartSwitchConfigCode() {
        return options.getSmartSwitchConfigCode();
    }

    /**
     * Returns SmartSwitch group code
     * @return smartSwitch group code
     */
    public String getSmartSwitchGroupCode() {
        return options.getSmartSwitchGroupCode();
    }

    /**
     * Returns SmartSwitch contract code
     * @return smartSwitch contract code
     */
    public String getSmartSwitchContractCode() {
        return options.getSmartSwitchContractCode();
    }

    /**
     * Returns the fingerprint from the device
     * @return fingerprint
     */
    public String getFingerprint() {
        Context context = null;
        String fingerprint = null;

        if (getApplicationContext() != null)
            context = getApplicationContext();
        else if (getActivity() != null)
            context = getActivity().getBaseContext();

        if (context != null) {
            InfinitySharedPreferencesManager infinityStorage =
                    new InfinitySharedPreferencesManager(context);

            if (infinityStorage.getFingerPrint() == null) {
                if (YouboraPermissionUtils.isReadPhoneStatePermissionGranted(context))
                   infinityStorage.saveFingerprint(generateFingerprint());
                else
                    infinityStorage.saveFingerprint(YouboraUtil.getRandomUUID());
            }

            fingerprint = infinityStorage.getFingerPrint();
        }

        return fingerprint;
    }

    private String generateFingerprint() {
        final TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(
                context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(),
                ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUuid.toString();
    }

    // Add listeners
    /**
     * Adds an Init listener
     * @param listener to add
     */
    public void addOnWillSendInitListener(WillSendRequestListener listener) {
        if (willSendInitListeners == null)
            willSendInitListeners = new ArrayList<>(1);

        willSendInitListeners.add(listener);
    }

    /**
     * Adds a Start listener
     * @param listener to add
     */
    public void addOnWillSendStartListener(WillSendRequestListener listener) {
        if (willSendStartListeners == null)
            willSendStartListeners = new ArrayList<>(1);

        willSendStartListeners.add(listener);
    }

    /**
     * Adds a Join listener
     * @param listener to add
     */
    public void addOnWillSendJoinListener(WillSendRequestListener listener) {
        if (willSendJoinListeners == null)
            willSendJoinListeners = new ArrayList<>(1);

        willSendJoinListeners.add(listener);
    }

    /**
     * Adds a Pause listener
     * @param listener to add
     */
    public void addOnWillSendPauseListener(WillSendRequestListener listener) {
        if (willSendPauseListeners == null)
            willSendPauseListeners = new ArrayList<>(1);

        willSendPauseListeners.add(listener);
    }

    /**
     * Adds a Resume listener
     * @param listener to add
     */
    public void addOnWillSendResumeListener(WillSendRequestListener listener) {
        if (willSendResumeListeners == null)
            willSendResumeListeners = new ArrayList<>(1);

        willSendResumeListeners.add(listener);
    }

    /**
     * Adds a Seek listener
     * @param listener to add
     */
    public void addOnWillSendSeekListener(WillSendRequestListener listener) {
        if (willSendSeekListeners == null)
            willSendSeekListeners = new ArrayList<>(1);

        willSendSeekListeners.add(listener);
    }

    /**
     * Adds a Buffer listener
     * @param listener to add
     */
    public void addOnWillSendBufferListener(WillSendRequestListener listener) {
        if (willSendBufferListeners == null)
            willSendBufferListeners = new ArrayList<>(1);

        willSendBufferListeners.add(listener);
    }

    /**
     * Adds a Error listener
     * @param listener to add
     */
    public void addOnWillSendErrorListener(WillSendRequestListener listener) {
        if (willSendErrorListeners == null)
            willSendErrorListeners = new ArrayList<>(1);

        willSendErrorListeners.add(listener);
    }

    /**
     * Adds a Stop listener
     * @param listener to add
     */
    public void addOnWillSendStopListener(WillSendRequestListener listener) {
        if (willSendStopListeners == null)
            willSendStopListeners = new ArrayList<>(1);

        willSendStopListeners.add(listener);
    }

    /**
     * Adds a Ping listener
     * @param listener to add
     */
    public void addOnWillSendPingListener(WillSendRequestListener listener) {
        if (willSendPingListeners == null)
            willSendPingListeners = new ArrayList<>(1);

        willSendPingListeners.add(listener);
    }

    /**
     * Adds an ad Start listener
     * @param listener to add
     */
    public void addOnWillSendAdStartListener(WillSendRequestListener listener) {
        if (willSendAdStartListeners == null)
            willSendAdStartListeners = new ArrayList<>(1);

        willSendAdStartListeners.add(listener);
    }

    /**
     * Adds an ad Join listener
     * @param listener to add
     */
    public void addOnWillSendAdJoinListener(WillSendRequestListener listener) {
        if (willSendAdJoinListeners == null)
            willSendAdJoinListeners = new ArrayList<>(1);

        willSendAdJoinListeners.add(listener);
    }

    /**
     * Adds an ad Pause listener
     * @param listener to add
     */
    public void addOnWillSendAdPauseListener(WillSendRequestListener listener) {
        if (willSendAdPauseListeners == null)
            willSendAdPauseListeners = new ArrayList<>(1);

        willSendAdPauseListeners.add(listener);
    }

    /**
     * Adds an ad Resume listener
     * @param listener to add
     */
    public void addOnWillSendAdResumeListener(WillSendRequestListener listener) {
        if (willSendAdResumeListeners == null)
            willSendAdResumeListeners = new ArrayList<>(1);

        willSendAdResumeListeners.add(listener);
    }

    /**
     * Adds an ad Buffer listener
     * @param listener to add
     */
    public void addOnWillSendAdBufferListener(WillSendRequestListener listener) {
        if (willSendAdBufferListeners == null)
            willSendAdBufferListeners = new ArrayList<>(1);

        willSendAdBufferListeners.add(listener);
    }

    /**
     * Adds an ad Stop listener
     * @param listener to add
     */
    public void addOnWillSendAdStopListener(WillSendRequestListener listener) {
        if (willSendAdStopListeners == null)
            willSendAdStopListeners = new ArrayList<>(1);

        willSendAdStopListeners.add(listener);
    }

    /**
     * Adds an ad Stop listener
     * @param listener to add
     */
    public void addOnWillSendAdClickListener(WillSendRequestListener listener) {
        if (willSendAdClickListeners == null)
            willSendAdClickListeners = new ArrayList<>(1);

        willSendAdClickListeners.add(listener);
    }

    /**
     * Adds an ad Init listener
     * @param listener to add
     */
    public void addOnWillSendAdInitListener(WillSendRequestListener listener) {
        if (willSendAdInitListeners == null)
            willSendAdInitListeners = new ArrayList<>(1);

        willSendAdInitListeners.add(listener);
    }

    /**
     * Adds an ad Error listener
     * @param listener to add
     */
    public void addOnWillSendAdErrorListener(WillSendRequestListener listener) {
        if (willSendAdErrorListeners == null)
            willSendAdErrorListeners = new ArrayList<>(1);

        willSendAdErrorListeners.add(listener);
    }

    /**
     * Adds a session Start listener
     * @param listener to add
     */
    public void addOnWillSendSessionStartListener(WillSendRequestListener listener) {
        if (willSendSessionStartListeners == null)
            willSendSessionStartListeners = new ArrayList<>(1);

        willSendSessionStartListeners.add(listener);
    }

    /**
     * Adds a session Stop listener
     * @param listener to add
     */
    public void addOnWillSendSessionStopListener(WillSendRequestListener listener) {
        if (willSendSessionStopListeners == null)
            willSendSessionStopListeners = new ArrayList<>(1);

        willSendSessionStopListeners.add(listener);
    }

    /**
     * Adds a session Stop listener
     * @param listener to add
     */
    public void addOnWillSendSessionEventListener(WillSendRequestListener listener) {
        if (willSendSessionEventListeners == null)
            willSendSessionEventListeners = new ArrayList<>(1);

        willSendSessionEventListeners.add(listener);
    }

    /**
     * Adds a session Stop listener
     * @param listener to add
     */
    public void addOnWillSendSessionNavListener(WillSendRequestListener listener) {
        if (willSendSessionNavListeners == null)
            willSendSessionNavListeners = new ArrayList<>(1);

        willSendSessionNavListeners.add(listener);
    }

    /**
     * Adds a session Stop listener
     * @param listener to add
     */
    public void addOnWillSendSessionBeatListener(WillSendRequestListener listener) {
        if (willSendSessionBeatListeners == null)
            willSendSessionBeatListeners = new ArrayList<>(1);

        willSendSessionBeatListeners.add(listener);
    }

    // Remove listeners
    /**
     * Removes an Init listener
     * @param listener to remove
     */
    public void removeOnWillSendInitListener(WillSendRequestListener listener) {
        if (willSendInitListeners != null)
            willSendInitListeners.remove(listener);
    }

    // Remove listeners
    /**
     * Removes an ad Init listener
     * @param listener to remove
     */
    public void removeOnWillSendAdInitListener(WillSendRequestListener listener) {
        if (willSendAdInitListeners != null)
            willSendAdInitListeners.remove(listener);
    }

    /**
     * Removes a Start listener
     * @param listener to remove
     */
    public void removeOnWillSendStartListener(WillSendRequestListener listener) {
        if (willSendStartListeners != null)
            willSendStartListeners.remove(listener);
    }

    /**
     * Removes a Join listener
     * @param listener to remove
     */
    public void removeOnWillSendJoinListener(WillSendRequestListener listener) {
        if (willSendJoinListeners != null)
            willSendJoinListeners.remove(listener);
    }

    /**
     * Removes a Pause listener
     * @param listener to remove
     */
    public void removeOnWillSendPauseListener(WillSendRequestListener listener) {
        if (willSendPauseListeners != null)
            willSendPauseListeners.remove(listener);
    }

    /**
     * Removes a Resume listener
     * @param listener to remove
     */
    public void removeOnWillSendResumeListener(WillSendRequestListener listener) {
        if (willSendResumeListeners != null)
            willSendResumeListeners.remove(listener);
    }

    /**
     * Removes a Seek listener
     * @param listener to remove
     */
    public void removeOnWillSendSeekListener(WillSendRequestListener listener) {
        if (willSendSeekListeners != null)
            willSendSeekListeners.remove(listener);
    }

    /**
     * Removes a Buffer listener
     * @param listener to remove
     */
    public void removeOnWillSendBufferListener(WillSendRequestListener listener) {
        if (willSendBufferListeners != null)
            willSendBufferListeners.remove(listener);
    }

    /**
     * Removes a Error listener
     * @param listener to remove
     */
    public void removeOnWillSendErrorListener(WillSendRequestListener listener) {
        if (willSendErrorListeners != null)
            willSendErrorListeners.remove(listener);
    }

    /**
     * Removes a Stop listener
     * @param listener to remove
     */
    public void removeOnWillSendStopListener(WillSendRequestListener listener) {
        if (willSendStopListeners != null)
            willSendStopListeners.remove(listener);
    }

    /**
     * Removes a Ping listener
     * @param listener to remove
     */
    public void removeOnWillSendPingListener(WillSendRequestListener listener) {
        if (willSendPingListeners != null)
            willSendPingListeners.remove(listener);
    }

    /**
     * Removes an ad Start listener
     * @param listener to remove
     */
    public void removeOnWillSendAdStartListener(WillSendRequestListener listener) {
        if (willSendAdStartListeners != null)
            willSendAdStartListeners.remove(listener);
    }

    /**
     * Removes an ad Join listener
     * @param listener to remove
     */
    public void removeOnWillSendAdJoinListener(WillSendRequestListener listener) {
        if (willSendAdJoinListeners != null)
            willSendAdJoinListeners.remove(listener);
    }

    /**
     * Removes an ad Pause listener
     * @param listener to remove
     */
    public void removeOnWillSendAdPauseListener(WillSendRequestListener listener) {
        if (willSendAdPauseListeners != null)
            willSendAdPauseListeners.remove(listener);
    }

    /**
     * Removes an ad Resume listener
     * @param listener to remove
     */
    public void removeOnWillSendAdResumeListener(WillSendRequestListener listener) {
        if (willSendAdResumeListeners != null)
            willSendAdResumeListeners.remove(listener);
    }

    /**
     * Removes an ad Buffer listener
     * @param listener to remove
     */
    public void removeOnWillSendAdBufferListener(WillSendRequestListener listener) {
        if (willSendAdBufferListeners != null)
            willSendAdBufferListeners.remove(listener);
    }

    /**
     * Removes an ad Stop listener
     * @param listener to remove
     */
    public void removeOnWillSendAdStopListener(WillSendRequestListener listener) {
        if (willSendAdStopListeners != null)
            willSendAdStopListeners.remove(listener);
    }

    /**
     * Removes an ad Click listener
     * @param listener to remove
     */
    public void removeOnWillSendAdClick(WillSendRequestListener listener) {
        if (willSendAdClickListeners != null)
            willSendAdClickListeners.remove(listener);
    }

    /**
     * Removes an ad Error listener
     * @param listener to remove
     */
    public void removeOnWillSendAdError(WillSendRequestListener listener) {
        if (willSendAdErrorListeners != null)
            willSendAdErrorListeners.remove(listener);
    }

    /**
     * Removes a Session start listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionStart(WillSendRequestListener listener) {
        if (willSendSessionStartListeners != null)
            willSendSessionStartListeners.remove(listener);
    }

    /**
     * Removes a Session stop listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionStop(WillSendRequestListener listener) {
        if (willSendSessionStopListeners != null)
            willSendSessionStopListeners.remove(listener);
    }

    /**
     * Removes a Session nav listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionNav(WillSendRequestListener listener) {
        if (willSendSessionNavListeners != null)
            willSendSessionNavListeners.remove(listener);
    }

    /**
     * Removes a Session event listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionEvent(WillSendRequestListener listener) {
        if (willSendSessionEventListeners != null) {
            willSendSessionEventListeners.remove(listener);
        }
    }

    /**
     * Removes a Session beat listener
     * @param listener to remove
     */
    public void removeOnWillSendSessionBeat(WillSendRequestListener listener) {
        if (willSendSessionBeatListeners != null)
            willSendSessionBeatListeners.remove(listener);
    }

    /**
     * Saves the context
     */
    public void setApplicationContext(Context context){
        this.context = context;
    }

    /**
     * Gets the context
     */
    public Context getApplicationContext(){
        return this.context;
    }

    // Content Adapter listener
    private PlayerAdapter.AdapterEventListener eventListener = new PlayerAdapter.AdapterEventListener() {

        @Override
        public void onStart(Map<String, String> params) {
            startListener(params);
        }

        @Override
        public void onJoin(Map<String, String> params) {
            joinListener(params);
        }

        @Override
        public void onPause(Map<String, String> params) {
            pauseListener(params);
        }

        @Override
        public void onResume(Map<String, String> params) {
            resumeListener(params);
        }

        @Override
        public void onStop(Map<String, String> params) {
            stopListener(params);
        }

        @Override
        public void onBufferBegin(Map<String, String> params, boolean convertFromBuffer) {
            bufferBeginListener();
        }

        @Override
        public void onBufferEnd(Map<String, String> params) {
            bufferEndListener(params);
        }

        @Override
        public void onSeekBegin(Map<String, String> params, boolean convertFromBuffer) {
            seekBeginListener();
        }

        @Override
        public void onSeekEnd(Map<String, String> params) {
            seekEndListener(params);
        }

        @Override
        public void onClick(Map<String, String> params) { }

        @Override
        public void onAllAdsCompleted(Map<String, String> params) { }

        @Override
        public void onAdInit(Map<String, String> params) {
            adInitListener(params);
        }

        @Override
        public void onError(Map<String, String> params) {
            errorListener(params);
        }
    };

    // Ad Adapter listener
    private PlayerAdapter.AdapterEventListener adEventListener =  new PlayerAdapter.AdapterEventListenerImpl() {

        @Override
        public void onStart(Map<String, String> params) {
            adStartListener(params);
        }

        @Override
        public void onJoin(Map<String, String> params) {
            adJoinListener(params);
        }

        @Override
        public void onPause(Map<String, String> params) {
            adPauseListener(params);
        }

        @Override
        public void onResume(Map<String, String> params) {
            adResumeListener(params);
        }

        @Override
        public void onStop(Map<String, String> params) {
            adStopListener(params);
        }

        @Override
        public void onBufferBegin(Map<String, String> params, boolean convertFromBuffer) {
            adBufferBeginListener();
        }

        @Override
        public void onBufferEnd(Map<String, String> params) {
            adBufferEndListener(params);
        }

        @Override
        public void onClick(Map<String, String> params) {
            adClickListener(params);
        }

        @Override
        public void onAdInit(Map<String, String> params) {
            adInitListener(params);
        }

        @Override
        public void onAllAdsCompleted(Map<String, String> params) {
            adAllAdsCompleted(params);
        }

        @Override
        public void onError(Map<String, String> params) {
            adErrorListener(params);
        }
    };

    //Infinity
    private Infinity.InfinityEventListener infinityEventListener = new Infinity.InfinityEventListener() {

        @Override
        public void onSessionStart(String screenName, Map<String, String> dimensions, String parentId) {
            sessionStartListener(screenName, dimensions, parentId);
        }

        @Override
        public void onSessionStop(Map<String, String> params) {
            sessionStopListener(params);
        }

        @Override
        public void onNav(String screenName) {
            sessionNavListener(screenName);
        }

        @Override
        public void onEvent(Map<String, String> dimensions, Map<String, Double> values, String eventName) {
            sessionEventListener(dimensions, values, eventName);
        }
    };

    /**
     * Will send Request Interface.
     * This callback will be invoked just before an event is sent, to offer the chance to
     * modify the params that will be sent.
     */
    public interface WillSendRequestListener {

        /**
         * Called just before a {@link Request} is sent
         * @param serviceName the service name
         * @param plugin the {@link Plugin} that is calling this callback
         * @param params a Map of params that will be sent in the Request
         */
        void willSendRequest(String serviceName, Plugin plugin, Map<String, String> params);
        /**
         * Called just before a {@link Request} is sent
         * @param serviceName the service name
         * @param plugin the {@link Plugin} that is calling this callback
         * @param params an ArrayList of {@link JSONObject}
         */
        void willSendRequest(String serviceName, Plugin plugin, ArrayList<JSONObject> params);
    }

    public boolean isStarted() {
        return isStarted;
    }

    private boolean isExtraMetadataReady() {
        Bundle options = getOptions().toBundle();

        if (getOptions().getPendingMetadata() != null && getOptions().getWaitForMetadata()) {
            ArrayList<String> pendingMetadataKeys = getOptions().getPendingMetadata();
            for(String pendingKey: pendingMetadataKeys) {
                if (options.get(pendingKey) == null) {
                    return false;
                }
            }
        }
        return true;
    }
}