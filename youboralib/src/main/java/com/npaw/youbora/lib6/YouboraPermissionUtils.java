package com.npaw.youbora.lib6;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;

/**
 * An utility class with static methods to check whether a permission is granted or not.
 *
 * @author      Nice People at Work
 * @since       6.3.8
 */
public class YouboraPermissionUtils {

    static public boolean isReadPhoneStatePermissionGranted(Context context) {
        String permission = Manifest.permission.READ_PHONE_STATE;
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}
